import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { SafeHtml } from './pipes/safe-html-pipe';
// import { LoaderComponent } from '../Galaxy applications/Vms/vms-container/Shared/loader/loader.component';

@NgModule({
  declarations: [
    SafeHtml
],
  imports: [
    CommonModule, MatAutocompleteModule
  ],
  exports: [
   SafeHtml, MatAutocompleteModule
  ]
})
export class SharedModule { }
