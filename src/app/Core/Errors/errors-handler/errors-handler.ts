import { ErrorHandler, Injectable, Injector} from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { NotificationService } from '../../services/notification/notification.service';

// import * as StackTraceParser from 'error-stack-parser';



@Injectable(
  {
    providedIn: 'root'
  }
)

export class ErrorsHandler implements ErrorHandler {
  constructor(
    private injector: Injector,
  ) {}

  handleError(error: Error | HttpErrorResponse) {
    const notificationService = this.injector.get(NotificationService);
    const router = this.injector.get(Router);

    if (error instanceof HttpErrorResponse) {
    // Server error happened
      console.log(!navigator.onLine);
      if (!navigator.onLine) {
        // No Internet connection
        console.log('offline');
        return notificationService.notify('No Internet Connection');
      }
      // Http Error
      console.log(`${error.status} - ${error.message}`);
      return notificationService.notify(`${error.status} - ${error.message}`);
    } else {
      // Client Error Happend
      console.log('client error');
      router.navigate(['/error'], { queryParams: {error: error} });
    }
    // Log the error anyway
    console.error(error);
  }
}

