import { NgModule, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';


import { ErrorsHandler } from './errors-handler/errors-handler';
import { ServerErrorsInterceptor } from './server-errors-interceptors/server-errors.interceptor';


import { ErrorRoutingModule } from './errors-routing/errors-routing.module';

import { ErrorsComponent } from './errors-component/errors/errors.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ErrorRoutingModule,
    HttpClientModule
  ],
  declarations: [
    ErrorsComponent
  ],
  providers: [

    {
      provide: HTTP_INTERCEPTORS,
      useClass: ServerErrorsInterceptor,
      multi: true
    },
  ]
})
export class ErrorsModule { }
