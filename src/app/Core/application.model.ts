export class Application {
  AppId: number;
  AppName: string;
  AppURL: string;
  AppIcon: string;
  AppDesc: string;
  IsFavorite: boolean;
  constructor(appName: string, appUrl: string, appIcon: string, appId: number, appDesc: string, isFavorite: boolean = false) {
    this.AppId = appId;
    this.AppName = appName;
    this.AppURL = appUrl;
    this.AppIcon = appIcon;
    this.AppDesc = appDesc;
    this.IsFavorite = isFavorite;
  }
}
