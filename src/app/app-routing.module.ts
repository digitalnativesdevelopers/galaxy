import { AuthGuard } from './Auth/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './Featured/home-page/home-page.component';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
  // { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '', component: HomePageComponent},
  { path: 'TruVisit', loadChildren: './Galaxy applications/Vms/vms-container/vms.module#VmsModule'},
  { path: 'all', loadChildren: './Featured/all-apps/all-apps.module#AllAppsModule'},
  { path: 'admin', loadChildren: './Featured/admin/admin.module#AdminModule'},
  { path: '**', component: NotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
