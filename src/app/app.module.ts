import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomePageModule } from './Featured/home-page/home-page.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {  RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';

import { ErrorsModule } from './Core/Errors';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MsAdalAngular6Module } from 'microsoft-adal-angular6';
import { AuthenticationGuard, MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { SharedModule } from './shared/shared.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { AlertsModule } from 'angular-alert-module';
// import { LoaderComponent } from './loader/loader.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { VmsModule } from './Galaxy applications/Vms/vms-container/vms.module';

import { InterceptorConfigService } from './utilities/interceptor-config.service';
import { AuthGuard } from './Auth/auth.guard';

// import { MatMomentDateModule, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from "@angular/material-moment-adapter";
import { AuthenticationService } from 'src/app/Auth/authentication.service';
import { ChartsModule } from 'ng2-charts/ng2-charts';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
  //  LoaderComponent,
  ],
  imports: [
    BrowserModule,
    SharedModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    HttpClientModule,
    RouterModule,
    ToastrModule.forRoot(),
    HomePageModule,
    NgbModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    VmsModule,
    ToastrModule.forRoot(),
    // MatMomentDateModule,
    // AlertsModule.forRoot(),
    MsAdalAngular6Module.forRoot({
      tenant: 'FidelityBankNigeria.onmicrosoft.com',
      clientId: '41119c22-5640-41f0-abab-4ef4b73f6d4a',
      redirectUri: window.location.origin,
      endpoints: {
        'https://localhost/Api/': 'xxx-bae6-4760-b434-xxx'
      },
      navigateToLoginRequestUrl: true,
      cacheLocation: '<localStorage / sessionStorage>'
    }),
    AppRoutingModule,
    ErrorsModule,
    SharedModule,
    ChartsModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
  ],
  providers: [AuthenticationGuard, MsAdalAngular6Service, AuthGuard, AuthenticationService,
     { provide: HTTP_INTERCEPTORS, useClass: InterceptorConfigService, multi: true }
    // { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } }
  ],

  bootstrap: [AppComponent]
})
export class AppModule {
}
