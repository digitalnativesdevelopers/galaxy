import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VmsAuthenticationService } from './Shared/auth/vms-authentication.service';
import { AuthenticationService } from 'src/app/Auth/authentication.service';

@Component({
  selector: 'app-vms-container',
  templateUrl: './vms-container.component.html',
  styleUrls: ['./vms-container.component.scss']
})
export class VmsContainerComponent implements OnInit {
  role: 'receptionist';
  constructor( private router: Router,
    private galaxyAuth: AuthenticationService,
    private vmsAuth: VmsAuthenticationService) { }


  ngOnInit() {
    this.galaxyAuth.getStaffRole();
  }

}
