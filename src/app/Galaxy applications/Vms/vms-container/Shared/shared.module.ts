import { LoaderComponent } from './loader/loader.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { ErrorComponent } from './error/error.component';
import { AuthComponent } from './auth/auth.component';
import { NotificationComponent } from './notification/notification.component';
import { TableComponent } from './table/table.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatToolbarModule, MatButtonModule,
  MatSelectModule, MatInputModule,
  MatRadioModule, MatStepperModule, MatAutocompleteModule,
  MatNativeDateModule, MatFormFieldModule, MatTableModule,
  // tslint:disable-next-line:max-line-length
  MatPaginatorModule, MatSortModule, MatDatepickerModule, MatTabsModule,
  MatCardModule, MatIconModule, MatGridListModule, MatDialogModule, MatChipsModule } from '@angular/material';
  // tslint:disable-next-line:max-line-length
  // MatPaginatorModule, MatSortModule, MatDatepickerModule, MatTabsModule, MatCardModule, MatIconModule, MatGridListModule, MatDialogModule, MatChipsModule } from '@angular/material';



@NgModule({
  declarations: [ErrorComponent, AuthComponent, NotificationComponent, TableComponent, LoaderComponent],
  imports: [
    MatChipsModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatStepperModule,
    MatSelectModule,
    MatCardModule,
    MatInputModule,
    MatNativeDateModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    CommonModule,
    SharedRoutingModule,
    MatButtonModule,
    MatTabsModule,
    MatToolbarModule,
    MatCardModule,
    FormsModule,
    MatIconModule,
    MatGridListModule,
    MatDialogModule,
    MatChipsModule
  ],
  exports: [ MatRadioModule, MatAutocompleteModule, MatChipsModule,
    MatStepperModule, LoaderComponent, ErrorComponent, AuthComponent, NotificationComponent,
    TableComponent, MatSortModule,
    MatPaginatorModule, MatTableModule, MatDatepickerModule, MatFormFieldModule,
    MatToolbarModule, MatCardModule,
   // tslint:disable-next-line:max-line-length
   MatNativeDateModule, ReactiveFormsModule, MatInputModule, MatButtonModule,
   MatSelectModule, MatTabsModule, FormsModule, MatIconModule,
   MatGridListModule, MatDialogModule
  ]
})
export class SharedModule { }
