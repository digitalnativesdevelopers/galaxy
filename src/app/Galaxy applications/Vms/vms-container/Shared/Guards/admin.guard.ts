import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { VmsAuthenticationService } from '../auth/vms-authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor( private router: Router, private vmsAuth: VmsAuthenticationService ){

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      if (this.vmsAuth.isAdmin) {
        console.log('User is either a Super Admin or VMS Admin');
        return true;
      }
      this.router.navigateByUrl('/TruVisit/staff');
      console.log('User is neither a VMS Admin or Super admin');
        return false;
  }
}
