import { TestBed, async, inject } from '@angular/core/testing';

import { ReceptionistGuard } from './receptionist.guard';

describe('ReceptionistGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReceptionistGuard]
    });
  });

  it('should ...', inject([ReceptionistGuard], (guard: ReceptionistGuard) => {
    expect(guard).toBeTruthy();
  }));
});
