import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { UtilitiesService } from 'src/app/utilities/utilities.service';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})

export class AuthService {
  BASE_URL: string = environment.galaxy_vms_baseUrl;
  galaxyGetRoleUrl = `${this.BASE_URL}/getUserRole`; // endpoint subject to change #wishful coding

  private roleSource = new BehaviorSubject<StaffRole[]>(null);
  staffole$ = this.roleSource.asObservable();

   // Observable sources: Error
   private errorSource = new BehaviorSubject<string>(null);
   error$ = this.errorSource.asObservable();

   // Observable sources: Loader
   private loaderSource = new BehaviorSubject<boolean>(false);
   loader$ = this.loaderSource.asObservable();

  constructor(private http: HttpClient, private util: UtilitiesService) { }
  data = {
    currentUser : 'P2045'
  };
  getUserRole(): Observable<any> {
    return this.http.post(this.galaxyGetRoleUrl, this.data)
    .pipe(
      retry(3),
      catchError(this.util.handleError)
    );
  }

  getUserRoleData() {
    this.getUserRole().subscribe(response => {
      this.extractData(response.ResponseObject);
      this.updateError(null);
    }, (err: HttpErrorResponse) => {
      this.updateError(err);
      this.updateLoader(false);
    }
    );
  }

  extractData(response) {
    const staffRole: StaffRole[] = [];
    response.forEach(res => {
      staffRole.push(
        {roleName : res.RoleName}
      );
    });
    this.updateLoader(false);
    this.updateError(null);
    this.updateStaffRole(staffRole);
  }

  updateStaffRole(staffRole) {
    this.roleSource.next(staffRole);
  }

  updateError(message) {
    this.errorSource.next(message);
    // console.log(message);
  }

  updateLoader(state) {
    this.loaderSource.next(state);
  }


}

interface StaffRole {
  roleName: string;
}
