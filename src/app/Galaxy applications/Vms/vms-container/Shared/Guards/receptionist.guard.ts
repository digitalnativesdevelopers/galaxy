import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { VmsAuthenticationService } from '../auth/vms-authentication.service';

@Injectable({
  providedIn: 'root'
})
export class ReceptionistGuard implements CanActivate {

  role = 'admin';

  constructor( private router: Router, private vmsAuth: VmsAuthenticationService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      if ( this.vmsAuth.isReceptionist ) {
        return true;
      }
      this.router.navigateByUrl('/TruVisit/admin');
      return false;
  }
}
