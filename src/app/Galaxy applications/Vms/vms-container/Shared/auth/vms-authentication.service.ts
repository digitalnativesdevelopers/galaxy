import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { AuthenticationService } from 'src/app/Auth/authentication.service';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { retry, catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

const param = {
  'staffId': '',
  'platform': ''
};

@Injectable({
  providedIn: 'root'
})
export class VmsAuthenticationService {

  staffRole = new BehaviorSubject<any>(null);
  staffRole$ = this.staffRole.asObservable();

  roles = [];
  constructor( private http: HttpClient, private galaxyAuth: AuthenticationService ) { }

  _checkStaffRole (): Observable<any> {
    return this.http.post(environment.galaxy_checkStaffRole, param)
    .pipe(
      retry(3), catchError(this.handleError)
    );
  }

  get isSuperAdmin () {
    let role;
    const roles = [];
    role = JSON.parse(sessionStorage.getItem('***'));
    for (const r of role) {
      this.roles.push(window.atob(r).split(/ /g)[0] + ' ' + window.atob(r).split(/ /g)[1]);
    }
    if (this.roles.includes('Super Admin')) {
      return true;
    }
    return false;
  }

  get isReceptionist () {
    let role;
    const roles = [];
    role = JSON.parse(sessionStorage.getItem('***'));
    console.log(role);
    for (const r of role) {
      this.roles.push(window.atob(r).split(/ /g)[0] + ' ' + window.atob(r).split(/ /g)[1]);
    }
    if (this.roles.includes('VMS Receptionist') ) {
      return true;
    }
    return false;
  }

  get isAdmin () {
    let role;
    const roles = [];
    role = JSON.parse(sessionStorage.getItem('***'));
    for (const r of role) {
      this.roles.push(window.atob(r).split(/ /g)[0] + ' ' + window.atob(r).split(/ /g)[1]);
    }
    if (this.roles.includes('VMS Admin') || this.roles.includes('Super Admin')) {
      return true;
    }
    return false;
  }


  handleError( error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }


}
