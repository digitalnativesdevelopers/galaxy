import { TestBed } from '@angular/core/testing';

import { VmsAuthenticationService } from './vms-authentication.service';

describe('VmsAuthenticationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VmsAuthenticationService = TestBed.get(VmsAuthenticationService);
    expect(service).toBeTruthy();
  });
});
