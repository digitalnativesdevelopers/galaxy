import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import * as $ from 'jquery';
import { BranchUser, SigninModel, UserPendingAppointments } from './signin-model';
import { SigninService } from './signin.service';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';




@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  allBranchUser: BranchUser[];
  filteredList : Observable<BranchUser[]>;
  UserPendingAppointment: UserPendingAppointments[];
  isLinear = true;
  signInForm: FormGroup;
  appointmentsSubForm: FormGroup;
  personalDetailsSubForm: FormGroup;
  deviceSubForm: FormGroup;
  captureImageSubForm: FormGroup;
  captureFingerprintSubForm: FormGroup;
  toggleAppointment = false;
  hasDevice = false;
  selectedValue: any;
  selectedValue2: any;
  hasDeviceDetails = true;
  userStaffId: string;
  loading: boolean;
  buttonDisable: boolean;

  // For image capture
  @ViewChild('video')
  public video: ElementRef;

  @ViewChild('canvas')
  public canvas: ElementRef;

  public captures: Array<any>;
  constructor(private _formBuilder: FormBuilder, private signinService: SigninService, private toastr: ToastrService) { 

        this.captures = [];
        // Instantiate form
    this.createForm();
        this.signinService.getBranchUsersData();
        setTimeout(() => {
          this.signinService.allBranchUser.subscribe(res =>{
            this.setFilteredList(res);
          });
        }, 1000);

  }

  ngOnInit() {
    // Instantiate conditional validators
    this.formControlValueChanged();

  }
  // Filter staff on host name field
  private _filter(value: any): any[] {
    const filterValue = value.toLowerCase();
    this.signinService.allBranchUser.subscribe(res => {
      this.allBranchUser = res.filter(option => option.staffName.toLowerCase().includes(filterValue));

    });
    return this.allBranchUser;
  }

  onSelect(user) {
    this.userStaffId = user.staffId;
    if (this.toggleAppointment) {
      this.getSelectedOptionValue2(user.staffId);
    }
  }

  setFilteredList(m) {
    this.allBranchUser = m;
    if (this.allBranchUser) {
      this.filteredList = this.signInForm.controls['formArray'].get('1.hostId').valueChanges
      .pipe(
        startWith(''),
        map(inputValue => (inputValue === '' ? this.allBranchUser.slice()
        : this.filterListWithUserInput(inputValue)).slice())
      );
    }
  }

  filterListWithUserInput(value: string): BranchUser[] {
    const filterValue = value ? value.toLowerCase() : '';
    return this.allBranchUser.filter(user => user.staffName.toLowerCase().indexOf(filterValue) > -1);
  }

  clearInput() {
    this.signInForm.controls['formArray'].get('1.hostId').reset()
  }

  // Create form
  createForm() {
    this.signInForm = this._formBuilder.group({
      formArray: this._formBuilder.array([
        this.appointmentsSubForm = this._formBuilder.group({
          hasAppointment: [ , [Validators.required]]

        }),
        this.personalDetailsSubForm = this._formBuilder.group({
          hostId: [''],
          appointmentId: [''],
          visitorNumber: ['' , [Validators.required, Validators.minLength(11), Validators.maxLength(11)]],
          visitorFullName: ['', [Validators.required, Validators.minLength(3)]],
          visitorAddress: ['', [Validators.required, Validators.minLength(3)]],
          visitorEmail: ['', [Validators.minLength(5)]],
          purpose: [, [Validators.required]],
          pointOfEntry: ['', [Validators.required]],
          tagNo: ['', [Validators.required]]
        }),
        this.deviceSubForm = this._formBuilder.group({
          hasDevice: ['', [Validators.required]],
          deviceType: ['', [Validators.required]],
          deviceColor: ['', [Validators.required]],
          deviceSerialNo: ['', [Validators.required]],
          deviceDescription: ['', [Validators.required, Validators.minLength(3)]]
        }),
        this.captureImageSubForm = this._formBuilder.group({
          imageBase64: ['', [Validators.required]]
        }),
        this.captureFingerprintSubForm = this._formBuilder.group({
          fingerPrintBase64: ['', [Validators.required]]
        })
      ])
    });
  }


// Conditional validators for hasDevice and deviceType
  formControlValueChanged() {
    const deviceTypeControl = this.signInForm.controls['formArray'].get('2.deviceType');
    const deviceColorControl = this.signInForm.controls['formArray'].get('2.deviceColor');
    const deviceSerialNoControl = this.signInForm.controls['formArray'].get('2.deviceSerialNo');
    const deviceDescriptionControl = this.signInForm.controls['formArray'].get('2.deviceDescription');
    this.signInForm.controls['formArray'].get('2.hasDevice').valueChanges.subscribe(
        hasDevice => {
            if (hasDevice === 'yes') {
               deviceColorControl.setValidators([Validators.required]);
               deviceSerialNoControl.setValidators([Validators.required]);
              this.signInForm.controls['formArray'].get('2.deviceType').valueChanges.subscribe(
                deviceType => {
                  if (hasDevice === 'yes' && deviceType === 'Other'){
                    deviceColorControl.setValidators(null);
                    deviceSerialNoControl.setValidators(null);
                    deviceDescriptionControl.setValidators([Validators.required]);
                  }
                  if (hasDevice === 'yes' && deviceType !== 'Other'){
                    deviceDescriptionControl.setValidators(null);
                    deviceColorControl.setValidators([Validators.required]);
                    deviceSerialNoControl.setValidators([Validators.required]);
                  }
                });
            }
             if (hasDevice === 'no') {
              deviceTypeControl.setValidators(null);
              deviceColorControl.setValidators(null);
              deviceSerialNoControl.setValidators(null);
              deviceDescriptionControl.setValidators(null);
            }
            deviceTypeControl.updateValueAndValidity();
            deviceColorControl.updateValueAndValidity();
            deviceSerialNoControl.updateValueAndValidity();
            deviceDescriptionControl.updateValueAndValidity();
        });
}


  // Passing validation variables to the template
  get vn() { return this.signInForm.controls['formArray'].get('1.visitorNumber'); }
  get vf() { return this.signInForm.controls['formArray'].get('1.visitorFullName'); }
  get va() { return this.signInForm.controls['formArray'].get('1.visitorAddress'); }
  get ve() { return this.signInForm.controls['formArray'].get('1.visitorEmail'); }
  get p() { return this.signInForm.controls['formArray'].get('1.purpose'); }
  get pe() { return this.signInForm.controls['formArray'].get('1.pointOfEntry'); }
  get tn() { return this.signInForm.controls['formArray'].get('1.tagNo'); }
  get dt() { return this.signInForm.controls['formArray'].get('2.deviceType'); }
  get dd() { return this.signInForm.controls['formArray'].get('2.deviceDescription'); }
  get dc() { return this.signInForm.controls['formArray'].get('2.deviceColor'); }
  get ds() { return this.signInForm.controls['formArray'].get('2.deviceSerialNo'); }
  // get fp() { return this.signInForm.controls['formArray'].get('4.fingerPrintBase64'); }

  // Required by capture image
  public ngAfterViewInit() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
        this.video.nativeElement.srcObject = stream;
        this.video.nativeElement.play();
      });
    }
  }

  // Capture visitor image and finger print
  public capture() {
    let context = this.canvas.nativeElement.getContext('2d').drawImage(this.video.nativeElement, 0, 0, 640, 480);
    this.captures.push(this.canvas.nativeElement.toDataURL('image/png'));
    let image: string = this.captures[this.captures.length - 1];
    this.signInForm.controls['formArray'].get('3.imageBase64').patchValue(image);
    const fingerprintImage = localStorage.getItem("imageSrc");
    this.signInForm.controls['formArray'].get('4.fingerPrintBase64').patchValue(fingerprintImage);
    

  }

   

  //Has device function
  setValidatorAndField(data) {
    if (data === 'yes') {
      this.hasDevice = true;
      let f: string = "yes";
      this.signInForm.controls['formArray'].get('2.hasDevice').patchValue(f);

    }
    if (data === 'no') {
      this.hasDevice = false;
      const f = 'no';
      this.signInForm.controls['formArray'].get('2.hasDevice').patchValue(f);
      this.signInForm.controls['formArray'].get('2.deviceType').patchValue('');
      this.signInForm.controls['formArray'].get('2.deviceColor').patchValue('');
      this.signInForm.controls['formArray'].get('2.deviceDescription').patchValue('');
      this.signInForm.controls['formArray'].get('2.deviceSerialNo').patchValue('');

    }
  }


  //Has appointment function
  toggleAppointmentButton(data) {
    if (data == "true") {
      let d: boolean = true;
      this.toggleAppointment = true;
      this.signInForm.controls['formArray'].get('0.hasAppointment').patchValue(d);
      // Disable input if there is an appointment
      this.signInForm.controls['formArray'].get('1.visitorNumber').disable({onlySelf: true});
      this.signInForm.controls['formArray'].get('1.visitorFullName').disable({onlySelf:true});
      this.signInForm.controls['formArray'].get('1.visitorEmail').disable({onlySelf:true});
      this.signInForm.controls['formArray'].get('1.visitorAddress').disable({onlySelf:true});
      this.signInForm.controls['formArray'].get('1.purpose').disable({onlySelf:true});
      this.signInForm.controls['formArray'].get('1.pointOfEntry').disable({onlySelf:true});
    } else if (data == "false") {
      let d: boolean = false;
      this.toggleAppointment = false;
      this.signInForm.controls['formArray'].get('0.hasAppointment').patchValue(d);
       // Enable input if there is no appointment
       this.signInForm.controls['formArray'].get('1.visitorNumber').enable({onlySelf: true});
       this.signInForm.controls['formArray'].get('1.visitorFullName').enable({onlySelf:true});
       this.signInForm.controls['formArray'].get('1.visitorEmail').enable({onlySelf:true});
       this.signInForm.controls['formArray'].get('1.visitorAddress').enable({onlySelf:true});
       this.signInForm.controls['formArray'].get('1.purpose').enable({onlySelf:true});
       this.signInForm.controls['formArray'].get('1.pointOfEntry').enable({onlySelf:true});
       
       // Reset fields  if there is no appointment
       this.signInForm.controls['formArray'].get('1.hostId').reset();
       this.signInForm.controls['formArray'].get('1.visitorNumber').reset();
       this.signInForm.controls['formArray'].get('1.visitorFullName').reset();
       this.signInForm.controls['formArray'].get('1.visitorEmail').reset();
       this.signInForm.controls['formArray'].get('1.visitorAddress').reset();
       this.signInForm.controls['formArray'].get('1.purpose').reset();
       this.signInForm.controls['formArray'].get('1.pointOfEntry').reset();
    }

  }


  //Get selected value of devices
  getSelectedOptionValue(value) {
    if (value == "Other") {
      this.hasDeviceDetails = false;
      this.signInForm.controls['formArray'].get('2.deviceColor').patchValue('');
      this.signInForm.controls['formArray'].get('2.deviceSerialNo').patchValue('');
    } else  if (value !== "Other") {
      this.hasDeviceDetails = true;
      this.signInForm.controls['formArray'].get('2.deviceDescription').patchValue('');
    }

  }


  //Get selected hostnames' staffID
  getSelectedOptionValue2(staffId){
   let data2 : any = {
      staffId : staffId
     };
    setTimeout(() => {
      this.signinService.getUserPendingAppointmentData(data2);
    this.signinService.UserPendingAppointment.subscribe(res => {
      this.UserPendingAppointment = res;
    });
    }, 1000);
  }


  //Get selectedappointments ID
  getSelectedOptionValue3() {
    let appointmentId = this.signInForm.controls['formArray'].get('1.appointmentId').value;
    if (appointmentId != '') {
      this.signinService.getAppointmentDetails(appointmentId).subscribe(res => {
        if (res.code == "00") {
          let purpose = (res.data.Purpose).toString();
          this.signInForm.controls['formArray'].get('1.visitorFullName').setValue(res.data.VisitorName);
          this.signInForm.controls['formArray'].get('1.visitorEmail').setValue(res.data.Email);
          this.signInForm.controls['formArray'].get('1.visitorNumber').setValue(res.data.VisitorPhoneNumber);
          this.signInForm.controls['formArray'].get('1.appointmentId').setValue(res.data.AppointmentId);
          this.signInForm.controls['formArray'].get('1.visitorAddress').setValue(res.data.VisitorCompany);
          this.signInForm.controls['formArray'].get('1.purpose').setValue(purpose);
          this.signInForm.controls['formArray'].get('1.pointOfEntry').setValue(res.data.BranchName);
        } else {
          this.toastr.error('Error, occured while getting appointment details', 'Oops!' );
        }
      });
    }
  }


  // Submits form data
  postSignin() {
    this.signinService.updateLoader(true);
    this.signinService.loader$.subscribe( state => {
      this.loading = state;
      this.buttonDisable = state;
    });
    let p = this.signInForm.controls['formArray'].get('1.purpose').value;
    let p2 = +p;
    const fingerprintImage2 = localStorage.getItem('imageSrc');
    this.signInForm.controls['formArray'].get('4.fingerPrintBase64').patchValue(fingerprintImage2);

    const postSignin = new SigninModel(
      this.signInForm.controls['formArray'].get('1.visitorFullName').value,
      this.signInForm.controls['formArray'].get('1.visitorNumber').value,
      p2,
      this.signInForm.controls['formArray'].get('1.pointOfEntry').value,
      this.signInForm.controls['formArray'].get('2.deviceType').value,
      this.signInForm.controls['formArray'].get('0.hasAppointment').value,
      this.signInForm.controls['formArray'].get('2.deviceSerialNo').value,
      this.signInForm.controls['formArray'].get('1.visitorEmail').value,
      this.signInForm.controls['formArray'].get('1.visitorAddress').value,
      this.userStaffId,
      this.signInForm.controls['formArray'].get('1.tagNo').value,
      this.signInForm.controls['formArray'].get('2.deviceType').value,
      this.signInForm.controls['formArray'].get('2.deviceColor').value,
      this.signInForm.controls['formArray'].get('2.deviceType').value,
      this.signInForm.controls['formArray'].get('1.appointmentId').value,
      this.signInForm.controls['formArray'].get('3.imageBase64').value,
      this.signInForm.controls['formArray'].get('4.fingerPrintBase64').value,
      this.signInForm.controls['formArray'].get('2.deviceDescription').value,
      this.signInForm.controls['formArray'].get('2.hasDevice').value

    );
    setTimeout(() => {
      this.signinService.PostsigninForm(postSignin).subscribe(res => {

        this.getResponseMessage(res);
      });
    }, 2000);

  }


  // Get response message from api after form submission
  getResponseMessage(response) {
    this.signinService.updateLoader(false);
    if (response.code === '00') {

      this.toastr.success('Visitor Signed in successfully!', 'Success!');
      setTimeout(() => {
        // this.signInForm.controls['formArray'].reset();
         location.reload();
      }, 2000);
    } else if (response.code !== '00') {
      this.toastr.error('Error occured while signing in visitor!', 'Oops!');
    }

  }


}

