//Signin model
export class SigninModel {
    VisitorFullName:string;
	VisitorPhoneNumber:string;
	Purpose:number;
	BranchName:string;
	LaptopName:string;
	IsAppointment: boolean;
	SerialNumber:string;
	//VisitorCompany:string;
	Email:string;
	VisitorAddress:string;
	HostId:string;
	TagNumber:string;
	DeviceType:string;
	DeviceColor: string;
	DeviceName: string;
	AppointmentId:string;
	VisitImage:string;
	FingerPrintImage:string;
	OtherDeviceDescription:string;
    HasDevice: string;
    
    constructor(VisitorFullName: string, VisitorPhoneNumber:string, Purpose: number, BranchName:string, LaptopName:string, 
        IsAppointment:boolean, SerialNumber:string, Email:string, VisitorAddress:string, HostId:string,TagNumber:string,
         DeviceType:string,DeviceColor:string, DeviceName:string,AppointmentId:string,VisitImage:string,FingerPrintImage:string,OtherDeviceDescription:string,HasDevice:string){
             this.VisitorFullName = VisitorFullName,
             this.VisitorPhoneNumber = VisitorPhoneNumber,
             this.Purpose = Purpose,
             this.BranchName = BranchName,
             this.LaptopName = LaptopName,
             this.IsAppointment = IsAppointment,
             this.SerialNumber = SerialNumber,
             //this.VisitorCompany = VisitorCompany,
             this.Email = Email,
             this.VisitorAddress = VisitorAddress,
             this.HostId = HostId,
             this.TagNumber = TagNumber,
             this.DeviceType = DeviceType,
             this.DeviceColor = DeviceColor,
             this.DeviceName = DeviceName,
             this.AppointmentId = AppointmentId,
             this.VisitImage = VisitImage,
             this.FingerPrintImage = FingerPrintImage,
             this.OtherDeviceDescription = OtherDeviceDescription
             this.HasDevice = HasDevice

         }
}


//Branch user model
export class BranchUser {
    staffId : string;
    staffName : string;
    branchName : string;
    constructor(staffId : string, staffName: string, branchName : string){
        this.staffId = staffId,
        this.staffName =staffName,
        this.branchName = branchName
    }

    }

    //Pending appointment model
    export class UserPendingAppointments {
        
            VisitorPhoneNumber: string;
            Purpose: number;
            AppointmentId: number;
            VisitorCompany: string;
            Email: string;
            VisitorFullName: string;
           
            
        constructor(VisitorPhoneNumber: string, Purpose: number, AppointmentId: number,VisitorCompany: string,Email: string,VisitorFullName: string){
            this.VisitorPhoneNumber =  VisitorPhoneNumber,
            this.Purpose = Purpose,
            this.AppointmentId = AppointmentId,
            this.VisitorCompany = VisitorCompany,
            this.Email = Email,
            this.VisitorFullName = VisitorFullName
        }
    }