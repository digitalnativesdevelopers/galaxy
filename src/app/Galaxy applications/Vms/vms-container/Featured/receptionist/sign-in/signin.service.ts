import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { BranchUser, UserPendingAppointments } from './signin-model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SigninService {

  // API urls
  BASE_URL = environment.galaxy_vms_baseUrl;
  getAppointmentDetailsUrl = `${this.BASE_URL}appointments/details/`;
  getBranchUsersUrl = `${this.BASE_URL}users/getbranchusers`;
  postSigninUrl = `${this.BASE_URL}visitors/signin`;
  getUserPendingAppointmentUrl = `${this.BASE_URL}appointments/getuserpendingappointments`;

  UserPendingAppointment: Subject<UserPendingAppointments[]> = new BehaviorSubject<any>(null);
  allBranchUser: Subject<BranchUser[]> = new BehaviorSubject<any>(null);

  private loader = new BehaviorSubject<any>(false);
  loader$ = this.loader.asObservable();

  constructor(private http: HttpClient) { }

  // getBranchUsers
  getBranchUsers(): Observable<any> {
    return this.http.get(this.getBranchUsersUrl);
  }

  getAppointmentDetails(id): Observable<any> {
    return this.http.get(this.getAppointmentDetailsUrl + id);
  }

  getBranchUsersData() {
    this.getBranchUsers().subscribe(res => {
      this.extractData(res.data);
    });
  }

  // extractData for getBranchUsers
  extractData(data) {
    const branchUser: BranchUser[] = [];

    for (const s of data) {
      branchUser.push(new BranchUser(s.staff_ID, s.staff_name, s.branch_name));

    }
    this.allBranchUser.next(branchUser);
}

// UserPendingAppointment
getUserPendingAppointment(data2): Observable<any> {
  return this.http.post(this.getUserPendingAppointmentUrl, data2);
}

getUserPendingAppointmentData(data2) {
  this.getUserPendingAppointment(data2).subscribe(res => {
    this.extractUserPendingAppointmentData(res.data);
  });
}

// extractData for getBranchUsers
extractUserPendingAppointmentData(data) {
  const appointments: UserPendingAppointments[] = [];
  for (const s of data) {
   // tslint:disable-next-line:max-line-length
   appointments.push(new UserPendingAppointments(s.VisitorPhoneNumber, s.Purpose, s.AppointmentId, s.VisitorCompany, s.Email, s.VisitorFullName));

  }
  this.UserPendingAppointment.next(appointments);

}

// PostsigninForm
PostsigninForm(PostsigninFormBody): Observable<any> {
  return this.http.post(this.postSigninUrl, PostsigninFormBody);
}

// call pageloader
updateLoader (state) {
  this.loader.next(state);
}

}
