import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { environment } from '../../../../../../environments/environment';
import { map, retry, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class VisitorLogService {

  paremeter = {
    'visitStatus': 1,
    'displayLength': 5,
    'displayStart': 0,
    'startDate': '05/29/2018 14:50',
    'endDate': '05/29/2019 14:50'
    };

    private visitorLog = new BehaviorSubject<any>(null);
    visitorLog$ = this.visitorLog.asObservable();
    private Loader = new BehaviorSubject<any>(false);
    loader$ = this.Loader.asObservable();

  constructor( private htpp: HttpClient, private toastr: ToastrService ) { }

  _visitorLog(param): Observable<any> {
    return this.htpp.post(environment.vms_receptionist_visitorLog, param)
     .pipe (
       retry(3),
      catchError( this.handleError)
     );
  }

  // Get Visitor log
  getVisitorLog(param) {
    this.updateLoader(true);
    const visitorLog = [];
    this._visitorLog(param).subscribe(response => {
      if (response ) {
        this.updateLoader(false);
        for (const visitor of response.data) {
          visitorLog.push(
            {
              'BranchName': visitor.BranchName,
              'VisitorCompany': visitor.VisitorCompany,
              'VisitorName': visitor.VisitorFullName,
              'Email': visitor.Email,
              'PhoneNumber': visitor.VisitorPhoneNumber,
              'Purpose': visitor.Purpose,
              'TimeIn': visitor.TimeIn,
              'TimeOut': visitor.TimeOut,
              'TagNumber': visitor.TagNumber
            }
          );
        }
        this.visitorLog.next(visitorLog);
        this.updateLoader(false);
      }
    },
    (err: HttpErrorResponse) => {
      this.updateLoader(false);
      this.toastr.error('Error fetching your record, please try again later');
      console.log(err);
    }
    );
  }

  handleError( error: HttpErrorResponse) {
    return throwError(error);
  }

  updateLoader(state) {
    this.Loader.next(state);
  }
}
