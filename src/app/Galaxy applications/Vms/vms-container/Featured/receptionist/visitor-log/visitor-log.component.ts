import { Component, OnInit, ViewChild } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatDatepicker
} from '@angular/material';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import * as _moment from 'moment';
import { VisitorLogService } from '../visitor-log.service';

const moment = _moment;

@Component({
  selector: 'app-visitor-log',
  templateUrl: './visitor-log.component.html',
  styleUrls: ['./visitor-log.component.scss']
})
export class VisitorLogComponent implements OnInit {
  displayedColumns: string[] = ['name', 'company', 'email', 'phoneNumber', 'purpose', 'timeIn', 'timeOut' ];
  dataSource;
  // visitorsLog
  visitors;
  loading: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  createSearchForm: FormGroup;

  endDate =  new Date();
  startDate = new Date(this.endDate.getTime() - (7 * 24 * 60 * 60 * 1000));

  constructor(public fb: FormBuilder, private _visitorLogService: VisitorLogService) {
  }

  ngOnInit() {
    this._visitorLogService.getVisitorLog(
      {
        'visitStatus': 2,
        'displayLength': 5,
        'displayStart': 0,
        // 'startDate': this.startDate,
        'startDate':  '05/29/2018 14:50',
        'endDate': this.endDate
        }
    );
    this.getVisitorsLog();
    this.dataSource.paginator = this.paginator;
    this.createReactiveform();
  }

  // Filter Table
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  createReactiveform() {
    this.createSearchForm = this.fb.group({
      startDate: [this.startDate],
      endDate: [this.endDate],
      displayLength: [5],
      visitorStatus: ['Signed out']
    });
  }

  // get Visitos log
  getVisitorsLog() {
    this._visitorLogService.visitorLog$.subscribe( visitors => {
      this.visitors = visitors;
      this.dataSource = new MatTableDataSource<Visitor>(this.visitors);
      this._visitorLogService.loader$.subscribe( res => {
        this.loading = res;
      });
    });
  }

  fetchVisitorLog(formValue) {
    let visitStatus: number;
    let requestParam = {};
    let  startDate;
    let endDate;
    if (formValue.visitorStatus === 'Signed in') {
      visitStatus = 1;
    } else if ( formValue.visitorStatus === 'Signed out') {
      visitStatus = 2;
    }
    startDate = this.convert(formValue.startDate) + ' ' + '00:00';
    endDate = this.convert(formValue.endDate) + ' ' + '23:59';
    requestParam =  {
      'visitStatus': visitStatus,
      'displayLength': formValue.displayLength,
      'displayStart': 0,
      'startDate':  startDate,
      'endDate': endDate
      };
      this._visitorLogService.getVisitorLog(requestParam);
      this.getVisitorsLog();
  }

  // Format date
convert(str) {
  const date = new Date(str),
  mnth = ('0' + (date.getMonth() + 1)).slice(-2),
  day = ('0' + date.getDate()).slice(-2);
  return [ mnth, day, date.getFullYear()].join('/');
  }
}

// Dummy Data class
export interface Visitor {
  name: string;
  company: number;
  email: number;
  phoneNumber: string;
  purpose: string;
  timeIn: string;
  timeOut: string;
  tagNumber: string;
}

