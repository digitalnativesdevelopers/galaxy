import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SignoutService } from '../signout.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-out',
  templateUrl: './sign-out.component.html',
  styleUrls: ['./sign-out.component.scss']
})
export class SignOutComponent implements OnInit {
  signOutForm: FormGroup;
  deviceType: string;
  deviceSerialNo: string;
  visitorImage: string;
  picture: string;
  loading: boolean;
  constructor( private fb: FormBuilder,
    private signoutServ: SignoutService,
    private router: Router
    ) {
  }

  onEnter(value) {
    const details = {
      'staffID': 'P7569',
      'tagNumber': value
    };
    this.signoutServ.subFetchvisitorDetails(details);
    this.signoutServ.visitorDetails$.subscribe( res => {
      if ( res ) {
        this.signoutServ.loader$.subscribe( state => {
          this.loading = state;
        });
        this.signOutForm.controls['visitorName'].setValue(res.visitorFullName);
        this.signOutForm.controls['phoneNumber'].setValue(res.visitorPhoneNumber);
        this.signOutForm.controls['email'].setValue(res.visitorEmail);
        this.deviceType = res.deviceType;
        this.deviceSerialNo = res.deviceSerialNo;
        this.visitorImage = res.visitorImage;
      }
    });
  }

  signOut(tagNo) {
    this.signoutServ.subSignVisitorOut({'tagNumber' : tagNo});
    this.router.navigateByUrl('/TruVisit/receptionist');
  }
  ngOnInit() {
    // create signOut form
    this.signOutForm = this.fb.group({
      tagNumber: [null, Validators.required],
      visitorName: [null],
      phoneNumber: [null],
      email: [null],
      devices: [null]
    });
  }

}
