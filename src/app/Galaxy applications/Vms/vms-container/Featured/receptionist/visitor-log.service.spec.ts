import { TestBed } from '@angular/core/testing';

import { VisitorLogService } from './visitor-log.service';

describe('VisitorLogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VisitorLogService = TestBed.get(VisitorLogService);
    expect(service).toBeTruthy();
  });
});
