import { Component, OnInit } from '@angular/core';
import { VmsAuthenticationService } from '../../../../Shared/auth/vms-authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isSuperAdmin: boolean;
  isAdmin: boolean;

  constructor(private auth: VmsAuthenticationService) { }

  ngOnInit() {
    this.getStaffRole();
  }

  getStaffRole() {
    this.isSuperAdmin = this.auth.isSuperAdmin;
    this.isAdmin = this.auth.isAdmin;
  }

}
