import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReceptionistComponent } from './receptionist.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignOutComponent } from './sign-out/sign-out.component';
import { HomeComponent } from './home/home.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { ReportNonComplianceComponent } from './report-non-compliance/report-non-compliance.component';
import { VisitorLogComponent } from './visitor-log/visitor-log.component';
import { ReceptionistGuard } from '../../Shared/Guards/receptionist.guard';

const routes: Routes = [
  {
    path: '', component: ReceptionistComponent, canActivate: [ReceptionistGuard],
    // {path: 'receptionist', component: ReceptionistComponent,
    children: [
      // {path: '', redirectTo: 'home', pathMatch: 'full'},
      { path: '', component: HomeComponent },
      { path: 'signin', component: SignInComponent },
      { path: 'signout', component: SignOutComponent },
      { path: 'appointments', component: AppointmentsComponent },
      { path: 'statistics', component: StatisticsComponent },
      { path: 'non-compliance', component: ReportNonComplianceComponent },
      {path: 'visitorlog', component: VisitorLogComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceptionistRoutingModule { }
