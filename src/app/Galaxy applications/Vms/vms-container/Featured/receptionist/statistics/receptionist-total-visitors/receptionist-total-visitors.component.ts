import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ReceptionistTotalVisitorsService } from './receptionist-total-visitors.service';

@Component({
  selector: 'app-receptionist-total-visitors',
  templateUrl: './receptionist-total-visitors.component.html',
  styleUrls: ['./receptionist-total-visitors.component.scss']
})
export class ReceptionistTotalVisitorsComponent implements OnInit {

  totalNumberOfVisitors: number;


  constructor(private visitorService: ReceptionistTotalVisitorsService) { }

  ngOnInit() {
    this.visitorService.getTotalNoOfVisitors();
    this.getTotalVisitors();
  }

  // Get count of all visitors

  getTotalVisitors() {
    this.visitorService.visitorsCount.subscribe(res => {
      this.totalNumberOfVisitors = res;
    });
  }

}
