import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceptionistTotalVisitorsComponent } from './receptionist-total-visitors.component';

describe('ReceptionistTotalVisitorsComponent', () => {
  let component: ReceptionistTotalVisitorsComponent;
  let fixture: ComponentFixture<ReceptionistTotalVisitorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceptionistTotalVisitorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceptionistTotalVisitorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
