import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceptionistVisitorsOnAppointmentComponent } from './receptionist-visitors-on-appointment.component';

describe('ReceptionistVisitorsOnAppointmentComponent', () => {
  let component: ReceptionistVisitorsOnAppointmentComponent;
  let fixture: ComponentFixture<ReceptionistVisitorsOnAppointmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceptionistVisitorsOnAppointmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceptionistVisitorsOnAppointmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
