import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ReceptionistMostVisitedBlockService } from './receptionist-most-visited-block.service';

@Component({
  selector: 'app-receptionist-most-visited-block',
  templateUrl: './receptionist-most-visited-block.component.html',
  styleUrls: ['./receptionist-most-visited-block.component.scss']
})
export class ReceptionistMostVisitedBlockComponent implements OnInit {


  totalVisitorsForBlocks: [];

  public barChartLabels: string[];
  public barChartData: number[] = [];
  public barChartType = 'horizontalBar';

  constructor(
    public blocksService: ReceptionistMostVisitedBlockService) {
  }

  ngOnInit() {
    this.blocksService.getTotalNoOfVisitors();
    this.getAllBlocksVisitors();
  }

  getAllBlocksVisitors() {
    this.blocksService.blockCount.subscribe(res => {
      if ( res ) {
        this.barChartLabels = res.label;
        this.barChartData.push(res.data);
      } else  {
        console.log('error: res is turnioniown');
      }
    });
  }
}
