import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceptionistRecentVisitorsComponent } from './receptionist-recent-visitors.component';

describe('ReceptionistRecentVisitorsComponent', () => {
  let component: ReceptionistRecentVisitorsComponent;
  let fixture: ComponentFixture<ReceptionistRecentVisitorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceptionistRecentVisitorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceptionistRecentVisitorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
