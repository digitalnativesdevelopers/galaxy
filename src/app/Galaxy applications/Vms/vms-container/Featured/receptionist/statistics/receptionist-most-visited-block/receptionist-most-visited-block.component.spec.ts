import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceptionistMostVisitedBlockComponent } from './receptionist-most-visited-block.component';

describe('ReceptionistMostVisitedBlockComponent', () => {
  let component: ReceptionistMostVisitedBlockComponent;
  let fixture: ComponentFixture<ReceptionistMostVisitedBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceptionistMostVisitedBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceptionistMostVisitedBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
