import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceptionistMostVisitedMonthComponent } from './receptionist-most-visited-month.component';

describe('ReceptionistMostVisitedMonthComponent', () => {
  let component: ReceptionistMostVisitedMonthComponent;
  let fixture: ComponentFixture<ReceptionistMostVisitedMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceptionistMostVisitedMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceptionistMostVisitedMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
