import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

// const httpOptions = {
//   headers: new HttpHeaders({
//     'Content-Type':  'application/json',
//     'Access-Control-Allow-Origin': '<origin> | *'
//   })
// };

@Injectable({
  providedIn: 'root'
})
export class ReceptionistSignedInService {

  signedInCount: Subject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }

  getSignedIn() {
    return this.http.get(environment.vms_get_receptionist_dashboard_card_items);
  }

  getTotalNoOfSignedIn() {
    this.getSignedIn().subscribe( res => {
      this.extractSignedIn(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

  extractSignedIn(response) {
    if (response) {
      let numberOfSignedIn: number ;
      numberOfSignedIn = response.TotalSignedInVisitorToday;
      this.signedInCount.next(numberOfSignedIn);
    }
  }

}
