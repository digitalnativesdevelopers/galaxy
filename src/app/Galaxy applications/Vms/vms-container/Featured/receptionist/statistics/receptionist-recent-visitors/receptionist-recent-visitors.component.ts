import { Component, OnInit } from '@angular/core';
import { ReceptionistRecentVisitorsService } from './receptionist-recent-visitors.service';

@Component({
  selector: 'app-receptionist-recent-visitors',
  templateUrl: './receptionist-recent-visitors.component.html',
  styleUrls: ['./receptionist-recent-visitors.component.scss']
})
export class ReceptionistRecentVisitorsComponent implements OnInit {

  RecentVisitors: [];
  

  public emptyMessage: string= 'No Staff has been visited in this branch';

  constructor(
  private recentService: ReceptionistRecentVisitorsService) {
  }

  ngOnInit() {
    this.recentService.getTotalNoOfVisitors();
    this.getTotalRecentVisitors();
  }

  getTotalRecentVisitors() {
    this.recentService.recentVisitorsCount.subscribe(res => {
      if (res) {
        let recentVisitorsCount: number;
        recentVisitorsCount = res.length;
        // console.log('this is the lenght of the visitors', recentVisitorsCount);
        if( recentVisitorsCount != 0) {
          this.RecentVisitors = res;
        }
      }
    });
  }

}

//   getMonthlyVisits() {
//     this.visitedEmployeeService.busiestEmployeeCount.subscribe(res => {
//       this.monthlyEmployeeVisits = res;
//       if ( res ) {
        
//         if (res != null) {
          
//           this.barChartData.push(res.data);
//           this.barChartLabels = res.label;
//         }
//       } else {
//         console.log('err: res is turnioniown');
//       }
//     });
//   }

// }
