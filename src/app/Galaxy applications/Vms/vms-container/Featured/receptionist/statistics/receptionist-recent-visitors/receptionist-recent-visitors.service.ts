import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ReceptionistRecentVisitorsService {

  recentVisitorsCount: Subject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }


  getRecentVisitors() {
    return this.http.get(environment.vms_get_receptionist_dashboard_card_items);
  }


  extractTotalVisitors(response) {
    if (response) {
      let numberOfRecentVisitors: [] ;
      numberOfRecentVisitors = response.MostRecentVisitors;
      this.recentVisitorsCount.next(numberOfRecentVisitors);
    }
  }

  getTotalNoOfVisitors() {
    this.getRecentVisitors().subscribe( res => {
      // console.log(res);
      this.extractTotalVisitors(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

}