import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '<origin> | *'
  })
};


@Injectable({
  providedIn: 'root'
})
export class ReceptionisGettMostVisitedMonthService {


  busiestMonthCount: Subject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }

  getBusyMonth() {
    return this.http.get(environment.vms_receptionist_most_visited_month);
  }

  extractMonthlyVisitors(res) {
    if (res) {
      const data: any = [];
      const label: any = [];
      let chartValues;
      let numberOfMonthlyVisitors = [] ;
      numberOfMonthlyVisitors = res;

      for ( const monthlyData of numberOfMonthlyVisitors) {
        data.push ( monthlyData.y);
      }

      for (const monthlyDataLabel of numberOfMonthlyVisitors) {
        label.push( monthlyDataLabel.x);
      }

      chartValues = { 'data': data, 'label': label};
      this.busiestMonthCount.next(chartValues);
    }
  }

  getTotalNoOfVisitors() {
    this.getBusyMonth().subscribe( res => {
      this.extractMonthlyVisitors(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

}
