import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ReceptionisGettMostVisitedMonthService } from './receptionis-gett-most-visited-month.service';

@Component({
  selector: 'app-receptionist-most-visited-month',
  templateUrl: './receptionist-most-visited-month.component.html',
  styleUrls: ['./receptionist-most-visited-month.component.scss']
})
export class ReceptionistMostVisitedMonthComponent implements OnInit {

  monthlyEmployeeVisits: [];

  public barChartLabels: string[];
  public barChartData: number[] = [];
  public barChartType = 'bar';

  constructor(
  private busyMonthService: ReceptionisGettMostVisitedMonthService) {
  }

  ngOnInit() {
    this.busyMonthService.getTotalNoOfVisitors();
    this.getAllMonthsVisitors();
  }

  getAllMonthsVisitors() {
    this.busyMonthService.busiestMonthCount.subscribe(res => {
      this.monthlyEmployeeVisits = res;
      if ( res ) {
        const months = res.label;
        console.log ('months', months);
        this.barChartData.push(res.data);
        this.barChartLabels = months.reverse();
      } else {
        console.log('err: res is turnioniown');
      }
    });
  }

}
