import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceptionistMostVisitedEmployeeComponent } from './receptionist-most-visited-employee.component';

describe('ReceptionistMostVisitedEmployeeComponent', () => {
  let component: ReceptionistMostVisitedEmployeeComponent;
  let fixture: ComponentFixture<ReceptionistMostVisitedEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceptionistMostVisitedEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceptionistMostVisitedEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
