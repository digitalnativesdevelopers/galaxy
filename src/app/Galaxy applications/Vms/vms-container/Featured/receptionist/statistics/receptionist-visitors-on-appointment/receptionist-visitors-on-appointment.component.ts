import { Component, OnInit } from '@angular/core';
import { ReceptionistAppointmentsService } from './receptionist-appointments.service';

@Component({
  selector: 'app-receptionist-visitors-on-appointment',
  templateUrl: './receptionist-visitors-on-appointment.component.html',
  styleUrls: ['./receptionist-visitors-on-appointment.component.scss']
})
export class ReceptionistVisitorsOnAppointmentComponent implements OnInit {

  totalNumberOfAppointments: number;

  constructor(private appointmentService: ReceptionistAppointmentsService) { }

  ngOnInit() {
    this.appointmentService.getTotalNoOfAppointments();
    this.getTotalAppointments();
  }

  getTotalAppointments() {
    this.appointmentService.appointmentCount.subscribe(res => {
      this.totalNumberOfAppointments = res;
    });
  }

}
