import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ReceptionistMostVisitedStaffService } from './receptionist-most-visited-staff.service';

@Component({
  selector: 'app-receptionist-most-visited-employee',
  templateUrl: './receptionist-most-visited-employee.component.html',
  styleUrls: ['./receptionist-most-visited-employee.component.scss']
})
export class ReceptionistMostVisitedEmployeeComponent implements OnInit {

  monthlyEmployeeVisits: [];

  public emptyMessage: string= 'No Staff has been visited in this branch';

  public barChartLabels: string[];
  public barChartData: number[] = [];
  public barChartType = 'doughnut';

  constructor(
    private visitedEmployeeService: ReceptionistMostVisitedStaffService) {
  }

  ngOnInit() {
    this.visitedEmployeeService.getTotalNoOfVisitors();
    this.getMonthlyVisits();
  }

  getMonthlyVisits() {
    this.visitedEmployeeService.busiestEmployeeCount.subscribe(res => {
      this.monthlyEmployeeVisits = res;
      if ( res ) {
        
        if (res != null) {
          
          this.barChartData.push(res.data);
          this.barChartLabels = res.label;
        }
      } else {
        console.log('err: res is turnioniown');
      }
    });
  }

}
