import { Component, OnInit } from '@angular/core';
import { ReceptionistSignedInService } from './receptionist-signed-in.service';

@Component({
  selector: 'app-receptionist-signed-in',
  templateUrl: './receptionist-signed-in.component.html',
  styleUrls: ['./receptionist-signed-in.component.scss']
})
export class ReceptionistSignedInComponent implements OnInit {

  totalNumberOfSignedIn: number;

  constructor(private signedInService: ReceptionistSignedInService) { }

  ngOnInit() {
    this.signedInService.getTotalNoOfSignedIn();
    this.getTotalSignedIn();
  }
  getTotalSignedIn() {
    this.signedInService.signedInCount.subscribe(res => {
      this.totalNumberOfSignedIn = res;
    });
  }
}

