import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceptionistSignedInComponent } from './receptionist-signed-in.component';

describe('ReceptionistSignedInComponent', () => {
  let component: ReceptionistSignedInComponent;
  let fixture: ComponentFixture<ReceptionistSignedInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceptionistSignedInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceptionistSignedInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
