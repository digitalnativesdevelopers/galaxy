import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/map';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '<origin> | *'
  })
};


@Injectable({
  providedIn: 'root'
})
export class ReceptionistMostVisitedStaffService {

  busiestEmployeeCount: Subject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }

  getBusyEmployee() {
    return this.http.get(environment.vms_receptionist_get_most_visited_staff);
  }

  extractMonthlyVisits(res) {
    if (res) {
      const data: any = [];
      const label: any = [];
      let chartValues;

      let numberOfMonthlyVisitors = [] ;
      numberOfMonthlyVisitors = res.ResponseObject;

      // console.log('monthly', numberOfMonthlyVisitors);
      // console.log(numberOfMonthlyVisitors.length);
      for ( const monthlyData of numberOfMonthlyVisitors) {
        data.push (monthlyData.NumberofVisits);
      }
      // console.log(data);
      for (const monthlyDataLabel of numberOfMonthlyVisitors) {
        label.push( monthlyDataLabel.StaffId);
      }
      // console.log(label);
      chartValues = { 'data': data, 'label': label};
      // console.log('chartValues', chartValues);
      this.busiestEmployeeCount.next(chartValues);
    }
  }

  getTotalNoOfVisitors() {
    this.getBusyEmployee().subscribe( res => {
      // console.log('res', res.ResponseObject, res.ResponseMessage);
      this.extractMonthlyVisits(res);
      // console.log('responseMonthly', this.busiestEmployeeCount.value);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

}
