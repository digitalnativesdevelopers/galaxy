import { Component, OnInit } from '@angular/core';
import { VisitorLogService } from './visitor-log.service';
import { VmsAuthenticationService } from '../../Shared/auth/vms-authentication.service';
import { AuthenticationService } from 'src/app/Auth/authentication.service';
import { Router } from '@angular/router';
import { AppoinmentsService } from './appointments/appoinments.service';


@Component({
  selector: 'app-receptionist',
  templateUrl: './receptionist.component.html',
  styleUrls: ['./receptionist.component.scss']
})
export class ReceptionistComponent implements OnInit {
  startDate;
  endDate;

  constructor(private visitorServ: VisitorLogService,
    private galaxyAuth: AuthenticationService,
    private route: Router,
    private vmsAuth: VmsAuthenticationService,
    private appointmentService : AppoinmentsService) {
    this.startDate = this.convert(new Date()) + ' ' + '08:00';
    this.endDate = this.convert (new Date(new Date().getTime() - (7 * 24 * 60 * 60 * 1000))) + ' ' + '08:00';
  }

  ngOnInit() {
    this.appointmentService.instantiateDefaultAllAppointment();
  }

  // Format date
  convert(str) {
    const date = new Date(str),
    mnth = ('0' + (date.getMonth() + 1)).slice(-2),
    day = ('0' + date.getDate()).slice(-2);
    return [ mnth, day, date.getFullYear()].join('/');
    }
}
