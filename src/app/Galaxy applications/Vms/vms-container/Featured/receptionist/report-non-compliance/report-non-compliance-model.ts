export class ReportNonComplianceModel {
    ReceptionistId : string;
    StaffIdReported : string;
    Issue : string;
    Comment : string;

    constructor(StaffIdReported : string, issue : string,comment : string,receptionistId? : string){
        this.ReceptionistId = receptionistId;
        this.StaffIdReported = StaffIdReported;
        this.Issue = issue;
        this.Comment = comment;
    }
}



export class BranchUser {
  
    staffId : string;
    staffName : string;
    branchName : string;
    constructor(staffId : string,staffName: string,branchName : string){
        this.staffId = staffId,
        this.staffName =staffName,
        this.branchName = branchName
    }

    
}