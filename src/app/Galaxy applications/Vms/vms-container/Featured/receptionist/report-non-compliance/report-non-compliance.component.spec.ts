import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportNonComplianceComponent } from './report-non-compliance.component';

describe('ReportNonComplianceComponent', () => {
  let component: ReportNonComplianceComponent;
  let fixture: ComponentFixture<ReportNonComplianceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportNonComplianceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportNonComplianceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
