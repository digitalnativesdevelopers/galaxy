import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { BranchUser } from './report-non-compliance-model';
import { retry, catchError } from 'rxjs/operators';
import { UtilitiesService } from 'src/app/utilities/utilities.service';


@Injectable({
  providedIn: 'root'
})
export class ReportNonComplianceService {
  BASE_URL = environment.galaxy_vms_baseUrl;
  postUrl = `${this.BASE_URL}users/getbranchusers`;
  postReportNonComplianceUrl = `${this.BASE_URL}vms/ReportNonCompliance`;
  allBranchUser: Subject<BranchUser[]> = new BehaviorSubject<any>(null);

  private errorSource = new BehaviorSubject<string>(null);
  error$ = this.errorSource.asObservable();

  // Observable sources: Loader
  private loaderSource = new BehaviorSubject<boolean>(false);
  loader$ = this.loaderSource.asObservable();

  constructor(private http: HttpClient, private util: UtilitiesService ) { }

  getBranchUsers(): Observable<any> {
    return this.http.get(this.postUrl).pipe(
      retry(3),
      catchError(this.util.handleError)
    );
  }

  getBranchUsersData() {
    this.getBranchUsers().subscribe(res => {
      this.extractData(res.data);
      console.log(res.data);
    },
    ( err: HttpErrorResponse) => {
      this.updateError(err);
      this.updateLoader(false);
    });
  }

  extractData(data) {
    const branchUser: BranchUser[] = [];

    for (const s of data) {
      branchUser.push(new BranchUser(s.staff_ID, s.staff_name, s.branch_name));

    }
    this.updateLoader(false);
    this.updateError(null);
    this.allBranchUser.next(branchUser);
  }

  // Post report Non-Compliance Form
  postReportNonCompliance(reportNonComplianceModel): Observable<any> {
    return this.http.post(this.postReportNonComplianceUrl, reportNonComplianceModel).pipe(
      retry(3),
      catchError(this.util.handleError)
    );
  }

  updateError(message) {
    this.errorSource.next(message);
    console.log(message);
  }

  updateLoader(state) {
    this.loaderSource.next(state);
  }
}
