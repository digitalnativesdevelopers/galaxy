import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReportNonComplianceService } from './report-non-compliance.service';
import { BranchUser, ReportNonComplianceModel } from './report-non-compliance-model';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-report-non-compliance',
  templateUrl: './report-non-compliance.component.html',
  styleUrls: ['./report-non-compliance.component.scss']
})
export class ReportNonComplianceComponent implements OnInit {
  allBranchUser: BranchUser[];
  filteredList: Observable<BranchUser[]>;
  reportComplianceForm: FormGroup;
  errorMessage: string;
  loading: boolean;
  message: string;
  userStaffId: any;
  constructor(private fb: FormBuilder, private complianceService: ReportNonComplianceService, private toastr: ToastrService) { }

  ngOnInit() {
    this.createReportComplianceForm();
    this.complianceService.getBranchUsersData();
    this.complianceService.allBranchUser.subscribe(res => {
      this.setFilteredList(res);
      // console.log(this.allBranchUser);
    });
    this.complianceService.error$.subscribe(err => this.errorMessage = err);
  }

  onSelect(user) {
    this.userStaffId = user.staffId;
  }

  setFilteredList(m) {
    this.allBranchUser = m;
    if (this.allBranchUser) {
      this.filteredList = this.reportComplianceForm.controls['staffName'].valueChanges
      .pipe(
        startWith(''),
        map(inputValue => (inputValue === '' ? this.allBranchUser.slice()
        : this.filterListWithUserInput(inputValue)).slice())
      );
    }
  }

  filterListWithUserInput(value: string): BranchUser[] {
    const filterValue = value ? value.toLowerCase() : '';
    return this.allBranchUser.filter(user => user.staffName.toLowerCase().indexOf(filterValue) > -1);
  }

  clearInput() {
    this.reportComplianceForm.controls['staffName'].reset();
  }

  createReportComplianceForm() {
    this.reportComplianceForm = this.fb.group({
      staffName: ['', Validators.required],
      pickIssue: ['', Validators.required],
      comment: ['', Validators.required]
    });
  }

  postReportNonCompliance() {
    // console.log(this.reportComplianceForm.value);
    this.complianceService.updateLoader(true);
    this.complianceService.loader$.subscribe(loader => this.loading = loader);
    if (this.reportComplianceForm.valid) {
      const staffName = this.reportComplianceForm.controls['staffName'].value;
      const issue = this.reportComplianceForm.controls['pickIssue'].value;
      const comment = this.reportComplianceForm.controls['comment'].value;
      const postReportNonCompliance = new ReportNonComplianceModel(this.userStaffId, issue, comment);

        this.complianceService.postReportNonCompliance(postReportNonCompliance).subscribe(
          res => {
            if (res.ResponseCode === '00') {
              this.complianceService.updateLoader(false);
              this.complianceService.updateError(null);
              this.toastr.success(res.ResponseMessage);
              this.reportComplianceForm.reset();
            } else {
              this.toastr.error(res.ResponseMessage);
            }
          }, (err: HttpErrorResponse) => {
            this.complianceService.updateError(err);
            this.complianceService.updateLoader(false);
          }
        );
    }
  }

}
