import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReceptionistRoutingModule } from './receptionist-routing.module';
import { StatisticsComponent } from './statistics/statistics.component';
import { SignOutComponent } from './sign-out/sign-out.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { ReportNonComplianceComponent } from './report-non-compliance/report-non-compliance.component';
import { ReceptionistComponent } from './receptionist.component';
import { SharedModule } from '../../Shared/shared.module';
import { HomeComponent } from './home/home.component';
import { CoreModule } from '../../Core/core.module';
import { NavbarComponent } from './home/navbar/navbar.component';
import { IntroImageComponent } from './home/intro-image/intro-image.component';
import { CardsComponent } from './home/cards/cards.component';
import { VisitorLogComponent } from './visitor-log/visitor-log.component';
import { FormsModule } from '@angular/forms';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

// tslint:disable-next-line:max-line-length
import { ReceptionistMostVisitedBlockComponent } from './statistics/receptionist-most-visited-block/receptionist-most-visited-block.component';
// tslint:disable-next-line:max-line-length
import { ReceptionistMostVisitedEmployeeComponent } from './statistics/receptionist-most-visited-employee/receptionist-most-visited-employee.component';
// tslint:disable-next-line:max-line-length
import { ReceptionistMostVisitedMonthComponent } from './statistics/receptionist-most-visited-month/receptionist-most-visited-month.component';
import { ReceptionistRecentVisitorsComponent } from './statistics/receptionist-recent-visitors/receptionist-recent-visitors.component';
import { ReceptionistSignedInComponent } from './statistics/receptionist-signed-in/receptionist-signed-in.component';
import { ReceptionistTotalVisitorsComponent } from './statistics/receptionist-total-visitors/receptionist-total-visitors.component';
// tslint:disable-next-line:max-line-length
import { ReceptionistVisitorsOnAppointmentComponent } from './statistics/receptionist-visitors-on-appointment/receptionist-visitors-on-appointment.component';
import { ChartsModule } from 'ng2-charts/ng2-charts';
// import { Select2Module } from 'ng2-select2';




@NgModule({
  declarations: [
    StatisticsComponent,
    SignOutComponent,
    SignInComponent,
    AppointmentsComponent,
    ReportNonComplianceComponent,
    ReceptionistComponent,
    StatisticsComponent,
    SignOutComponent,
    SignInComponent,
    AppointmentsComponent,
    ReportNonComplianceComponent,
    ReceptionistComponent,
    HomeComponent,
    NavbarComponent,
    IntroImageComponent,
    CardsComponent,
    VisitorLogComponent,
    ReceptionistMostVisitedBlockComponent,
    ReceptionistMostVisitedEmployeeComponent,
    ReceptionistMostVisitedMonthComponent,
    ReceptionistRecentVisitorsComponent,
    ReceptionistSignedInComponent,
    ReceptionistTotalVisitorsComponent,
    ReceptionistVisitorsOnAppointmentComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,
    FormsModule,
    ReceptionistRoutingModule,
    MDBBootstrapModule,
    SharedModule,
    ReceptionistRoutingModule,
    MDBBootstrapModule,
    ChartsModule

    // Select2Module
]

})
export class ReceptionistModule { }
