import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDatepicker } from '@angular/material';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as _moment from 'moment';
import { AppoinmentsStatus, AppointmentsTable, AppoinmentsModel } from './appoinments-model';
import { AppoinmentsService } from './appoinments.service';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';

const moment = _moment;


@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.scss']
})
export class AppointmentsComponent implements OnInit {
  // displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  allAppointments: AppointmentsTable[];
  displayedColumns: string[] = ['staffId', 'visitorName', 'phone', 'location', 'email', 'date', 'status', 'company'];
  dataSource: MatTableDataSource<AppointmentsTable>;
  errorMessage: string;
  loading: boolean;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  createSearchForm: FormGroup;
  appointmentStatus: AppoinmentsStatus[] = [
    { appoinmentStatus: 'Pending', value: 1 },
    { appoinmentStatus: 'Visited', value: 2 },
    { appoinmentStatus: 'Cancelled', value: 3 },
    { appoinmentStatus: 'SignedIn', value: 4 }
  ];
  // createForm: any;
  endDate = new Date();
  startDate = new Date(this.endDate.getTime() - (7 * 24 * 60 * 60 * 1000));

  constructor(public fb: FormBuilder, private appointmentsService: AppoinmentsService, private toastr: ToastrService) {

  }

  ngOnInit() {

    this.createReactiveform();
    this.appointmentsService.allAppoinments.subscribe(res => {
      this.allAppointments = res;
    });
    // console.log(this.allAppointments);
    setTimeout(() => {
      this.dataSource = new MatTableDataSource<AppointmentsTable>(this.allAppointments);
      this.dataSource.paginator = this.paginator;
    }, 1000);

    this.appointmentsService.loader$.subscribe(loader => this.loading = loader);
    this.appointmentsService.error$.subscribe(err => this.errorMessage = err);
    // console.log(this.allAppointments);
    // this.appointmentStatus.push(new AppoinmentsStatus("Visited", 1));
    // this.appointmentStatus.push(new AppoinmentsStatus("Pending", 2));
    // this.appointmentStatus.push(new AppoinmentsStatus("Cancelled", 3));
  }

  // Filter Table
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  createReactiveform() {
    this.createSearchForm = this.fb.group({
      startDate: [this.startDate],
      endDate: [this.endDate],
      status: [, Validators.required],
      displayLength: [5]
    });
  }

  submitAppointment() {
    this.appointmentsService.updateLoader(true);
    this.appointmentsService.loader$.subscribe(loader => this.loading = loader);
    console.log(this.createSearchForm.value);
    if (this.createSearchForm.valid) {
      const lstartDate = this.createSearchForm.controls['startDate'].value;
      const lendDate = this.createSearchForm.controls['endDate'].value;
      const appointmentStatus = this.createSearchForm.controls['status'].value;
      const displayLength = this.createSearchForm.controls['displayLength'].value;

      const startDate = `${this.convert(lstartDate)} 00:00`;
      const endDate = `${this.convert(lendDate)} 00:00`;
      console.log(startDate);
      const appointmentModel = new AppoinmentsModel(startDate, endDate, appointmentStatus, displayLength)
     // console.log(appointmentModel);

      this.appointmentsService.getAppoinments(appointmentModel).subscribe(res => {
        if (res.code === '00') {
          // this.toastr.success('Processing Request');
          console.log(res.data.length);
          if (res.data.length === 0) {
            this.toastr.info('No result Found for your search criteria.');
            const emptyAppoinmentModel: AppointmentsTable[] = [];
            this.appointmentsService.extractData(emptyAppoinmentModel);
            this.appointmentsService.allAppoinments.subscribe( res => {
              this.allAppointments = res;
            });
          } else {
            this.appointmentsService.extractData(res.data);
            this.appointmentsService.allAppoinments.subscribe(res => {
              this.allAppointments = res;
            });
          }
        } else {
          this.toastr.error('An error occured while processing your request')
        }
        // console.log(this.allAppointments);
        this.dataSource = new MatTableDataSource<AppointmentsTable>(this.allAppointments);
        this.dataSource.paginator = this.paginator;
        this.appointmentsService.updateError(null);
      },
      (err:HttpErrorResponse) => {
        this.appointmentsService.updateError(err);
        this.appointmentsService.updateLoader(false);
        this.appointmentsService.setReceptionistAllAppointmentToEmpty();
      }
      );
    }
  }

  convert(str) {
    const date = new Date(str),
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2);
    return [mnth, day, date.getFullYear()].join('/');
  }


}