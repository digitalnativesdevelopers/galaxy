
export class AppoinmentsModel {
  startDate: string;
  endDate: string;
  appointmentStatus: any;
  displayLength: number;
  displayStart: number;


  constructor(startDate: string, endDate: string, appoinmentStatus: number, displayLength: number, displayStart?: number) {
    this.startDate = startDate,
      this.endDate = endDate,
      this.appointmentStatus = appoinmentStatus,
      this.displayLength = displayLength,
      this.displayStart = displayStart | 0
  }
}

export class AppoinmentsStatus {
  appoinmentStatus: string;
  value: number;
}

export interface AppointmentsTable {
  staffId: string;
  visitorName: string;
  phone: number;
  location: string;
  email: string;
  date: string;
  status: string;
  company: string;
}
