import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { AppointmentsTable, AppoinmentsModel } from './appoinments-model';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { UtilitiesService } from 'src/app/utilities/utilities.service';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppoinmentsService {
  BASE_URL = environment.galaxy_vms_baseUrl;
  postUrl = `${this.BASE_URL}appointments/getappointmentsbranch`;
  allAppoinments: Subject<AppointmentsTable[]> = new BehaviorSubject<AppointmentsTable[]>(null);
    // Observable sources: Error
    private errorSource = new BehaviorSubject<string>(null);
    error$ = this.errorSource.asObservable();
    // Observable sources: Loader
    private loaderSource = new BehaviorSubject<boolean>(false);
    loader$ = this.loaderSource.asObservable();


  constructor(private http: HttpClient, private util: UtilitiesService) { }


  getAppoinments(appoinmentModel): Observable<any> {
    return this.http.post(this.postUrl, appoinmentModel).pipe(
      retry(3),
      catchError(this.util.handleError)
    );
  }

  getAppoinmentsData(appoinmentModel) {
    this.getAppoinments(appoinmentModel).subscribe(res => {
     console.log(res);
      this.extractData(res.data);
      this.updateError(null);
    },
    (err: HttpErrorResponse) => {
      this.updateError(err);
      this.updateLoader(false);
      this.setReceptionistAllAppointmentToEmpty();
    });
  }

  extractData(data) {
    const appoinments: AppointmentsTable[] = [];
    // need staffId, Status and Date
    for (const s of data) {
      appoinments.push(
        // tslint:disable-next-line:max-line-length
        {staffId : s.StaffId, visitorName : s.VisitorName, phone : s.VisitorPhoneNumber, location : s.BranchName, email : s.Email, 
          date : s.AppointmentTime, status : s.AppointmentStatus, company : s.VisitorCompany}
       );

    }

    this.updateAllAppointments(appoinments);
    this.updateError(null);
    this.updateLoader(false);
    // console.log(this.allAppoinments);
  }

  updateAllAppointments(appoinments) {
    this.allAppoinments.next(appoinments);
  }

  updateError(message) {
    this.errorSource.next(message);
    console.log(message);
  }

  updateLoader(state) {
    this.loaderSource.next(state);
  }

  setReceptionistAllAppointmentToEmpty() {
    const  emptyAppointmentTable: AppointmentsTable[] = [];
    this.allAppoinments.next(emptyAppointmentTable);
  }

  instantiateDefaultAllAppointment() {

    const endDate = new Date();
    const startDate = new Date(endDate.getTime() - (7 * 24 * 60 * 60 * 1000));

    const EndDate = `${this.convert(endDate)} 00:00`;
    const StartDate = `${this.convert(startDate)} 00:00`;

    const allAppointmentModel = new AppoinmentsModel(StartDate, EndDate, 2, 5);
    // console.log(allAppointmentModel);
    this.getAppoinmentsData(allAppointmentModel);
    console.log(this.allAppoinments);
  }

  convert(str) {
    const date = new Date(str),
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2);
    return [mnth, day, date.getFullYear()].join('/');
  }


}
