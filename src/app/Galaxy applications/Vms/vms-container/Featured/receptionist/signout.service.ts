import { Injectable, Pipe } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { environment } from '../../../../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SignoutService {
  private visitorDetails = new BehaviorSubject<any>(null);
    visitorDetails$ = this.visitorDetails.asObservable();
  private loader = new BehaviorSubject<any>(false);
  loader$ = this.loader.asObservable();


  constructor( private http: HttpClient, private toastr: ToastrService) { }

  _signOutVisitor(tagNo): Observable<any> {
    return this.http.post( environment.vms_receptionist_signOut, tagNo)
    .pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  _fetchVisitorDetails(details): Observable<any> {
    return this.http.post( environment.vms_receptionist_fetchvisitorData, details);
  }

  subSignVisitorOut(tagNo) {
    this._signOutVisitor(tagNo).subscribe( res => {
      if ( res.message !== 'No user is signed in with this tag number') {
        this.toastr.success('Visitor Signed out successfully');
      } else {
        this.toastr.error('No visitor is signed in with this tag number');
      }
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }
  subFetchvisitorDetails(details) {
    this.updateLoader(true);
    this._fetchVisitorDetails(details).subscribe( res => {
      if (res.ResponseMessage === 'success') {
        const visitorDetails = {
          'visitorFullName' : res.ResponseObject[0].VisitorFullName,
          'visitorPhoneNumber' : res.ResponseObject[0].VisitorPhoneNumber,
          'visitorEmail' : res.ResponseObject[0].Email,
          'deviceType' : res.ResponseObject[0].DeviceType,
          'deviceSerialNo' : res.ResponseObject[0].SerialNumber,
          'visitorImage' : res.ResponseObject[0].VisitImage
        };
        this.visitorDetails.next(visitorDetails);
        this.updateLoader(false);
      } else if ( res.ResponseMessage === 'No Visitor found.' ) {
        const visitorDetails = {
          'visitorFullName' : '',
          'visitorPhoneNumber' : '',
          'visitorEmail' : '',
          'deviceType' : '',
          'deviceSerialNo' : '',
        };
        this.visitorDetails.next(visitorDetails);
        this.updateLoader(false);
        this.toastr.error ('No visitor with this tag number');
      }
    });
  }

  handleError( error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }

  updateLoader(state) {
    this.loader.next(state);
  }
}
