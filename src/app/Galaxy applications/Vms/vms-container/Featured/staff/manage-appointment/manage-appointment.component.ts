import { AuthenticationService } from 'src/app/Auth/authentication.service';
import { Appointments, Category } from './appointments';
import { ManageAppointmentService } from './manage-appointment.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { DataSource } from '@angular/cdk/table';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manage-appointment',
  templateUrl: './manage-appointment.component.html',
  styleUrls: ['./manage-appointment.component.scss']
})
export class ManageAppointmentComponent implements OnInit {

  appointments: Appointments[];
  appointmentToUpdate: any = {};
  appointmentIdToDelete: number;
  displayedColumns: string[] = ['visitor', 'company', 'phone', 'status', 'purpose', 'place', 'arrival', 'actions'];
  dataSource: any //MatTableDataSource<Appointments>;
  editAppointmentForm: FormGroup;
  staffId: string;
  categories: Category[] = [
    {
      name: 'Personal', value: 1
    },
    {
      name: 'Official', value: 2
    },
    {
      name: 'Others', value: 3
    }
  ];
  arrivalTime: any;
  searchKey: string;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.paginator = this.paginator;
  }

  constructor(
    private fb: FormBuilder,
    private data: ManageAppointmentService,
    private router: Router) { }

  ngOnInit() {
    this.data.getStaffDetail();
    this.createReactiveForm();

    this.data.appointments.subscribe(res => {
      this.appointments = res;

      this.dataSource = new MatTableDataSource<Appointments>(this.appointments);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;

    });

    // setTimeout(() => {
    //   this.dataSource = new MatTableDataSource<Appointments>(this.appointments);
    //   this.dataSource.sort = this.sort;
    //   this.dataSource.paginator = this.paginator;
    // }, 2000);
  }


  // Edit Appointments Form
  createReactiveForm() {
    this.editAppointmentForm = this.fb.group({
      visitorName: [null, Validators.required],
      company: [null, Validators.required],
      phoneNumber: [null, Validators.required],
      arrivalDate: [null, Validators.required],
      arrivalTime: [null, Validators.required],
      purpose: [null, Validators.required]
    });
  }

  // get and set the edit category modal form
  setFormData(row) {
    this.editAppointmentForm.controls['visitorName'].setValue(row.visitorName);
    this.editAppointmentForm.controls['company'].setValue(row.company);
    this.editAppointmentForm.controls['phoneNumber'].setValue(row.phone);
    this.editAppointmentForm.controls['purpose'].setValue(row.purpose);
    this.appointmentToUpdate.AppointmentId = row.id;

    // split the date and time, and then pass back to the modal
    const arrivalDate = this.separateDateAndTime(row.arrival);

    this.editAppointmentForm.controls['arrivalDate'].setValue(arrivalDate);
    this.editAppointmentForm.controls['arrivalTime'].setValue(this.arrivalTime);
    console.log(this.appointmentToUpdate.AppointmentId);
  }


  // separates date and time and return date
  separateDateAndTime(date) {
    const newDate = date.split('T');
    this.setTime(newDate[1]);
    return new Date(newDate[0]);
  }

  // sets time to the API specification
  setTime(time) {
    const timeLength = 5;
    this.arrivalTime = time.substring(0, timeLength);
  }


  // makes sure the form is empty
  initializeFormData() {
    this.editAppointmentForm.controls['visitorName'].setValue('');
    this.editAppointmentForm.controls['company'].setValue(' ');
    this.editAppointmentForm.controls['phoneNumber'].setValue(' ');
    this.editAppointmentForm.controls['purpose'].setValue(' ');
    this.editAppointmentForm.controls['arrivalDate'].setValue(' ');
    this.editAppointmentForm.controls['arrivalTime'].setValue(' ');
  }

  // sets the id of the appointment to cancel
  setCancel(id) {
    this.appointmentIdToDelete = id;
  }

  // cancel appointment
  cancelAppointment() {
    this.data.subscribeCancelAppointment(this.appointmentIdToDelete);

    this.dataSource = new MatTableDataSource<Appointments>(this.appointments);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  // convert datetime from SQL format to javascript format
  convert(str) {
    const date = new Date(str),
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2);
    return [mnth, day, date.getFullYear()].join('/');
  }


  // on submit of edit appointment form
  onSubmit(visitor) {

    const dateToPass = this.convert(visitor.arrivalDate);

    // Prepare request body
    this.appointmentToUpdate.ArrivalDate = dateToPass + ' ' + visitor.arrivalTime;
    this.appointmentToUpdate.VisitorFullName = visitor.visitorName;
    // this.appointmentToUpdate.Email = visitor.email;
    this.appointmentToUpdate.VisitorPhoneNumber = visitor.phoneNumber;
    this.appointmentToUpdate.VisitorCompany = visitor.company;
    this.appointmentToUpdate.Purpose = visitor.purpose;

    // Call the service

    this.data.subscribeEditAppointment(this.appointmentToUpdate.AppointmentId, this.appointmentToUpdate);



    // Initialize the form
    this.initializeFormData();
  }

  // Reload the page
  initMatTable() {
    this.data.getStaffAppointment();
  }


  // Clear search box
  onSearchClear() {
    this.searchKey = '';
    this.applyFilter(this.searchKey);
  }
}
