export interface Appointments {
    id?: number;
    visitorName: string;
    company: string;
    // email: string;
    phone: string;
    arrival: string;
    purpose: string;
    status: string;
    place: string

    // constructor(visitorName: string, company: string, email: string, phone: string,
    //   arrival: string, purpose: string, status: string, place: string, id?: number) {
    //     this.id = id;
    //     this.visitorName = visitorName;
    //     this.company = company;
    //     this.email = email;
    //     this.phone = phone;
    //     this.arrival = arrival;
    //     this.purpose = purpose;
    //     this.status = status;
    //     this.place = place;
    // }
}

export class Category{
    name: string;
    value: number;

    constructor(name: string, value: number){
        this.name = name;
        this.value = value;
    }
}
