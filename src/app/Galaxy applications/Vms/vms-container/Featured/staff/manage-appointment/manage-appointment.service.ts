import { Response } from './../../../../../../Featured/home-page/favourite-apps/favorite-app';
import { AuthenticationService } from 'src/app/Auth/authentication.service';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { Appointments } from './appointments';
import { environment } from 'src/environments/environment.prod';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};


@Injectable({
  providedIn: 'root'
})
export class ManageAppointmentService {

  staffId: string;
  appointments: Subject<Appointments[]> = new BehaviorSubject<any>(null);
  staff: any = {};

  constructor(private http: HttpClient,
    private auth: AuthenticationService,
    private alert: ToastrService,
    private router: Router) { }
    

  // get staff details
  getStaffDetail() {
    this.auth.staffId.subscribe(res => {
      this.staffId = res;
      this.staff.staffId = this.staffId;
    });
  }

  // get staff appointments request
  fetchAppointments(): Observable<any> {
    return this.http.post(environment.vms_staff_GetStaffAppointment, { stafId: 'P7580' },{withCredentials:true});
  }

  // get staff appointments from the request response
  getStaffAppointment() {
    this.extractAppointmentData();
    this.fetchAppointments().subscribe(res => {
      this.extractAppointmentData(res.data);
      console.log(res.data);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

  // push the appointments to the observable
  extractAppointmentData(response?) {
    if (response) {
      const appointments: Appointments[] = [];
      for (const appointment of response) {
        // appointments.push({
        //   visitorName: appointment.VisitorName, company: appointment.VisitorCompany, email: appointment.Email, phone: appointment.VisitorPhoneNumber,
        //   arrival: appointment.AppointmentDate, purpose: appointment.Purpose, status: appointment.AppointmentStatus, place: appointment.BranchName, id: appointment.AppointmentId,
        // });
        if (appointment.AppointmentStatus == 1) {

          appointments.push({
            visitorName: appointment.VisitorFirstName, company: appointment.VisitorCompany, phone: appointment.VisitorPhoneNumber,
            arrival: appointment.AppointmentDate, purpose: appointment.Purpose, status: "Pending", place: appointment.BranchName, id: appointment.AppointmentId,
          });
        }
        else if (appointment.AppointmentStatus == 2) {
          appointments.push({
            visitorName: appointment.VisitorFirstName, company: appointment.VisitorCompany, phone: appointment.VisitorPhoneNumber,
            arrival: appointment.AppointmentDate, purpose: appointment.Purpose, status: "Visited", place: appointment.BranchName, id: appointment.AppointmentId,
          });
        }
        else if (appointment.AppointmentStatus == 3) {
          appointments.push({
            visitorName: appointment.VisitorFirstName, company: appointment.VisitorCompany, phone: appointment.VisitorPhoneNumber,
            arrival: appointment.AppointmentDate, purpose: appointment.Purpose, status: "Canceled", place: appointment.BranchName, id: appointment.AppointmentId,
          });
        }
        else {
          appointments.push({
            visitorName: appointment.VisitorFirstName, company: appointment.VisitorCompany, phone: appointment.VisitorPhoneNumber,
            arrival: appointment.AppointmentDate, purpose: appointment.Purpose, status: "Signed In", place: appointment.BranchName, id: appointment.AppointmentId,
          });
        }
      }

      this.appointments.next(appointments);
    }
    else{
      const appointments: Appointments[] = [];
      this.appointments.next(appointments);
      console.log(this.appointments);
    }
  }

  // Edit appointment request
  editAppointment(id, details): Observable<any> {
    return this.http.post<any>(environment.vms_staff_editAppointment + id, details,{withCredentials:true});
  }

  // subscribe to the edit appointment response
  subscribeEditAppointment(id, details) {
    this.editAppointment(id, details).subscribe(res => {
      this.alert.success(res.message);
      console.log(res);
      this.getStaffAppointment();
    },
      (err: HttpErrorResponse) => {
        this.alert.error("An error ocurred, please try again");
        console.log(err);
      }
    );
  }

  // Delete appointment request
  cancelAppointment(id): Observable<any> {
    return this.http.post<any>(environment.vms_staff_cancelAppointment + id, '',{withCredentials:true});
  }

  // subscribe to the cancel appointment response
  subscribeCancelAppointment(id) {
    this.cancelAppointment(id).subscribe(res => {
      this.alert.success(res.message);
      this.getStaffAppointment();
      console.log(res);
    },
      (err: HttpErrorResponse) => {
        //  this.alerts.setMessage('Application not deleted, please try again', 'error');
        this.alert.error("An error occured, please try again");
        console.log(err);
      });
  }


}
