import { TestBed } from '@angular/core/testing';

import { ManageAppointmentService } from './manage-appointment.service';

describe('ManageAppointmentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageAppointmentService = TestBed.get(ManageAppointmentService);
    expect(service).toBeTruthy();
  });
});
