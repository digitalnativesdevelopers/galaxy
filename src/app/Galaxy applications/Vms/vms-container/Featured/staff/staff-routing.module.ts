import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StaffComponent } from './staff.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ManageAppointmentComponent } from './manage-appointment/manage-appointment.component';
import { CreateAppointmentComponent } from './create-appointment/create-appointment.component';
import { VisitorLogComponent } from './visitor-log/visitor-log.component';

const routes: Routes = [
  {path: '', component: StaffComponent,
  children : [
    {path : '', redirectTo : 'dashboard', pathMatch : 'full'},
    {path : 'manage-appointment', component : ManageAppointmentComponent},
    {path: 'dashboard', component: DashboardComponent},
    {path : 'visitorlog', component : VisitorLogComponent},
    {path: 'createAppointment', component: CreateAppointmentComponent}
  ]},
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaffRoutingModule { }
