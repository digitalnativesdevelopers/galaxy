export class VisitorlogModel {
    visitStatus: number;
    displayLength: number;
    startDate: string;
    endDate: string;

    constructor(displayLength: number,startDate: string,endDate: string, visitStatus? : number){
        this.visitStatus = visitStatus | 2,
        this.displayLength = displayLength,
        this.startDate = startDate,
        this.endDate = endDate
    }
}


export interface VisitorlogTable {
    visitorName: string;
    picture: string;
    company: string;
    email: string;
    phoneNumber: number;
    purpose: string;
    timeIn: Date;
    timeOut: Date;
}


