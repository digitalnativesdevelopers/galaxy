import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { VisitorlogTable, VisitorlogModel } from './visitorlog-model';
import { retry, catchError } from 'rxjs/operators';
import { UtilitiesService } from 'src/app/utilities/utilities.service';

@Injectable({
  providedIn: 'root'
})
export class VisitorLogService {
  BASE_URL = environment.galaxy_vms_baseUrl;
  getVisitorLogUrlForStaff = `${this.BASE_URL}staff/visitors`;
  staffVisitorLogs: Subject<VisitorlogTable[]> = new BehaviorSubject<VisitorlogTable[]>(null);

  private loaderSource = new BehaviorSubject<boolean>(false);
  loader$ = this.loaderSource.asObservable();

  // Observable sources: Error
  private errorSource = new BehaviorSubject<string>(null);
  error$ = this.errorSource.asObservable();

  constructor(private http: HttpClient, private Util: UtilitiesService) { }

  getStaffVisitorLog(vistorLogModel): Observable<any> {

    return this.http.post(this.getVisitorLogUrlForStaff, vistorLogModel).pipe(
      retry(3),
      catchError(this.Util.handleError)
    );
  }

  getStaffVisitorLogData(vistorLogModel) {
    // this.updateLoader(true);
    this.getStaffVisitorLog(vistorLogModel).subscribe(res => {
      this.extractData(res.data);
      this.updateError(null);
    },
      (err: HttpErrorResponse) => {
        this.updateError(err);
        this.updateLoader(false);
        this.setStaffVisitorLogToEmpty();
      });
  }

  extractData(data) {
    console.log(data);
    const staffVisitorLog: VisitorlogTable[] = [];
    // need staffId, Status and Date
    for (const s of data) {
      staffVisitorLog.push(
        {
          visitorName: s.VisitorFullName,
          picture: s.VisitImage,
          company: s.VisitorCompany,
          email: s.Email,
          phoneNumber: s.VisitorPhoneNumber,
          purpose: s.Purpose,
          timeIn: new Date(s.TimeIn),
          timeOut: new Date(s.TimeOut)
        }
      );

    }
    this.updateStaffVisitorLogs(staffVisitorLog);
    this.updateLoader(false);
    this.updateError(null);
    // console.log(this.staffVisitorLogs);
  }

  setStaffVisitorLogToEmpty() {
    const emptyVisitorLogTable: VisitorlogTable[] = [];
    console.log(emptyVisitorLogTable);
    this.staffVisitorLogs.next(emptyVisitorLogTable);
  }

  updateStaffVisitorLogs(staffVisitorLog: VisitorlogTable[]) {
    this.staffVisitorLogs.next(staffVisitorLog);
  }

  updateError(message) {
    this.errorSource.next(message);
    console.log(message);
  }

  updateLoader(state) {
    this.loaderSource.next(state);
  }

  instantiateDefaultStaffVisitorLog() {

    const endDate = new Date();
    const startDate = new Date(endDate.getTime() - (7 * 24 * 60 * 60 * 1000));

    const EndDate = `${this.convert(endDate)} 00:00`;
    const StartDate = `${this.convert(startDate)} 00:00`;

    const visitorLogDefaultModel = new VisitorlogModel(5, StartDate, EndDate);
    console.log(visitorLogDefaultModel);
    this.getStaffVisitorLogData(visitorLogDefaultModel);
  }

  convert(str) {
    const date = new Date(str),
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2);
    return [mnth, day, date.getFullYear()].join('/');
  }
}
