import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDatepicker } from '@angular/material';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as _moment from 'moment';
import { VisitorlogTable, VisitorlogModel } from './visitorlog-model';
import { VisitorLogService } from './visitor-log.service';
import { ToastrService } from 'ngx-toastr';

const moment = _moment;

@Component({
  selector: 'app-visitor-log',
  templateUrl: './visitor-log.component.html',
  styleUrls: ['./visitor-log.component.scss']
})
export class VisitorLogComponent implements OnInit {
  // displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  staffVisitorLogs: VisitorlogTable[ ];
  displayedColumns: string[] = ['visitorName', 'picture', 'company', 'email', 'phoneNumber', 'purpose', 'timeIn', 'timeOut'];
  dataSource: MatTableDataSource<VisitorlogTable>;
  errorMessage: string;
  loading: boolean;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  createSearchForm: FormGroup;
  endDate = new Date();
  startDate = new Date(this.endDate.getTime() - (7 * 24 * 60 * 60 * 1000));

  constructor(public fb: FormBuilder, private visitorLogService: VisitorLogService, private toastr: ToastrService) {

  }

  ngOnInit() {
    this.visitorLogService.staffVisitorLogs.subscribe(res => {
      this.staffVisitorLogs = res;
    });
    console.log(this.staffVisitorLogs);
    setTimeout(() => {
      this.dataSource = new MatTableDataSource<VisitorlogTable>(this.staffVisitorLogs);
      this.dataSource.paginator = this.paginator;
    }, 2000);
    this.visitorLogService.loader$.subscribe(l => this.loading = l);
    this.createReactiveform();
  }

  // Filter Table
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  createReactiveform() {
    this.createSearchForm = this.fb.group({
      startDate: [this.startDate, Validators.required],
      endDate: [this.endDate, Validators.required],
      displayLength: [5]
    });
  }

  submitVisitorLog() {

    console.log(this.createSearchForm.value);
    if (this.createSearchForm.valid) {
      const lstartDate = this.createSearchForm.controls['startDate'].value;
      const lendDate = this.createSearchForm.controls['endDate'].value;
      const displayLength = this.createSearchForm.controls['displayLength'].value;


      const startDate = `${this.convert(lstartDate)} 00:00`;
      const endDate = `${this.convert(lendDate)} 23:59`;

     
      const visitorModel = new VisitorlogModel(displayLength, startDate, endDate);
     
      this.visitorLogService.getStaffVisitorLog(visitorModel).subscribe(res => {
        this.visitorLogService.updateLoader(true);
        if (res.code === "00") {
          //this.toastr.success('Form Submitted Successfully');
          if (res.data.length == 0) {
            this.toastr.info('No result Found for your search criteria.');
            let emptVisitorLogModel : VisitorlogTable[] = []; 
            this.visitorLogService.extractData(emptVisitorLogModel);
            this.visitorLogService.staffVisitorLogs.subscribe(res => {
              this.staffVisitorLogs = res;
            });
          }else{
            this.visitorLogService.extractData(res.data);
            this.visitorLogService.staffVisitorLogs.subscribe(res => {
              this.staffVisitorLogs = res;
            });
          }
        } else {
          this.toastr.error('An error occured while processing your request')
        }

        // console.log(this.staffVisitorLogs);
        this.dataSource = new MatTableDataSource<VisitorlogTable>(this.staffVisitorLogs);
        this.dataSource.paginator = this.paginator;

      });
    }
  }

  convert(str) {
    
    const date = new Date(str),
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2);
    return [mnth, day, date.getFullYear()].join('/');
  }

  
}