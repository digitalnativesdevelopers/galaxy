import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../../../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { map, catchError, retry } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CreateappointmentService {

  constructor( private http: HttpClient, private toastr: ToastrService, private router: Router ) { }

  _createAppointment(appointmentDetails): Observable<any> {
    return this.http.post<any>( environment.vms_createAppointment, appointmentDetails)
    .pipe(
        retry(3),
        catchError( this.handleError)
    );
  }
// Create a new appointment
  createAppointment(appointmentDetails) {
    this._createAppointment(appointmentDetails).subscribe( response => {
      if (response.message === 'Appointment created successfully') {
        this.toastr.success('Appointment created successfully');
        this.router.navigateByUrl('/staff');
      } else {
        if (response.message === `Date isn't  working hours|`) {
          this.toastr.error(`Time isn't a working hour`);
        } else {
        this.toastr.error(response.message);
        }
      }
    },
    (err: HttpErrorResponse) => {
      this.toastr.error('Appointment not created, pls try again');
     console.log(err);
    }
    );
  }

  handleError( error: HttpErrorResponse) {
    return throwError(error);
  }
}
