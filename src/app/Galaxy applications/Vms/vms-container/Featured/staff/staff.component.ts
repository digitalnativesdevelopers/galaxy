import { Component, OnInit } from '@angular/core';
import { VisitorLogService } from './visitor-log/visitor-log.service';
import { VisitorlogModel } from './visitor-log/visitorlog-model';
import * as _moment from 'moment';
import { ManageAppointmentService } from './manage-appointment/manage-appointment.service';
import { VmsAuthenticationService } from '../../Shared/auth/vms-authentication.service';
import { AuthenticationService } from '../../../../../Auth/authentication.service';
const moment = _moment;

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})
export class StaffComponent implements OnInit {
  endDate = new Date();
  startDate = new Date(this.endDate.getTime() - (7 * 24 * 60 * 60 * 1000));

  visitorLogDefaultModel: VisitorlogModel;

  constructor(private visitorLogService: VisitorLogService,
    private vmsAuth: VmsAuthenticationService,
    private galaxyAuth: AuthenticationService,
    private manageAppointmentService: ManageAppointmentService) { }

  ngOnInit() {
    this.manageAppointmentService.getStaffAppointment();
    this.visitorLogService.instantiateDefaultStaffVisitorLog();
  }
  
}
