import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaffRoutingModule } from './staff-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateAppointmentComponent } from './create-appointment/create-appointment.component';
import { ManageAppointmentComponent } from './manage-appointment/manage-appointment.component';
import { VisitorLogComponent } from './visitor-log/visitor-log.component';
import { StaffComponent } from './staff.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { SharedModule } from '../../Shared/shared.module';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { DashboardTopbarComponent } from './dashboard/dashboard-topbar/dashboard-topbar.component';
import { TotalVisitorsComponent } from './dashboard/total-visitors/total-visitors.component';
import { MostVisitedBlockChartComponent } from './dashboard/most-visited-block-chart/most-visited-block-chart.component';
import { MostVisitedEmployeeChartComponent } from './dashboard/most-visited-employee-chart/most-visited-employee-chart.component';
import { MostVisitedMonthChartComponent } from './dashboard/most-visited-month-chart/most-visited-month-chart.component';
import { RecentVisitorsComponent } from './dashboard/recent-visitors/recent-visitors.component';
import { SignedInVisitorsComponent } from './dashboard/signed-in-visitors/signed-in-visitors.component';
import { VisitorsOnAppointmentComponent } from './dashboard/visitors-on-appointment/visitors-on-appointment.component';




@NgModule({
  declarations: [
    DashboardComponent,
    CreateAppointmentComponent,
    ManageAppointmentComponent,
    VisitorLogComponent,
    StaffComponent,
    DashboardTopbarComponent,
    TotalVisitorsComponent,
    MostVisitedBlockChartComponent,
    MostVisitedEmployeeChartComponent,
    MostVisitedMonthChartComponent,
    RecentVisitorsComponent,
    SignedInVisitorsComponent,
    VisitorsOnAppointmentComponent,
  ],
  imports: [
    CommonModule,
    StaffRoutingModule,
    SharedModule,
    MDBBootstrapModule,
    ChartsModule

  ]
})
export class StaffModule { }
