import { TestBed } from '@angular/core/testing';

import { CreateappointmentService } from './createappointment.service';

describe('CreateappointmentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreateappointmentService = TestBed.get(CreateappointmentService);
    expect(service).toBeTruthy();
  });
});
