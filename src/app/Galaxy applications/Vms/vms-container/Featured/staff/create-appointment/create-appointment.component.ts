import { CreateappointmentService } from './../createappointment.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Appointment } from '../appointment';
import { AuthenticationService } from 'src/app/Auth/authentication.service';

@Component({
  selector: 'app-create-appointment',
  templateUrl: './create-appointment.component.html',
  styleUrls: ['./create-appointment.component.scss']
})
export class CreateAppointmentComponent implements OnInit {
  createAppointmentForm: FormGroup;
  intent: boolean;
  minDate = new Date();
  minTime = '8:00';
  maxTime = '16:59';
  date: any;
  user: string;

  constructor(
    private fb: FormBuilder,
    private galaxyAuth: AuthenticationService,
    private _appointmentService: CreateappointmentService) {
  }
  getCurrentDate() {
    const currentDate = new Date();
    const date = currentDate.getDate();
    const month = currentDate.getMonth() ;
    const year = currentDate.getTime();
    console.log(year);
  }
  ngOnInit() {
    this.galaxyAuth.staffDetails$.subscribe(res => {
      if (res) {
        this.user = res.StaffName;
      }
    });
    this.createAppointmentForm = this.fb.group({
      visitorName: [null, Validators.required],
      placeOfWork: [null, Validators.required],
      phoneNumber: [null, Validators.required],
      email: [null, Validators.email],
      arrivalDate: [null, Validators.required],
      arrivalTime: [null, Validators.required],
      placeOfVisit: [null, Validators.required],
      purpose: [null, Validators.required]
    });
  }

  createAppointment(formValue) {
    let purpose: number;
    let arrivalDate;
    if ( formValue.purpose === 'personal') {
      purpose = 1;
    } else if (  formValue.purpose === 'official' ) {
      purpose = 2;
    } else if ( formValue.purpose === 'others' ) {
      purpose = 3;
    }
    arrivalDate = this.convert (formValue.arrivalDate);
    const appointmentDetails = {
      'VisitorPhoneNumber': formValue.phoneNumber,
      'BranchName': formValue.placeOfVisit,
      'VisitorCompany': formValue.placeOfWork,
      'VisitorFullName': formValue.visitorName,
      'ArrivalDate': arrivalDate + ' ' + formValue.arrivalTime,
      'Purpose': purpose
    };
    this._appointmentService.createAppointment(appointmentDetails);
  }
  // Format date
  convert(str) {
    const date = new Date(str),
    mnth = ('0' + (date.getMonth() + 1)).slice(-2),
    day = ('0' + date.getDate()).slice(-2);
    return [ mnth, day, date.getFullYear()].join('/');
    }

}
