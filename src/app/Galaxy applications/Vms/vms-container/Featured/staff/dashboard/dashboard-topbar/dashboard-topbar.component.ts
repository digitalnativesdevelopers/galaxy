import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/Auth/authentication.service';
import { VmsAuthenticationService } from '../../../../Shared/auth/vms-authentication.service';

@Component({
  selector: 'app-dashboard-topbar',
  templateUrl: './dashboard-topbar.component.html',
  styleUrls: ['./dashboard-topbar.component.scss']
})
export class DashboardTopbarComponent implements OnInit {
  isReceptionist: boolean;
  isAdmin: boolean;

  constructor(private auth: VmsAuthenticationService) { }

  ngOnInit() {
    this.getStaffRole();
  }

  getStaffRole() {
    this.isAdmin = this.auth.isAdmin;
    this.isReceptionist = this.auth.isReceptionist;
  }
}
