import { Component, OnInit } from '@angular/core';
import { StaffTotalVisitorsService } from './staff-total-visitors.service';

@Component({
  selector: 'app-total-visitors',
  templateUrl: './total-visitors.component.html',
  styleUrls: ['./total-visitors.component.scss']
})
export class TotalVisitorsComponent implements OnInit {

  totalNumberOfVisitors: number;

  constructor(private visitorService: StaffTotalVisitorsService) { }

  ngOnInit() {
    this.visitorService.getTotalNoOfVisitors();
    this.getTotalVisitors();
  }

  // Get count of all visitors

  getTotalVisitors() {
    this.visitorService.visitorsCount.subscribe(res => {
      this.totalNumberOfVisitors = res;
    });
  }

}
