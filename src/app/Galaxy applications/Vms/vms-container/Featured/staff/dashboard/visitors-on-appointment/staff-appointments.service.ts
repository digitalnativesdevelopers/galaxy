import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '<origin> | *'
  })
};

@Injectable({
  providedIn: 'root'
})
export class StaffAppointmentsService {

  appointmentCount: Subject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }

  getAppointments() {
    return this.http.post(environment.vms_staff_card_items, httpOptions);
  }

  getTotalNoOfAppointments() {
    this.getAppointments().subscribe( res => {
      this.extractAppointments(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

  extractAppointments(response) {
    if (response) {
      let numberOfAppointments: number ;
      numberOfAppointments = response.TotalAppointmentsToday;
      console.log('appointments', response.TotalAppointmentsToday);
      this.appointmentCount.next(numberOfAppointments);
    }
  }

}
