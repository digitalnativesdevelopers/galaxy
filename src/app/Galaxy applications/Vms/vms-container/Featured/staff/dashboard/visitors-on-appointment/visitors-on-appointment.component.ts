import { Component, OnInit } from '@angular/core';
import { StaffAppointmentsService } from './staff-appointments.service';

@Component({
  selector: 'app-visitors-on-appointment',
  templateUrl: './visitors-on-appointment.component.html',
  styleUrls: ['./visitors-on-appointment.component.scss']
})
export class VisitorsOnAppointmentComponent implements OnInit {

  totalNumberOfAppointments: number;

  constructor(private appointmentService: StaffAppointmentsService) { }

  ngOnInit() {
    this.appointmentService.getTotalNoOfAppointments();
    this.getTotalAppointments();
  }

  getTotalAppointments() {
    this.appointmentService.appointmentCount.subscribe(res => {
      this.totalNumberOfAppointments = res;
    });
  }

}
