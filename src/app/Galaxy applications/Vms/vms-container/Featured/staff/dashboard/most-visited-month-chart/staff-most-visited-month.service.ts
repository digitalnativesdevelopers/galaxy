import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '<origin> | *'
  })
};


@Injectable({
  providedIn: 'root'
})
export class StaffMostVisitedMonthService {


  busiestMonthCount: Subject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }

  param = {
    'staffID': 'P7569',
    'fullName': 'Chukwuzuorom Uzomah'
  };

  getBusyMonth() {
    return this.http.post(environment.vms_staff_most_visited_month, this.param, httpOptions);
  }

  extractMonthlyVisitors(res) {
    if (res) {
      const data: any = [];
      const label: any = [];
      let chartValues;
      let numberOfMonthlyVisitors = [] ;
      numberOfMonthlyVisitors = res;
      // console.log('monthly', numberOfMonthlyVisitors);
      // console.log(numberOfMonthlyVisitors.length);
      for ( const monthlyData of numberOfMonthlyVisitors) {
        data.push ( monthlyData.y);
      }
      // console.log('data', data);
      for (const monthlyDataLabel of numberOfMonthlyVisitors) {
        label.push( monthlyDataLabel.x);
      }
      // console.log('label', label);
      chartValues = { 'data': data, 'label': label};
      // console.log(chartValues);
      this.busiestMonthCount.next(chartValues);
    }
  }

  getTotalNoOfVisitors() {
    this.getBusyMonth().subscribe( res => {
      // console.log('response', res);
      this.extractMonthlyVisitors(res);
      // console.log('responseMonthly', this.busiestMonthCount.value);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

}
