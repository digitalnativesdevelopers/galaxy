import { StaffMostVisitedMonthService } from './staff-most-visited-month.service';
import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-most-visited-month-chart',
  templateUrl: './most-visited-month-chart.component.html',
  styleUrls: ['./most-visited-month-chart.component.scss']
})

export class MostVisitedMonthChartComponent implements OnInit {

  monthlyEmployeeVisits: [];

  public barChartLabels: string[];
  public barChartData: number[] = [];
  public barChartType = 'bar';

  constructor(
  private busyMonthService: StaffMostVisitedMonthService) {
  }

  ngOnInit() {
    this.busyMonthService.getTotalNoOfVisitors();
    this.getAllMonthsVisitors();
  }

  getAllMonthsVisitors() {
    this.busyMonthService.busiestMonthCount.subscribe(res => {
      // console.log( res);
      this.monthlyEmployeeVisits = res;
      // console.log('monthly', this.monthlyVisitors);
      if ( res ) {
        const months = res.label;
        console.log ('months', months);
        this.barChartData.push(res.data);
        this.barChartLabels = months.reverse();

        // console.log('barLables', this.barChartLabels);
        // console.log('barData', this.barChartData);
      } else {
        console.log('err: res is turnioniown');
      }
    });
  }

}
