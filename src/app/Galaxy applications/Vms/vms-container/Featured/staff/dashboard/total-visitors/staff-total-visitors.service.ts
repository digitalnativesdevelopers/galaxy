import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '<origin> | *'
  })
};


@Injectable({
  providedIn: 'root'
})
export class StaffTotalVisitorsService {

  constructor(private http: HttpClient) { }

  visitorsCount: Subject<any> = new BehaviorSubject<any>(null);


  getTotalUsers() {
    return this.http.post(environment.vms_staff_card_items, httpOptions);
  }

  getTotalNoOfVisitors() {
    this.getTotalUsers().subscribe( res => {
      this.extractTotalVisitors(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

  extractTotalVisitors(response) {
    if (response) {
      let numberOfUsers: number ;
      numberOfUsers = response.TotalVisitors;
      this.visitorsCount.next(numberOfUsers);
    }
  }

}
