import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '<origin> | *'
  })
};

@Injectable({
  providedIn: 'root'
})
export class StaffRecentVisitorsService {

  recentVisitorsCount: Subject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }

  getRecentVisitors() {
    return this.http.post(environment.vms_staff_card_items, httpOptions);
  }


  extractTotalVisitors(response) {
    if (response) {
      let numberOfRecentVisitors: [] ;
      numberOfRecentVisitors = response.MostRecentVisitors;
      this.recentVisitorsCount.next(numberOfRecentVisitors);
    }
  }

  getTotalNoOfVisitors() {
    this.getRecentVisitors().subscribe( res => {
      // console.log(res);
      // console.log(res.MostRecentVisitors);
      this.extractTotalVisitors(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

}