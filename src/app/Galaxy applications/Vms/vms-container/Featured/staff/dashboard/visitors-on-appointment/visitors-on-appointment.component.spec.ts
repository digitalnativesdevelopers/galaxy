import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitorsOnAppointmentComponent } from './visitors-on-appointment.component';

describe('VisitorsOnAppointmentComponent', () => {
  let component: VisitorsOnAppointmentComponent;
  let fixture: ComponentFixture<VisitorsOnAppointmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitorsOnAppointmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitorsOnAppointmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
