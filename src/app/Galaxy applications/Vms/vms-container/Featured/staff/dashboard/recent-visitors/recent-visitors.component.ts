import { Component, OnInit } from '@angular/core';
import { StaffRecentVisitorsService } from './staff-recent-visitors.service';

@Component({
  selector: 'app-recent-visitors',
  templateUrl: './recent-visitors.component.html',
  styleUrls: ['./recent-visitors.component.scss']
})
export class RecentVisitorsComponent implements OnInit {

  public RecentVisitors: any[];
  public emptyMessage: string;


  constructor(
  private recentService: StaffRecentVisitorsService) {
  }

  ngOnInit() {
    this.recentService.getTotalNoOfVisitors();
    this.getTotalRecentVisitors();
  }

  getTotalRecentVisitors() {
    this.recentService.recentVisitorsCount.subscribe(res => {
      if (res) {
        if (res.length !== 0 ) {
          this.RecentVisitors = res;
        }
        else if (res.length < 1) {
          this.emptyMessage = 'You have no recent vistors.';
          console.log('empty message status', this.emptyMessage);
        }
      } else {
        this.emptyMessage = 'Getting recent visitors...';
      }
    });
  }

}

