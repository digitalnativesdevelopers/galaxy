import { StaffVisitorsService } from './staff-visitors.service';
import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-most-visited-block-chart',
  templateUrl: './most-visited-block-chart.component.html',
  styleUrls: ['./most-visited-block-chart.component.scss']
})

export class MostVisitedBlockChartComponent implements OnInit {


  totalVisitorsForBlocks: [];

  public barChartLabels: string[];
  public barChartData: number[] = [];
  public barChartType = 'pie';

  constructor(
    public blocksService: StaffVisitorsService) {
  }

  ngOnInit() {
    this.blocksService.getTotalNoOfVisitors();
    this.getAllBlocksVisitors();
  }

  getAllBlocksVisitors() {
    // const data: any = [];
    this.blocksService.blockCount.subscribe(res => {
      if ( res ) {
        // console.log('res', res);
        this.barChartLabels = res.label;
        this.barChartData.push(res.data);

        // console.log('Lables for blocks', this.barChartLabels);
        // console.log('Data for blocks', this.barChartData);

      } else  {
        // console.log('error: res is turnioniown');
      }
    });
  }
}
