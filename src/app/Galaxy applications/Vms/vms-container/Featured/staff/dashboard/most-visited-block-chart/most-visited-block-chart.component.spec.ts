import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostVisitedBlockChartComponent } from './most-visited-block-chart.component';

describe('MostVisitedBlockChartComponent', () => {
  let component: MostVisitedBlockChartComponent;
  let fixture: ComponentFixture<MostVisitedBlockChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostVisitedBlockChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostVisitedBlockChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
