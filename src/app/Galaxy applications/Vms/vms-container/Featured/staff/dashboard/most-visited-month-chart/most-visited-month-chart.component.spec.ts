import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostVisitedMonthChartComponent } from './most-visited-month-chart.component';

describe('MostVisitedMonthChartComponent', () => {
  let component: MostVisitedMonthChartComponent;
  let fixture: ComponentFixture<MostVisitedMonthChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostVisitedMonthChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostVisitedMonthChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
