import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignedInVisitorsComponent } from './signed-in-visitors.component';

describe('SignedInVisitorsComponent', () => {
  let component: SignedInVisitorsComponent;
  let fixture: ComponentFixture<SignedInVisitorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignedInVisitorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignedInVisitorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
