import { Component, OnInit } from '@angular/core';
import { StaffSignedInVisitorsService } from './staff-signed-in-visitors.service';

@Component({
  selector: 'app-signed-in-visitors',
  templateUrl: './signed-in-visitors.component.html',
  styleUrls: ['./signed-in-visitors.component.scss']
})
export class SignedInVisitorsComponent implements OnInit {

  totalNumberOfSignedIn: number;

  constructor(private signedInService: StaffSignedInVisitorsService) { }

  ngOnInit() {
    this.signedInService.getTotalNoOfSignedIn();
    this.getTotalSignedIn();
  }
  getTotalSignedIn() {
    this.signedInService.signedInCount.subscribe(res => {
      this.totalNumberOfSignedIn = res;
    });
  }
}

