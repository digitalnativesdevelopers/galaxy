import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { Promise } from 'q';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})


export class DashboardService {

  name = 'Angular 5';

  observable = Observable.create((observer) =>{
    observer.next('Start processing...');

    setTimeout(() => observer.next('Still processing...'), 3000);

    setTimeout(() => observer.complete(), 5000);
  });

}
