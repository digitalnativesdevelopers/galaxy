import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostVisitedEmployeeChartComponent } from './most-visited-employee-chart.component';

describe('MostVisitedEmployeeChartComponent', () => {
  let component: MostVisitedEmployeeChartComponent;
  let fixture: ComponentFixture<MostVisitedEmployeeChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostVisitedEmployeeChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostVisitedEmployeeChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
