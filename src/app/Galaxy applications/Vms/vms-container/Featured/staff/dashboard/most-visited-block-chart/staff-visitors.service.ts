import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '<origin> | *'
  })
};


@Injectable({
  providedIn: 'root'
})
export class StaffVisitorsService {


  blockCount: Subject<any> = new BehaviorSubject<any>(null);

  param = {
    'staffID': 'P7569',
    'fullName': 'Chukwuzuorom Uzomah'
  };

  constructor(private http: HttpClient) { }

  getBlockTotal() {
    return this.http.post(environment.vms_staff_categories_of_visitors, this.param, httpOptions);
  }

  extractMostVisitedBlocks(res) {
    if (res) {
      const data: any = [];
      const label: any = [];
      let chartValues;
      let numberOfBlockVisitors = [] ;
      numberOfBlockVisitors = res;
      // console.log('monthly', numberOfBlockVisitors);
      // console.log(numberOfBlockVisitors.length);
      for ( const monthlyData of numberOfBlockVisitors) {
        data.push ( monthlyData.value);
      }
      // console.log('data', data);
      for (const monthlyDataLabel of numberOfBlockVisitors) {
        label.push( monthlyDataLabel.label);
      }
      // console.log('label', label);
      chartValues = { 'data': data, 'label': label};
      // console.log(chartValues);
      this.blockCount.next(chartValues);
      // console.log('observe', this.blockCount.value);
    }
  }

  getTotalNoOfVisitors() {
    this.getBlockTotal().subscribe( res => {
      // console.log('res', res);
      this.extractMostVisitedBlocks(res);
      // console.log('responseMonthly', this.busiestMonthCount.value);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

}
