export interface Appointment {
    VisitorFullName: string;
    VisitorCompany: string;
    VisitorPhoneNumber: number;
    ArrivalDateAndTime: string;
    Purpose: string;
    BranchName: string;

    // constructor(
    //     visitorFullName: string,
    //     visitorCompany: string,
    //     visitorPhoneNumber: number, arrivaldateAndTime: string, purpose: string, branchName: string
    //     ) {
    //     this.VisitorFullName = visitorFullName;
    //     this.VisitorCompany = visitorCompany;
    //     this.VisitorPhoneNumber = visitorPhoneNumber;
    //     this.Purpose = purpose;
    //     this.BranchName = branchName;
    //     this.ArrivalDateAndTime = arrivaldateAndTime;
    // }
}