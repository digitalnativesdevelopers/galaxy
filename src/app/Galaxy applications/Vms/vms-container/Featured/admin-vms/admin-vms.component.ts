import { Component, OnInit } from '@angular/core';
import { AdminVisitorLogModel } from './visitor-log/admin-visitor-log-model';
import { AdminVisitorLogService } from './visitor-log/admin-visitor-log.service';
import { AdminNonComplianceService } from './view-non-compliance/admin-non-compliance.service';
import { ViewAdminService } from './manage-users/view-admins/view-admin.service';
import { ViewReceptionistService } from './manage-users/view-receptionists/view-receptionist.service';

@Component({
  selector: 'app-admin-vms',
  templateUrl: './admin-vms.component.html',
  styleUrls: ['./admin-vms.component.scss']
})
export class AdminVmsComponent implements OnInit {

  adminVisitorlogModel: AdminVisitorLogModel;
  constructor(private adminServ: ViewAdminService, private adminServ_: ViewReceptionistService,
    private adminVisitorLogService: AdminVisitorLogService) { }

  ngOnInit() {
    this.adminServ.getVmsAdmins();
    this.adminServ_.getVmsReceptionists();
    this.adminVisitorLogService.instantiateDefaultAdminVisitorLog();
  }


}
