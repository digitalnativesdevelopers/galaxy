import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { AdminVmsRoutingModule } from './admin-vms-routing.module';
import { AdminVmsComponent } from './admin-vms.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ViewNonComplianceComponent } from './view-non-compliance/view-non-compliance.component';
import { VisitorLogComponent } from './visitor-log/visitor-log.component';
import { SharedModule } from '../../Shared/shared.module';
import { AdminDashboardTopbarComponent } from './dashboard/admin-dashboard-topbar/admin-dashboard-topbar.component';
import { AdminMostVisitedBlockChartComponent } from './dashboard/admin-most-visited-block-chart/admin-most-visited-block-chart.component';
// tslint:disable-next-line:max-line-length
import { AdminMostVisitedEmployeeChartComponent } from './dashboard/admin-most-visited-employee-chart/admin-most-visited-employee-chart.component';
import { AdminMostVisitedMonthChartComponent } from './dashboard/admin-most-visited-month-chart/admin-most-visited-month-chart.component';
import { AdminRecentVisitorsComponent } from './dashboard/admin-recent-visitors/admin-recent-visitors.component';
import { AdminSignedInVisitorsComponent } from './dashboard/admin-signed-in-visitors/admin-signed-in-visitors.component';
import { AdminTotalVisitorsComponent } from './dashboard/admin-total-visitors/admin-total-visitors.component';
import { AdminVisitorsOnAppointmentComponent } from './dashboard/admin-visitors-on-appointment/admin-visitors-on-appointment.component';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { ViewAdminsComponent } from './manage-users/view-admins/view-admins.component';
import { ViewReceptionistsComponent } from './manage-users/view-receptionists/view-receptionists.component';
import { ManageAdminComponent } from './manage-users/manage-admin/manage-admin.component';
import { ManageReceptionistsComponent } from './manage-users/manage-receptionists/manage-receptionists.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';


@NgModule({
  declarations: [
    AdminVmsComponent,
    DashboardComponent,
    ViewNonComplianceComponent,
    VisitorLogComponent,
    AdminDashboardTopbarComponent,
    AdminMostVisitedBlockChartComponent,
    AdminMostVisitedEmployeeChartComponent,
    AdminMostVisitedMonthChartComponent,
    AdminRecentVisitorsComponent,
    AdminSignedInVisitorsComponent,
    AdminTotalVisitorsComponent,
    AdminVisitorsOnAppointmentComponent,
    ManageUsersComponent,
    ViewAdminsComponent,
    ViewReceptionistsComponent,
    ManageAdminComponent,
    ManageReceptionistsComponent
  ],
  imports: [
    CommonModule,
    AdminVmsRoutingModule,
    SharedModule,
    MDBBootstrapModule,
    HttpClientModule,
    ChartsModule
  ],
  exports: [
    AdminVmsComponent
  ]
})
export class AdminVmsModule { }
