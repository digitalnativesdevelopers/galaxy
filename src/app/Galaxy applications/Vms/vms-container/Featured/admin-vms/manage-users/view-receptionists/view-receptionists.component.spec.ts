import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewReceptionistsComponent } from './view-receptionists.component';

describe('ViewReceptionistsComponent', () => {
  let component: ViewReceptionistsComponent;
  let fixture: ComponentFixture<ViewReceptionistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewReceptionistsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewReceptionistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
