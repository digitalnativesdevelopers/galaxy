import { TestBed } from '@angular/core/testing';

import { ViewAdminService } from './view-admin.service';

describe('ViewAdminService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewAdminService = TestBed.get(ViewAdminService);
    expect(service).toBeTruthy();
  });
});
