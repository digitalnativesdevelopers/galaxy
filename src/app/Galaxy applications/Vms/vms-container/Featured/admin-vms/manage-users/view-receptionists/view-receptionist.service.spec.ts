import { TestBed } from '@angular/core/testing';

import { ViewReceptionistService } from './view-receptionist.service';

describe('ViewReceptionistService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewReceptionistService = TestBed.get(ViewReceptionistService);
    expect(service).toBeTruthy();
  });
});
