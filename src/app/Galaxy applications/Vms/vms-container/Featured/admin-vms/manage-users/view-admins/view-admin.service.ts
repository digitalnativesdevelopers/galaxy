import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../../../../../../../environments/environment';
import { retry, catchError } from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ViewAdminService {
  param = { 'StaffId': 'P9000'};
  admin = new BehaviorSubject<any>(null);
  admin$ = this.admin.asObservable();

  loader = new BehaviorSubject<any>(false);
  loader$ = this.loader.asObservable();

  constructor (private http: HttpClient ) { }

  getAdmins(): Observable<any> {
    return this.http.post(environment.vms_getAllAdmin, this.param, httpOptions)
    .pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  getVmsAdmins() {
   // this.updateLoader(true);
    this.getAdmins().subscribe( res => {
      console.log(res);
      if ( res.ResponseMessage === 'No Admin found.') {
        this.admin.next('none');
      } else {
        if (res.ResponseMessage === 'success' ) {
          this.filterResponse(res.ResponseObject);
          }
      }
    },
    (err: HttpErrorResponse) => {
      this.updateLoader(false);
      console.log(err);
    }
    );
  }

  filterResponse(response) {
    const admins = [];
    for (const admin of response) {
      admins.push ({
        'name': admin.StaffName,
        'staffId': admin.StaffId,
        'staffEmail': admin.StaffEmail
      });
    }
    this.admin.next(admins);
    this.updateLoader(false);
  }

  handleError( error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }

  updateLoader (state) {
    this.loader.next(state);
  }

}
