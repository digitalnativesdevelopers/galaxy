import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../../../../../environments/environment';
import { retry, catchError } from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ViewReceptionistService {
    param = { 'StaffId': 'P9000'};
    receptionists =  new BehaviorSubject<any>(null);
    receptionists$ = this.receptionists.asObservable();

    loader = new BehaviorSubject<any>(false);
    loader$ = this.loader.asObservable();

  constructor( private http: HttpClient ) { }

  getReceptionists(): Observable<any> {
    return this.http.post (environment.vms_getAllReceptionists, this.param, httpOptions)
    .pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

    getVmsReceptionists() {
      this.updateLoader(true);
      this.getReceptionists().subscribe( res => {
         console.log(res);
        if ( res.ResponseMessage === 'No Receptionist found.' ) {
          this.receptionists.next('none');
        } else {
          if (res.ResponseMessage === 'success' ) {
            this.filterResponse(res.ResponseObject);
            console.log(res);
            }
        }
      },
      (err: HttpErrorResponse) => {
        this.updateLoader(false);
        console.log(err);
      });
    }

    filterResponse(response) {
      const receptionists = [];
      for (const receptionist of response) {
        receptionists.push ({
          'name': receptionist.StaffName,
          'staffId': receptionist.StaffId,
          'staffEmail': receptionist.StaffEmail,
          'branch': receptionist.BranchName
        });
      }
      console.log(receptionists);
      this.receptionists.next(receptionists);
      this.updateLoader(false);
    }


  handleError( error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }

  updateLoader (state) {
    this.loader.next(state);
  }
}
