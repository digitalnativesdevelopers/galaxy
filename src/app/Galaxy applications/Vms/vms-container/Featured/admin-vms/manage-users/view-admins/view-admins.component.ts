import { ViewAdminService } from './view-admin.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import {MatSort, MatTableDataSource, MatPaginator} from '@angular/material';

export interface Admin {
  name: string;
  staffId: string;
  staffEmail: string;
}

@Component({
  selector: 'app-view-admins',
  templateUrl: './view-admins.component.html',
  styleUrls: ['./view-admins.component.scss']
})

export class ViewAdminsComponent implements OnInit {
  loading: boolean;
  noAdmin: boolean;
  admin: [];
  displayedColumns: string[] = ['name', 'staffId', 'staffEmail'];
  dataSource;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor( private adminServ: ViewAdminService) {
   }

   getAdmins () {
     this.adminServ.admin$.subscribe( res => {
       if ( res === 'none') {
         this.noAdmin = true;
       } else {
         this.noAdmin = false;
        this.admin = res;
        this.dataSource = new MatTableDataSource(this.admin);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
       }
       this.adminServ.loader$.subscribe( state => {
        this.loading = state;
      });
     });
   }

  ngOnInit() {
    this.getAdmins();
  }

}
