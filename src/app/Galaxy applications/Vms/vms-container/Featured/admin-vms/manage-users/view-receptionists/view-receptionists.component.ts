import { Component, OnInit, ViewChild } from '@angular/core';
import {MatSort, MatTableDataSource, MatPaginator} from '@angular/material';
import { ViewReceptionistService } from './view-receptionist.service';


export interface Receptionist {
  name: string;
  staffId: string;
  staffEmail: string;
  branch: string;
}

@Component({
  selector: 'app-view-receptionists',
  templateUrl: './view-receptionists.component.html',
  styleUrls: ['./view-receptionists.component.scss']
})
export class ViewReceptionistsComponent implements OnInit {
  loading: boolean;
  displayedColumns: string[] = ['name', 'staffId', 'staffEmail', 'branch'];
  noReceptionist: boolean;
  receptionist: [];
  dataSource;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  constructor( private recepServ: ViewReceptionistService) { }

  getReceptionists () {
    this.recepServ.receptionists$.subscribe( res => {
      console.log(res);
      if ( res === 'none') {
        this.noReceptionist = true;
      } else {
        this.noReceptionist = false;
       this.receptionist = res;
       this.dataSource = new MatTableDataSource(this.receptionist);
       this.dataSource.paginator = this.paginator;
      }
      this.recepServ.loader$.subscribe( state => {
       this.loading = state;
     });
    });
    console.log(this.noReceptionist);
  }

  ngOnInit() {
    this.getReceptionists();
  }

}
