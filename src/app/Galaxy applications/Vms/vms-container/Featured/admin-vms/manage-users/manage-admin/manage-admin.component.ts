import { Component, OnInit, ViewChild } from '@angular/core';

export interface Role {
  value: string;
  viewValue: string;
}



@Component({
  selector: 'app-manage-admin',
  templateUrl: './manage-admin.component.html',
  styleUrls: ['./manage-admin.component.scss']
})
export class ManageAdminComponent implements OnInit {

  roles: Role[] = [
    {value: '3', viewValue: 'Admin'},
    {value: '4', viewValue: 'Receptionist'},
    {value: '2', viewValue: 'General Users'}
  ];

  constructor() { }

  ngOnInit() {

  }

}
