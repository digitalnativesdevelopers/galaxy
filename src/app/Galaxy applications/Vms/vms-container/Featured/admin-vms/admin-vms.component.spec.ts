import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminVmsComponent } from './admin-vms.component';

describe('AdminVmsComponent', () => {
  let component: AdminVmsComponent;
  let fixture: ComponentFixture<AdminVmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminVmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminVmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
