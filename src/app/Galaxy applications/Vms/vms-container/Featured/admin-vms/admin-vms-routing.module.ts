import { AdminGuard } from './../../Shared/Guards/admin.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminVmsComponent } from './admin-vms.component';
import { ViewNonComplianceComponent } from './view-non-compliance/view-non-compliance.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { VisitorLogComponent } from './visitor-log/visitor-log.component';
import { AuthGuard } from '../../../../../Auth/auth.guard';
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { ViewAdminsComponent } from './manage-users/view-admins/view-admins.component';
import { ViewReceptionistsComponent } from './manage-users/view-receptionists/view-receptionists.component';
import { ManageAdminComponent } from './manage-users/manage-admin/manage-admin.component';
import { ManageReceptionistsComponent } from './manage-users/manage-receptionists/manage-receptionists.component';

const routes: Routes = [
  {path : '', component : AdminVmsComponent, canActivate: [AdminGuard],
  children : [
    {path : '', redirectTo : 'dashboard', pathMatch : 'full' },
    {path : 'view-non-compliance', component : ViewNonComplianceComponent},
    {path : 'dashboard', component : DashboardComponent},
    {path : 'visitor-log', component : VisitorLogComponent},
    {path : 'manageusers', component : ManageUsersComponent,
    children : [
      {path: '', component: ViewAdminsComponent},
      {path: 'viewadmins', component: ViewAdminsComponent},
      {path: 'viewreceptionists', component: ViewReceptionistsComponent},
      {path: 'manageadmins', component: ManageAdminComponent},
      {path: 'managereceptionists', component: ManageReceptionistsComponent}
    ]
  }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminVmsRoutingModule { }
