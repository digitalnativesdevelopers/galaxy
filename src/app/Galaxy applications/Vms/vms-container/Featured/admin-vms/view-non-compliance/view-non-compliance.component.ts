import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDatepicker } from '@angular/material';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as _moment from 'moment';
import { AdminViewNonComplianceTable, AdminNonComplianceModel } from './admin-non-compliance-model';
import { AdminNonComplianceService } from './admin-non-compliance.service';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';

const moment = _moment;


@Component({
  selector: 'app-view-non-compliance',
  templateUrl: './view-non-compliance.component.html',
  styleUrls: ['./view-non-compliance.component.scss']
})
export class ViewNonComplianceComponent implements OnInit {
  downloadUrl: string = environment.galaxy_vms_downloadReportNonCompliance;
  maxDate = new Date();
  adminNonCompliance: AdminViewNonComplianceTable[]
  displayedColumns: string[] = ['dateLogged', 'defaultingStaff', 'issue', 'comment', 'receptionistLoggingIssue'];
  dataSource: MatTableDataSource<AdminViewNonComplianceTable>;
  errorMessage: string;
  loading: boolean;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  createSearchForm: FormGroup;


  constructor(public fb: FormBuilder, private adminNonComplianceService: AdminNonComplianceService, private toastr: ToastrService) {
    this.createReactiveform();
  }

  ngOnInit() {
    this.adminNonComplianceService.getAdminViewNonComplianceData();
    this.adminNonComplianceService.adminViewNonComplianceLog.subscribe(res => {
      this.adminNonCompliance = res;
    });
    this.adminNonComplianceService.error$.subscribe(err => this.errorMessage = err);
    this.adminNonComplianceService.loader$.subscribe(loader => this.loading = loader);
    setTimeout(() => {
      this.dataSource = new MatTableDataSource<AdminViewNonComplianceTable>(this.adminNonCompliance);
      this.dataSource.paginator = this.paginator;
    }, 1500);
  }

  //Filter Table
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  createReactiveform() {
    this.createSearchForm = this.fb.group({
      startDate: [, Validators.required],
      endDate: [, Validators.required]
    });
  }

  downloadReport() {
    if (this.createSearchForm.valid) {
      const startDate = this.createSearchForm.controls['startDate'].value;
      const endDate = this.createSearchForm.controls['endDate'].value;
      const newStartDate = `${this.convert(startDate)} 00:00`;
      const newEndDate = `${this.convert(endDate)} 00:00`;

       const postDateWithUrl = `${this.downloadUrl}StartDate=${newStartDate}&EndDate=${newEndDate}`;
       console.log(postDateWithUrl);
      window.open(postDateWithUrl, '_blank');

      setTimeout(() => {
        this.toastr.success('DownloadCompleted');
      }, 2000);
    }
  }

  convert(str) {
    const date = new Date(str),
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2);
    return [mnth, day, date.getFullYear()].join('/');
  }

}
