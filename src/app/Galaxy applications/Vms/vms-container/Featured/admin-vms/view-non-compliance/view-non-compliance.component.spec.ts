import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewNonComplianceComponent } from './view-non-compliance.component';

describe('ViewNonComplianceComponent', () => {
  let component: ViewNonComplianceComponent;
  let fixture: ComponentFixture<ViewNonComplianceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewNonComplianceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewNonComplianceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
