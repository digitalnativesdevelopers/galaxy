import { DatePipe } from '@angular/common';

export class AdminNonComplianceModel {
    startDate : string;
    endDate : string;

    constructor(startDate : string,endDate : string){
        this.startDate = startDate,
        this.endDate = endDate
    }
}

export interface AdminViewNonComplianceTable{
    dateLogged : Date;
    defaultingStaff : string;
    issue : string;
    comment : string;
    receptionistLoggingIssue : string;
}
