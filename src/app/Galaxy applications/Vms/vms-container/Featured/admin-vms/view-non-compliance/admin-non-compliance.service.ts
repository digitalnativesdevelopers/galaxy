import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { AdminViewNonComplianceTable } from './admin-non-compliance-model';
import { UtilitiesService } from 'src/app/utilities/utilities.service';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AdminNonComplianceService {
  BASE_URL = environment.galaxy_vms_baseUrl;
  ViewNonComplianceUrlForAdmin = `${this.BASE_URL}vms/ViewNonCompliance`;
  //DownloadNonComplianceUrl: string = `${this.BASE_URL}?`
  adminViewNonComplianceLog: Subject<AdminViewNonComplianceTable[]> = new BehaviorSubject<AdminViewNonComplianceTable[]>(null);
  // Observable sources: Error
  private errorSource = new BehaviorSubject<string>(null);
  error$ = this.errorSource.asObservable();

  // Observable sources: Loader
  private loaderSource = new BehaviorSubject<boolean>(false);
  loader$ = this.loaderSource.asObservable();

  constructor(private http: HttpClient, private util: UtilitiesService) { }

  data = {
    staffID: 'P7575'
  }

  getAdminViewNonCompliance(): Observable<any> {
    return this.http.post(this.ViewNonComplianceUrlForAdmin, this.data).pipe(
      retry(3),
      catchError(this.util.handleError)
    );
  }

  getAdminViewNonComplianceData() {
    this.getAdminViewNonCompliance().subscribe(res => {
      // console.log(res.code);
      // console.log(res.message);
      this.extractData(res);
      this.updateError(null);
      console.log(res);
    }, (err: HttpErrorResponse) => {
      this.updateError(err);
      this.updateLoader(false);
    }
    );
  }

  extractData(data) {
    const adminViewNonComplianceLog: AdminViewNonComplianceTable[] = [];

    for (const s of data) {
      adminViewNonComplianceLog.push(
        {
          dateLogged: new Date(s.DateStaffWasReported),
          defaultingStaff: s.StaffIdReported,
          issue: s.Issue,
          comment: s.Comment,
          receptionistLoggingIssue: s.ReceptionistId
        }
      );

    }
    this.loaderSource.next(false);
    this.updateError(null);
    this.updateAdminViewNonCompliance(adminViewNonComplianceLog);

  }


  updateError(message) {
    this.errorSource.next(message);
    console.log(message);
  }

  updateLoader(state) {
    this.loaderSource.next(state);
  }

  updateAdminViewNonCompliance(adminViewNonCompliance) {
    this.adminViewNonComplianceLog.next(adminViewNonCompliance);
  }

}
