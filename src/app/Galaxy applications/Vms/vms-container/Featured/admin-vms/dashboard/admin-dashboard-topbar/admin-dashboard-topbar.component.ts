import { Component, OnInit } from '@angular/core';
import { VmsAuthenticationService } from '../../../../Shared/auth/vms-authentication.service';

@Component({
  selector: 'app-admin-dashboard-topbar',
  templateUrl: './admin-dashboard-topbar.component.html',
  styleUrls: ['./admin-dashboard-topbar.component.scss']
})
export class AdminDashboardTopbarComponent implements OnInit {
  isAdmin: boolean;
  constructor(private auth: VmsAuthenticationService) { }

  ngOnInit() {
  }

}
