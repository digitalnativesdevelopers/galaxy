import { AdminMostVisitedMonthService } from './admin-most-visited-month.service';
import { Component, OnInit } from '@angular/core';
import { AdminDashboardServiceService } from '../admin-dashboard-service.service';

@Component({
  selector: 'app-admin-most-visited-month-chart',
  templateUrl: './admin-most-visited-month-chart.component.html',
  styleUrls: ['./admin-most-visited-month-chart.component.scss']
})
export class AdminMostVisitedMonthChartComponent implements OnInit {


  monthlyVisitors: [];

  public barChartLabels: string[];
  public barChartData: number[] = [];
  public barChartType = 'bar';

  constructor(
  private busyMonthService: AdminMostVisitedMonthService) {
   this.busyMonthService.getTotalNoOfVisitors();
    this.getAllMonthsVisitors();
  }

  ngOnInit() {
  }

  getAllMonthsVisitors() {
    this.busyMonthService.busiestMonthCount.subscribe(res => {
      this.monthlyVisitors = res;
      if ( res ) {
        this.barChartData.push(res.data);
        this.barChartLabels = res.label;
      } else {
        console.log('err: res is turnioniown');
      }
    });
  }

}