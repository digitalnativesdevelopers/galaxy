import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '<origin> | *'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AdminSignedInVisitorsService {

  signedInCount: Subject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }

  getSignedIn(staffID: string) {
    return this.http.post(environment.vms_get_admin_dashboard_card_items, staffID, httpOptions);
  }

  getTotalNoOfSignedIn() {
    this.getSignedIn('P7560').subscribe( res => {
      this.extractSignedIn(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

  extractSignedIn(response) {
    if (response) {
      let numberOfSignedIn: number ;
      numberOfSignedIn = response.TotalSignedInVisitorToday;
      this.signedInCount.next(numberOfSignedIn);
    }
  }

}
