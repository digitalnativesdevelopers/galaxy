import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTotalVisitorsComponent } from './admin-total-visitors.component';

describe('AdminTotalVisitorsComponent', () => {
  let component: AdminTotalVisitorsComponent;
  let fixture: ComponentFixture<AdminTotalVisitorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTotalVisitorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTotalVisitorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
