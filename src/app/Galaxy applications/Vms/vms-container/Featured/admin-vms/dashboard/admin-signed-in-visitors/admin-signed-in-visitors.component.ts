import { Component, OnInit } from '@angular/core';
import { AdminSignedInVisitorsService } from './admin-signed-in-visitors.service';

@Component({
  selector: 'app-admin-signed-in-visitors',
  templateUrl: './admin-signed-in-visitors.component.html',
  styleUrls: ['./admin-signed-in-visitors.component.scss']
})
export class AdminSignedInVisitorsComponent implements OnInit {

  totalNumberOfSignedIn: number;

  constructor(private signedInService: AdminSignedInVisitorsService) { }

  ngOnInit() {
    this.signedInService.getTotalNoOfSignedIn();
    this.getTotalSignedIn();
  }
  getTotalSignedIn() {
    this.signedInService.signedInCount.subscribe(res => {
      this.totalNumberOfSignedIn = res;
    });
  }
}

