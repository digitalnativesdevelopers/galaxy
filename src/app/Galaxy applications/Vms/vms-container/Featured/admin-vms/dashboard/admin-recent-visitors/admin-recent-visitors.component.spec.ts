import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRecentVisitorsComponent } from './admin-recent-visitors.component';

describe('AdminRecentVisitorsComponent', () => {
  let component: AdminRecentVisitorsComponent;
  let fixture: ComponentFixture<AdminRecentVisitorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRecentVisitorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRecentVisitorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
