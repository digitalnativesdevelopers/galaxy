import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/map';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '<origin> | *'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AdminMostVisitedEmployeeService {


  busiestEmployeeCount: Subject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }

  param = {
    'staffID': 'P7569',
    'fullName': 'Chukwuzuorom Uzomah'
  };

  getBusyEmployee() {
    return this.http.post(environment.vms_get_admin_most_visited_staff, this.param, httpOptions);
  }

  extractMonthlyVisits(res) {
    if (res) {
      const data: any = [];
      const label: any = [];
      let chartValues;

      let numberOfMonthlyVisitors = [] ;
      numberOfMonthlyVisitors = res.ResponseObject;
      for ( const monthlyData of numberOfMonthlyVisitors) {
        data.push (monthlyData.NumberofVisits);
      }

      for (const monthlyDataLabel of numberOfMonthlyVisitors) {
        label.push( monthlyDataLabel.StaffId);
      }

      chartValues = { 'data': data, 'label': label};
      this.busiestEmployeeCount.next(chartValues);
    }
  }

  getTotalNoOfVisitors() {
    this.getBusyEmployee().subscribe( res => {
      this.extractMonthlyVisits(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

}
