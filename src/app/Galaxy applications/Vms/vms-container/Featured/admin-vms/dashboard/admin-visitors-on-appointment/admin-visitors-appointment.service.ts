import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '<origin> | *'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AdminVisitorsAppointmentService {

  appointmentCount: Subject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }

  getAppointments(staffID: string) {
    return this.http.post(environment.vms_get_admin_dashboard_card_items, staffID, httpOptions);
  }

  getTotalNoOfAppointments() {
    this.getAppointments('P7578').subscribe( res => {
      this.extractAppointments(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

  extractAppointments(response) {
    if (response) {
      let numberOfAppointments: number ;
      numberOfAppointments = response.TotalSignedInVisitorToday;
      this.appointmentCount.next(numberOfAppointments);
    }
  }

}

