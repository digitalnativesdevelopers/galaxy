import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '<origin> | *'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AdminMostVisitedBlockService {

  blockCount: Subject<any> = new BehaviorSubject<any>(null);

  param = {
    'staffID': 'P7560',
    'fullName': 'Emmanuel Oluwafemi'
  };


  constructor(private http: HttpClient) { }

  getBlockTotal() {
    return this.http.post(environment.vms_get_admin_most_visited_block, this.param, httpOptions);
  }

  extractMostVisitedBlocks(res) {
    if (res) {

      const data: any = [];
      const label: any = [];
      let chartValues;
      let numberOfBlockVisitors = [];
      numberOfBlockVisitors = res;

      for ( const monthlyData of numberOfBlockVisitors) {
        data.push ( monthlyData.value);
      }

      for (const monthlyDataLabel of numberOfBlockVisitors) {
        label.push( monthlyDataLabel.label);
      }

      chartValues = {'data': data, 'label': label};
      this.blockCount.next(chartValues);

    }
  }

  getTotalNoOfVisitors() {
    this.getBlockTotal().subscribe( res => {
      this.extractMostVisitedBlocks(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

}
