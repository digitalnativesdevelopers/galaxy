import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '<origin> | *'
  })
};


@Injectable({
  providedIn: 'root'
})
export class MostTotalVisitorsService {

  visitorsCount: Subject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }

  // get total users for admin dashboard

  getTotalUsers(staffID: string) {
    return this.http.post(environment.vms_get_admin_dashboard_card_items, staffID, httpOptions);
  }

  getTotalNoOfVisitors() {
    this.getTotalUsers('P7560').subscribe( res => {
      this.extractTotalVisitors(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

  extractTotalVisitors(response) {
    if (response) {
      let numberOfUsers: number ;
      numberOfUsers = response.TotalVisitors;
      this.visitorsCount.next(numberOfUsers);
    }
  }

  // ******************************************************
  // end of get total users for admin dashboard

}
