import { Component, OnInit } from '@angular/core';
import { AdminVisitorsAppointmentService } from './admin-visitors-appointment.service';

@Component({
  selector: 'app-admin-visitors-on-appointment',
  templateUrl: './admin-visitors-on-appointment.component.html',
  styleUrls: ['./admin-visitors-on-appointment.component.scss']
})
export class AdminVisitorsOnAppointmentComponent implements OnInit {

  totalNumberOfAppointments: number;

  constructor(private appointmentService: AdminVisitorsAppointmentService) { }

  ngOnInit() {
    this.appointmentService.getTotalNoOfAppointments();
    this.getTotalAppointments();
  }

  getTotalAppointments() {
    this.appointmentService.appointmentCount.subscribe(res => {
      this.totalNumberOfAppointments = res;
    });
  }

}
