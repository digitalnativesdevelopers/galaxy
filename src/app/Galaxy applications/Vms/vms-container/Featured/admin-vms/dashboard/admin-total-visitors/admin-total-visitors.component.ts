import { Component, OnInit } from '@angular/core';
import { MostTotalVisitorsService } from './most-total-visitors.service';

@Component({
  selector: 'app-admin-total-visitors',
  templateUrl: './admin-total-visitors.component.html',
  styleUrls: ['./admin-total-visitors.component.scss']
})
export class AdminTotalVisitorsComponent implements OnInit {

  totalNumberOfVisitors: number;

  constructor(
    private visitorService: MostTotalVisitorsService ) {
  }

  ngOnInit() {
    this.visitorService.getTotalNoOfVisitors();
    this.getTotalVisitors();
  }

  // Get count of all visitors

  getTotalVisitors() {
    this.visitorService.visitorsCount.subscribe(res => {
      this.totalNumberOfVisitors = res;
    });
  }

}
