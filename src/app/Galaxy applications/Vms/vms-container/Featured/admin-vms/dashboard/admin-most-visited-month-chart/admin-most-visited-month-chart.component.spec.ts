import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMostVisitedMonthChartComponent } from './admin-most-visited-month-chart.component';

describe('AdminMostVisitedMonthChartComponent', () => {
  let component: AdminMostVisitedMonthChartComponent;
  let fixture: ComponentFixture<AdminMostVisitedMonthChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMostVisitedMonthChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMostVisitedMonthChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
