import { Component, OnInit } from '@angular/core';
import { RecentVisitorsService } from './recent-visitors.service';

@Component({
  selector: 'app-admin-recent-visitors',
  templateUrl: './admin-recent-visitors.component.html',
  styleUrls: ['./admin-recent-visitors.component.scss']
})
export class AdminRecentVisitorsComponent implements OnInit {

  RecentVisitors: [];

  constructor(
  private recentService: RecentVisitorsService) {
  }

  ngOnInit() {
    this.recentService.getTotalNoOfVisitors();
    this.getTotalRecentVisitors();
  }

  getTotalRecentVisitors() {
    this.recentService.recentVisitorsCount.subscribe(res => {
      // console.log(res);
      this.RecentVisitors = res;
      // console.log('recent', this.RecentVisitors);
    });
  }

}
