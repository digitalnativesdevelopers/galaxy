import { AdminMostVisitedEmployeeService } from './admin-most-visited-employee.service';
import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-admin-most-visited-employee-chart',
  templateUrl: './admin-most-visited-employee-chart.component.html',
  styleUrls: ['./admin-most-visited-employee-chart.component.scss']
})
export class AdminMostVisitedEmployeeChartComponent implements OnInit {

  monthlyEmployeeVisits: [];

  public barChartLabels: string[];
  public barChartData: number[] = [];
  public barChartType = 'doughnut';

  constructor(
    private visitedEmployeeService: AdminMostVisitedEmployeeService) {
    this.visitedEmployeeService.getTotalNoOfVisitors();
    this.getMonthlyVisits();
  }

  ngOnInit() {
  }

  getMonthlyVisits() {
    this.visitedEmployeeService.busiestEmployeeCount.subscribe(res => {
      // console.log('res', res);
      if ( res ) {
        this.barChartData.push(res.data);
        this.barChartLabels = res.label;

      } else {
        // console.log('err: res is turnioniown');
      }
    });
  }

}
