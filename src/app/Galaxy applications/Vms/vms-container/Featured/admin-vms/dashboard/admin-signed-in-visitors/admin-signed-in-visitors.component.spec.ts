import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSignedInVisitorsComponent } from './admin-signed-in-visitors.component';

describe('AdminSignedInVisitorsComponent', () => {
  let component: AdminSignedInVisitorsComponent;
  let fixture: ComponentFixture<AdminSignedInVisitorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSignedInVisitorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSignedInVisitorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
