import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMostVisitedBlockChartComponent } from './admin-most-visited-block-chart.component';

describe('AdminMostVisitedBlockChartComponent', () => {
  let component: AdminMostVisitedBlockChartComponent;
  let fixture: ComponentFixture<AdminMostVisitedBlockChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMostVisitedBlockChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMostVisitedBlockChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
