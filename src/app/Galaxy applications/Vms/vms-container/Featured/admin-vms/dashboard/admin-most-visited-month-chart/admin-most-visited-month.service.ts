import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '<origin> | *'
  })
};


@Injectable({
  providedIn: 'root'
})
export class AdminMostVisitedMonthService {

  api = 'http://dtptest/trueVisit/api/vms/GetMostVisistedBlock';


  busiestMonthCount: Subject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }

  param = {
    'staffID': 'P7569',
    'fullName': 'Chukwuzuorom Uzomah'
  };

  getBusyMonth() {
    return this.http.post(this.api, this.param, httpOptions);
  }

  extractMonthlyVisitors(res) {
    if (res) {
      const data: any = [];
      const label: any = [];
      let chartValues;
      let numberOfMonthlyVisitors = [] ;
      numberOfMonthlyVisitors = res;

      for ( const monthlyData of numberOfMonthlyVisitors) {
        data.push ( monthlyData.value);
      }

      for (const monthlyDataLabel of numberOfMonthlyVisitors) {
        label.push( monthlyDataLabel.label);
      }

      chartValues = { 'data': data, 'label': label};
      this.busiestMonthCount.next(chartValues);
    }
  }

  getTotalNoOfVisitors() {
    this.getBusyMonth().subscribe( res => {
      this.extractMonthlyVisitors(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

}
