import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '<origin> | *'
  })
};


@Injectable({
  providedIn: 'root'
})

export class RecentVisitorsService {

  recentVisitorsCount: Subject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }

  param = {
    'staffID': 'P7560',
    'fullName': 'Emmanuel Oluwafemi'
  };

  getRecentVisitors() {
    return this.http.post(environment.vms_get_admin_dashboard_card_items, this.param, httpOptions);
  }

  extractTotalVisitors(response) {
    if (response) {
      let numberOfRecentVisitors: [] ;
      numberOfRecentVisitors = response.MostRecentVisitors;
      this.recentVisitorsCount.next(numberOfRecentVisitors);
    }
  }

  getTotalNoOfVisitors() {
    this.getRecentVisitors().subscribe( res => {
      // console.log(res);
      // console.log(res.MostRecentVisitors);
      this.extractTotalVisitors(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

}
