import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { AdminMostVisitedBlockService } from './admin-most-visited-block.service';


@Component({
  selector: 'app-admin-most-visited-block-chart',
  templateUrl: './admin-most-visited-block-chart.component.html',
  styleUrls: ['./admin-most-visited-block-chart.component.scss']
})
export class AdminMostVisitedBlockChartComponent implements OnInit {

  totalVisitorsForBlocks: [];

  public barChartLabels: string[];
  public barChartData: number[] = [];
  public barChartType = 'horizontalBar';

  constructor(
    public blocksService: AdminMostVisitedBlockService) {
    this.blocksService.getTotalNoOfVisitors();
    this.getAllBlocksVisitors();
  }

  ngOnInit() {
  }

  getAllBlocksVisitors() {
    this.blocksService.blockCount.subscribe(res => {
      if ( res ) {
        this.barChartLabels = res.label;
        this.barChartData.push(res.data);
      } else  {
        // console.log('error: res is turnioniown');
      }
    });
  }
}
