import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDashboardTopbarComponent } from './admin-dashboard-topbar.component';

describe('AdminDashboardTopbarComponent', () => {
  let component: AdminDashboardTopbarComponent;
  let fixture: ComponentFixture<AdminDashboardTopbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDashboardTopbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDashboardTopbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
