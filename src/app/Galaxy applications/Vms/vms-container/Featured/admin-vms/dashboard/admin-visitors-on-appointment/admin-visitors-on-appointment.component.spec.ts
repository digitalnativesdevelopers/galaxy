import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminVisitorsOnAppointmentComponent } from './admin-visitors-on-appointment.component';

describe('AdminVisitorsOnAppointmentComponent', () => {
  let component: AdminVisitorsOnAppointmentComponent;
  let fixture: ComponentFixture<AdminVisitorsOnAppointmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminVisitorsOnAppointmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminVisitorsOnAppointmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
