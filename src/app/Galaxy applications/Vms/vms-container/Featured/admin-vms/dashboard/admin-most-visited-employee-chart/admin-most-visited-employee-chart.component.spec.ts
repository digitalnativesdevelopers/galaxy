import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMostVisitedEmployeeChartComponent } from './admin-most-visited-employee-chart.component';

describe('AdminMostVisitedEmployeeChartComponent', () => {
  let component: AdminMostVisitedEmployeeChartComponent;
  let fixture: ComponentFixture<AdminMostVisitedEmployeeChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMostVisitedEmployeeChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMostVisitedEmployeeChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
