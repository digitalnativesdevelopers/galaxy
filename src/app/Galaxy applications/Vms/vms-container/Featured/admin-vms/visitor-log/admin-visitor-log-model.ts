import { DatePipe } from "@angular/common";

export class AdminVisitorLogModel {
    startDate : string;
    endDate : string;
    displayLength : number;
    visitStatus : number;
    

    constructor(startDate : string,endDate : string,displayLength : number, visitStatus? : number){
        this.startDate = startDate,
        this.endDate = endDate,
        this.displayLength = displayLength,
        this.visitStatus = visitStatus | 2
    }
}


export interface AdminVisitorLogTable {
    picture : any;
    visitorName : string;
    company : string;
    email : string;
    phone : number;
    purpose : number;
    signedIn : Date;
    signedOut: Date;
}

