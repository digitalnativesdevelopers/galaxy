import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { AdminVisitorLogTable, AdminVisitorLogModel } from './admin-visitor-log-model';
import { retry, catchError } from 'rxjs/operators';
import { UtilitiesService } from 'src/app/utilities/utilities.service';

@Injectable({
  providedIn: 'root'
})
export class AdminVisitorLogService {
  BASE_URL = environment.galaxy_vms_baseUrl;
  VisitorLogUrlForAdmin = `${this.BASE_URL}visitors/getbankvisitors`; 
  adminVisitorLogs: Subject<AdminVisitorLogTable[]> = new BehaviorSubject<AdminVisitorLogTable[]>(null);
  adminVisitorLogs$ = this.adminVisitorLogs.asObservable();

   // Observable sources: Error
   private errorSource = new BehaviorSubject<string>(null);
   error$ = this.errorSource.asObservable();

   // Observable sources: Loader
   private loaderSource = new BehaviorSubject<boolean>(false);
   loader$ = this.loaderSource.asObservable();

  constructor(private http: HttpClient, private util: UtilitiesService) { }

  getAdminVisitorLog(adminModel): Observable<any> {
    return this.http.post(this.VisitorLogUrlForAdmin, adminModel).pipe(
      retry(3),
      catchError(this.util.handleError)
    );
  }

  getAdminVisitorLogData(adminModel) {
    this.getAdminVisitorLog(adminModel).subscribe(res => {
      // console.log(res.code);
      // console.log(res.message);
      this.extractData(res.data);
      this.updateError(null);
       console.log(res.data);
    }, (err: HttpErrorResponse) => {
      this.updateError(err);
      this.updateLoader(false);
      this.setAdminVisitorLogToEmpty();
    }
    );
  }

  extractData(data) {
    console.log(data);
    const adminVisitorLog: AdminVisitorLogTable[] = [];

    for (const s of data) {
        adminVisitorLog.push(
       {
        visitorName : s.VisitorFullName,
        picture : s.VisitImage,
        company : s.VisitorCompany,
        email : s.Email,
        phone : s.VisitorPhoneNumber,
        purpose : s.Purpose,
        signedIn : new Date(s.TimeIn),
        signedOut : new Date(s.TimeOut)
       }
       );

    }
    this.loaderSource.next(false);
    this.updateError(null);
    this.updateAdminVisitorLogs(adminVisitorLog);

  }

  updateAdminVisitorLogs(adminVisitorLog) {
    this.adminVisitorLogs.next(adminVisitorLog);
  }

  updateError(message) {
    this.errorSource.next(message);
    console.log(message);
  }

  updateLoader(state) {
    this.loaderSource.next(state);
  }

  setAdminVisitorLogToEmpty() {
    const  emptyVisitorLogTable: AdminVisitorLogTable[] = [];
    this.adminVisitorLogs.next(emptyVisitorLogTable);
  }

  instantiateDefaultAdminVisitorLog() {

    const endDate = new Date();
    const startDate = new Date(endDate.getTime() - (7 * 24 * 60 * 60 * 1000));

    const EndDate = `${this.convert(endDate)} 00:00`;
    const StartDate = `${this.convert(startDate)} 00:00`;

    const adminVisitorlogModel = new AdminVisitorLogModel(StartDate, EndDate, 5);
    console.log(adminVisitorlogModel);
    this.getAdminVisitorLogData(adminVisitorlogModel);
  }

  convert(str) {
    const date = new Date(str),
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2);
    return [mnth, day, date.getFullYear()].join('/');
  }
}
