import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDatepicker } from '@angular/material';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as _moment from 'moment';
import { AdminVisitorLogModel, AdminVisitorLogTable } from './admin-visitor-log-model';
import { AdminVisitorLogService } from './admin-visitor-log.service';
import { HttpErrorResponse } from '@angular/common/http';

const moment = _moment;

@Component({
  selector: 'app-visitor-log',
  templateUrl: './visitor-log.component.html',
  styleUrls: ['./visitor-log.component.scss']
})
export class VisitorLogComponent implements OnInit {

  allVisitors : AdminVisitorLogTable[];
  displayedColumns: string[] = ['picture', 'visitorName', 'company', 'email','phone', 'purpose', 'signedIn', 'signedOut'];
  dataSource: MatTableDataSource<AdminVisitorLogTable>
  errorMessage: string;
  loading: boolean;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  createSearchForm: FormGroup;

  endDate = new Date();
  startDate = new Date(this.endDate.getTime() - (7 * 24 * 60 * 60 * 1000));

  constructor(public fb: FormBuilder, public adminVisitorLogService : AdminVisitorLogService, private toastr : ToastrService) {
  }

  ngOnInit() {
      this.adminVisitorLogService.adminVisitorLogs$.subscribe(res => {
        this.allVisitors = res;
      });
      setTimeout(() => {
        this.dataSource = new MatTableDataSource<AdminVisitorLogTable>(this.allVisitors);
        this.dataSource.paginator = this.paginator;
      }, 1500);

      this.adminVisitorLogService.loader$.subscribe(loader => this.loading = loader);
      this.adminVisitorLogService.error$.subscribe(err => this.errorMessage = err);
   
    this.createReactiveform();
    
  }

  //Filter Table
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  createReactiveform() {
    this.createSearchForm = this.fb.group({
      startDate: [this.startDate, Validators.required],
      endDate: [this.endDate, Validators.required],
      displayLength: [5]
    });
  }

  submitVisitorLog() {
    this.adminVisitorLogService.updateLoader(true);
    this.adminVisitorLogService.loader$.subscribe(loader => this.loading = loader);
    console.log(this.createSearchForm.value);
    if (this.createSearchForm.valid) {
      const lstartDate = this.createSearchForm.controls['startDate'].value;
      const lendDate = this.createSearchForm.controls['endDate'].value;
      const displayLength = this.createSearchForm.controls['displayLength'].value;

      const startDate = `${this.adminVisitorLogService.convert(lstartDate)} 00:00`;
      const endDate = `${this.adminVisitorLogService.convert(lendDate)} 23:59`;

      const adminVisitorModel = new AdminVisitorLogModel(startDate, endDate,displayLength);


      this.adminVisitorLogService.getAdminVisitorLog(adminVisitorModel).subscribe(res => {

        if (res.code === '00') {
          if (res.data.length === 0) {
            this.toastr.info('No result Found for your search criteria.');
            const emptVisitorLogModel: AdminVisitorLogModel[] = [];
            this.adminVisitorLogService.extractData(emptVisitorLogModel);
            this.adminVisitorLogService.adminVisitorLogs.subscribe( res => {
              this.allVisitors = res;
            });
          } else {
            this.adminVisitorLogService.extractData(res.data);
            this.adminVisitorLogService.adminVisitorLogs.subscribe( res => {
              this.allVisitors = res;
            });
          }
        } else {
          this.adminVisitorLogService.updateLoader(false);
          this.toastr.error('An error occured while processing your request');
        }
        console.log(this.allVisitors);
        this.dataSource = new MatTableDataSource<AdminVisitorLogTable>(this.allVisitors);
        this.dataSource.paginator = this.paginator;
        this.adminVisitorLogService.updateError(null);
      console.log(res.data);
      }, ( err: HttpErrorResponse) => {
     this.adminVisitorLogService.updateError(err);
     this.adminVisitorLogService.updateLoader(false);
     this.adminVisitorLogService.setAdminVisitorLogToEmpty();
    });
  }

}
}
