import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeaturedRoutingModule } from './featured-routing.module';
import { StaffRoutingModule } from './staff/staff-routing.module';
import { ReceptionistRoutingModule } from './receptionist/receptionist-routing.module';
import { StaffModule } from './staff/staff.module';
import { ReceptionistModule } from './receptionist/receptionist.module';
import { AdminVmsModule } from './admin-vms/admin-vms.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FeaturedRoutingModule,
    StaffRoutingModule,
    AdminVmsModule,
    ReceptionistRoutingModule,
    StaffModule,
    AdminVmsModule,
    ReceptionistModule

  ]
})
export class FeaturedModule { }
