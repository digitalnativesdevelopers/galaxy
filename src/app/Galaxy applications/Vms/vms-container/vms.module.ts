import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreModule } from './Core/core.module';
import { SharedModule } from './Shared/shared.module';
import { FeaturedModule } from './Featured/featured.module';
import { FeaturedRoutingModule } from './Featured/featured-routing.module';
import { VmsRoutingModule } from './vms-routing.module';
import { VmsContainerComponent } from './vms-container.component';
import { AdminVmsModule } from './Featured/admin-vms/admin-vms.module';
import { ChartsModule } from 'ng2-charts/ng2-charts';


@NgModule({
  declarations: [VmsContainerComponent],
  imports: [
    CommonModule,
    VmsRoutingModule,
    CommonModule,
    FeaturedRoutingModule,
    VmsRoutingModule,
    FeaturedModule,
    SharedModule,
    CoreModule,
    ChartsModule
  ]
})
export class VmsModule { }
