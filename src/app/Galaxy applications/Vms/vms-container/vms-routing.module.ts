import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VmsContainerComponent } from './vms-container.component';
import { AdminVmsComponent } from './Featured//admin-vms/admin-vms.component';
// import { VmsAuthGuardGuard } from './Shared/AuthGuard/vms-auth-guard.guard';

const routes: Routes = [
  { path: '', component: VmsContainerComponent,
    children: [
      {path : '', redirectTo : 'receptionist' , pathMatch : 'full'},
      {path: 'receptionist', loadChildren: './Featured/receptionist/receptionist.module#ReceptionistModule'},
      {path: 'staff', loadChildren: './Featured/staff/staff.module#StaffModule'},
     {path: 'admin', loadChildren: './Featured/admin-vms/admin-vms.module#AdminVmsModule'}


    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VmsRoutingModule { }

