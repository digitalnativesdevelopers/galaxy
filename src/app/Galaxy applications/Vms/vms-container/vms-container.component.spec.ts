import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VmsContainerComponent } from './vms-container.component';

describe('VmsContainerComponent', () => {
  let component: VmsContainerComponent;
  let fixture: ComponentFixture<VmsContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VmsContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VmsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
