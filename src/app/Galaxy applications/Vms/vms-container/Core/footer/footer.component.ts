import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vmsfooter',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  year: number;

  constructor() { }

  getYear() {
    const date = new Date();
    this.year = date.getFullYear();
  }

  ngOnInit() {
    this.getYear();
  }

}
