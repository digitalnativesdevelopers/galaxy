import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserServiceService } from '../Featured/admin/user-service.service';
import { retry, catchError } from 'rxjs/operators';
import { resetComponentState } from '@angular/core/src/render3/instructions';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
const param = {
  'staffId': '',
  'platform': ''
};

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  role:  string;

  staffRole = new BehaviorSubject<any>(null);
  staffRole$ = this.staffRole.asObservable();

  staffId: Subject<any> =  new BehaviorSubject<any>(null);
  staffDetails = new BehaviorSubject<any>(null);
  staffDetails$ = this.staffDetails.asObservable();

  constructor( private adalSvc: MsAdalAngular6Service,
    private route: Router,
    private userService: UserServiceService,
    private http: HttpClient) { }

  _checkStaffRole (): Observable<any> {
    return this.http.post(environment.galaxy_checkStaffRole, param)
    .pipe(
      retry(3), catchError(this.handleError)
    );
  }

  enrolStaff (staffDetails): Observable<any> {
    return this.http.post(environment.galaxy_addNewUser, staffDetails);
  }

  _getStaffDetails (): Observable<any> {
    return this.http.post(environment.galaxy_getUserDetails, param)
    .pipe(
      retry(3), catchError(this.handleError)
    );
  }

  getStaffDetails() {
    this._getStaffDetails().subscribe( details => {
      if ( details.ResponseCode === '00') {
        this.staffDetails.next(details.ResponseObject[0]);
      }
    });
  }

  enrolNewUser (staffDetails) {
    this.enrolStaff(staffDetails).subscribe( res => {
      this.userService.getAllUsers();
    });
    localStorage.setItem('Staff Role', JSON.stringify('users'));
  }

  get isAdmin () {
    let roles: any;
    this.staffRole$.subscribe(res => {
      roles = window.atob(res);
    });
    if (roles.includes('Super Admin')) {
      return true;
    } else {
      return false;
    }
  }

  // Check Staff role
  getStaffRole() {
    const roles: any = [];
   this._checkStaffRole().subscribe(res => {
    for ( const role of res.ResponseObject) {
      roles.push(window.btoa(role.RoleName));
    }
    if (!sessionStorage.getItem('***')) {
      sessionStorage.setItem('***', JSON.stringify(roles));
    } else {
      sessionStorage.clear();
      sessionStorage.setItem('***', JSON.stringify(roles));
    }
    this.staffRole.next(roles);
   },
   (err: HttpErrorResponse) => {
     console.log(err);
   }
   );
  }

  handleError( error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }

}
