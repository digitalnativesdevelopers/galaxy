import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RoleguardService implements CanActivate {

  constructor( private router: Router ) { }

  canActivate( route: ActivatedRouteSnapshot ): boolean {
    const role = localStorage.getItem('Staff Role');
    console.log(role);
    if (role === 'users') {
      console.log('User permitted');
      return true;
    }
    this.router.navigate(['home']);
    return false;
  }
}
