import { Component, OnInit } from '@angular/core';
import { StatisticsService } from '../statistics.service';

@Component({
  selector: 'app-chart1',
  templateUrl: './chart1.component.html',
  styleUrls: ['./chart1.component.scss']
})

export class Chart1Component implements OnInit {
  InactiveAppsCount: number;
  allAppsCount: number;
  allCategoriesCount: number;
  usersCount: number;

  constructor( private stat: StatisticsService ) { }
  // Get count of all categories
  getCategoryCount() {
    this.stat.categoryCount.subscribe( res => {
      this.allCategoriesCount = res;
    });
  }
  // get count of all active apps
  getallAppsCount() {
    this.stat.appsCount.subscribe( res => {
      this.allAppsCount = res;
    });
  }
  // Get count of all Inactive apps
  getInactiveAppsCount() {
    this.stat.InactiveAppsCount.subscribe( res => {
      this.InactiveAppsCount = res;
    });
  }

  // Get count of all Users apps
  getUsersCount() {
    this.stat.usersCount.subscribe( res => {
      this.usersCount = res;
    });
  }


  ngOnInit() {
    this.stat.getTotalNoOfCategories();
    this.stat.getTotalNoOfInactiveApplications();
    this.stat.getTotalNoOfApplications();
    this.stat.getTotalNoOfUsers();
    this.getCategoryCount();
    this.getInactiveAppsCount();
    this.getallAppsCount(); // for all active apps
    this.getUsersCount();
  }

}
