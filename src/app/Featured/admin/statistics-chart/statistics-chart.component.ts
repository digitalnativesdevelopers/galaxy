import { Component, OnInit } from '@angular/core';
import { StatisticsService } from './statistics.service';

@Component({
  selector: 'app-statistics-chart',
  templateUrl: './statistics-chart.component.html',
  styleUrls: ['./statistics-chart.component.scss']
})
export class StatisticsChartComponent implements OnInit {
  categoryCount: number;
  appsCount: number;
  usersCount: number;
  constructor(private statService: StatisticsService) { }

  getCategoryCount() {
    this.statService.categoryCount.subscribe( res => {
      this.categoryCount = res;
    });
  }

  getAppsCount() {
    this.statService.appsCount.subscribe( res => {
      this.appsCount = res;
    });
  }

  getUsersCount() {
    this.statService.usersCount.subscribe( res => {
      this.usersCount = res;
    });
  }


  ngOnInit() {
    this.statService.getTotalNoOfCategories();
    this.statService.getTotalNoOfApplications();
    this.statService.getTotalNoOfUsers();
    this.getCategoryCount();
    this.getAppsCount();
    this.getUsersCount();
  }

}
