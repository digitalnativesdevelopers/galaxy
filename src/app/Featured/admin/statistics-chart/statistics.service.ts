import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { Promise } from 'q';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};


@Injectable({
  providedIn: 'root'
})

export class StatisticsService {
  categoryCount: Subject<any> = new BehaviorSubject<any>(null);
  appsCount: Subject<any> = new BehaviorSubject<any>(null);
  usersCount: Subject<any> = new BehaviorSubject<any>(null);
  InactiveAppsCount: Subject<any> = new BehaviorSubject<any>(null);
  ActiveAppsCount: Subject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }

  totalNoOfCategories() {
    return this.http.get(environment.galaxy_admin_totalNoOfCategories, httpOptions);
  }

  totalNoOfApplications() {
    return this.http.get(environment.galaxy_admin_totalNoOfApplications, httpOptions);
  }
  totalNoOfInactiveApplications() {
    return this.http.get(environment.galaxy_admin_totalNoOfInactiveApps, httpOptions);
  }

  totalNoOfUsers() {
    return this.http.get(environment.galaxy_admin_totalNoOfUsers, httpOptions);
  }

  getTotalNoOfCategories() {
    this.totalNoOfCategories().subscribe( res => {
      this.extractCategoriesData(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

  getTotalNoOfApplications() {
    this.totalNoOfApplications().subscribe( res => {
      this.extractApplicationData(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

  getTotalNoOfInactiveApplications() {
    this.totalNoOfInactiveApplications().subscribe( res => {
      this.extractInactiveApplicationData(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

  getTotalNoOfUsers () {
    this.totalNoOfUsers().subscribe( res => {
      this.extractUsersData(res);
    },
      (err: HttpErrorResponse) => console.log(err)
    );
  }

  extractCategoriesData(response) {
    if (response) {
      let numberOfCategories: number ;
      numberOfCategories = response.ResponseObject[0]['Total number of categories'];
      this.categoryCount.next(numberOfCategories);
    }
  }

  extractApplicationData(response) {
    if (response) {
      let numberOfApps: number ;
      numberOfApps = response.ResponseObject[0]['Total number of Applications'];
      this.appsCount.next(numberOfApps);
    }
  }

  extractInactiveApplicationData(response) {
    if (response) {
      let numberOfInactiveApps: number ;
      numberOfInactiveApps = response.ResponseObject[0]['Total number of Inactive Applications'];
      this.InactiveAppsCount.next(numberOfInactiveApps);
    }
  }

  extractUsersData(response) {
    if (response) {
      let numberOfUsers: number ;
      numberOfUsers = response.ResponseObject[0]['Total number of Users'];
      this.usersCount.next(numberOfUsers);
    }
  }
}
