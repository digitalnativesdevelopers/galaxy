import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Users } from './app-details-container/users/usersModel';
import { Subject, BehaviorSubject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { catchError, retry } from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  users: Subject<Users[]> = new BehaviorSubject<any>(null);

  loader = new BehaviorSubject<any>(false);
  loader$ = this.loader.asObservable();

  constructor( private http: HttpClient, private toastr: ToastrService
    // private alerts: AlertsService
    ) { }

assignStaffRoleApiCall (staffDetails): Observable<any> {
  return this.http.post(environment.galaxy_admin_addStaffRole, staffDetails)
  .pipe (
    retry(3), catchError (this.handleError)
  );
}

fetchUsers (): Observable<any> {
  return this.http.get( environment.galaxy_admin_getAllUsers, httpOptions)
  .pipe (
    retry(3), catchError (this.handleError)
  );
}

editRole(staffDetails): Observable<any> {
  return this.http.post(environment.galaxy_admin_editStaffRole, staffDetails )
  .pipe (
    retry(3), catchError (this.handleError)
  );
}

_deleteRole(details): Observable<any> {
  return this.http.post( environment.galaxy_admin_removeStaffFromRole, details, httpOptions)
  .pipe (
    retry(3), catchError (this.handleError)
  );
}

assignStaffRole(staffDetails) {
  this.assignStaffRoleApiCall(staffDetails).subscribe( res => {
    this.toastr.success('Role assigned successfully');
    this.getAllUsers();
  },
  (err: HttpErrorResponse) => {
    this.toastr.error('Error assigning role, please try again');
    console.log(err);
  }
  );
}

deleteRole(details) {
  this._deleteRole(details).subscribe( res => {
    // console.log(res);
    this.toastr.success('Staff Role removed successfully');
    this.getAllUsers();
  },
  (err: HttpErrorResponse) => {
    this.toastr.error('Error removing staff from role, please try again');
    console.log(err);
  }
  );
}

editStaffRole(staffDetails) {
  console.log(staffDetails);
  this.editRole(staffDetails).subscribe( response => {
    console.log(response);
    if (response.ResponseCode !== '00') {
      this.toastr.error('Staff role not edited,ensure you have all details right then try again');
    } else {
      this.toastr.success('Role edited successfully');
      this.getAllUsers();
    }
  },
  (err: HttpErrorResponse) => {
    this.toastr.error('Error editing role, please try again');
    console.log(err);
  }
  );
}

addStaffRole(staffDetails) {
  this.assignStaffRoleApiCall(staffDetails).subscribe( res => {
    if (res.ResponseCode !== '00') {
      this.toastr.error('Role not assigned,ensure you have all details right then try again');
    } else {
      this.toastr.success('Role assigned successfully');
      this.getAllUsers();
    }
  },
  (err: HttpErrorResponse) => {
    this.toastr.error('Role not assigned, please try again');
    console.log(err);
  }
  );
}

// get all users
getAllUsers() {
  this.updateLoader(true);
  this.fetchUsers().subscribe( res => {
    this.extractUsers(res.ResponseObject);
  },
  ( err: HttpErrorResponse ) => {
    this.updateLoader(true);
    console.log(err);
  }
  );
}

// Extract users
extractUsers(response) {
  if (response) {
    const users = [];
    for (const user of response) {
      users.push(
        {
          'StaffId': user.StaffId,
          'StaffName': user.StaffName,
          'StaffEmail': user.StaffEmail,
          'RoleId' : user.RoleId,
          'RoleName': user.RoleName,
        }
      );
    }
    this.users.next(users);
    this.updateLoader(false);
  }
}

updateLoader(state) {
  this.loader.next(state);
}


handleError( error: HttpErrorResponse) {
  console.log(error);
  return throwError(error);
}


}
