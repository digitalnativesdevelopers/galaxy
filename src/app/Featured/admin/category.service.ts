import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, Subject, Observable, throwError } from 'rxjs';
import { Category } from './AppCategoryModel';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';
import { retry, catchError } from 'rxjs/operators';
// import { AlertsService } from 'angular-alert-module';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  appCategories: Subject<any> = new BehaviorSubject<any>(null);
  category: Category[];

  platform = { 'Platform': 'web'};

  loader = new BehaviorSubject<any>(false);
  loader$ = this.loader.asObservable();

  constructor(private http: HttpClient,
    private toastr: ToastrService
   // private alerts: AlertsService
    ) {}
 // allAppByAppCategory
  fetchApi(): Observable<any> {
    return this.http.post(environment.galaxy_admin_allAppByCategory,  this.platform)
    .pipe (
      retry(3), catchError (this.handleError)
    );
  }

  getAllCategories() {
    this.updateLoader(true);
    this.fetchApi().subscribe( res => {
      this.extractData(res.Categories);
    },
    (err: HttpErrorResponse) => {
      this.updateLoader(true);
      console.log(err);
    }
    );
  }

  // Extract the need data form the response
  extractData(response) {
    if (response) {
      const categories: Category[] = [];
      for (const category of response ) {
        // console.log(response)
        categories.push(
          new Category (category.CategoryName, category.CategoryDesc, category.CategoryID)
        );
      }
      this.appCategories.next(categories);
      this.updateLoader(false);
    }
  }

  // Method to edit a category
  editCategory(id: string, details): Observable<any> {
    return this.http.post<any>(environment.galaxy_admin_editCategory + id, details);
  }
  // Subscription for the editCategory observable
  extractUpdatedCategory(id: string, details) {
    this.editCategory(id, details).subscribe( res => {
      this.toastr.success('Category edited successfully');
     // this.alerts.setMessage('Application Category Edited succesfully', 'success');
      this.getAllCategories();
    },
    (err: HttpErrorResponse) => {
      this.toastr.error('Error editing the category');
    //  this.alerts.setMessage('Error editing the category, please try again', 'error');
      console.log(err);
    });
  }

  // Method to delete category
  deleteCategory(id): Observable<any> {
    return this.http.post<any>(environment.galaxy_admin_deleteCategory + id, id);
  }
  subscribeDeleteCategory(id) {
    this.deleteCategory(id).subscribe( res => {
      this.toastr.success('Category deleted');
    //  this.alerts.setMessage('Application Category deleted succesfully', 'success');
      this.getAllCategories();
    },
    (err: HttpErrorResponse) => {
      this.toastr.error('Category not deleted, pls try again');
    //  this.alerts.setMessage('Error deleting the category, please try again', 'error');
      console.log(err);
    });
  }

  addCategory(details): Observable<any> {
    return this.http.post<any>(environment.galaxy_admin_addCategory , details);
  }
  subscribeAddCategory(detail) {
    this.addCategory(detail).subscribe(res => {
      this.toastr.success('Category created successfully');
     // this.alerts.setMessage('Application Category created succesfully', 'success');
      this.getAllCategories();
    },
    (err: HttpErrorResponse) => {
      this.toastr.error('Category not created');
    //  this.alerts.setMessage('Error creating the category, please try again', 'error');
      console.log(err);
    });
  }

  updateLoader(state) {
    this.loader.next(state);
  }

  handleError( error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }
}
