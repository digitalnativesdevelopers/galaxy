import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../app-service.service';
import { MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { AuthenticationService } from 'src/app/Auth/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  userName: string;
  loggedIn: boolean;
  staffRole: string;

  @Input() apps: any;
  constructor(
    private route: Router,
    private auth: AuthenticationService,
    private _appService: AppService,
    private adalSvc: MsAdalAngular6Service
    ) {  }

  getUserDetails () {
    this.auth.staffDetails$.subscribe( res => {
      if (res) {
        this.userName = res.StaffName;
      }
    });
  }

  ngOnInit() {
    this.getUserDetails();
  }

}
