export class Category {
    id?: number;
    name: string;
    // icon: any;
    description: string;

    constructor( name: string, desc: string, id?: number) {
        this.name = name;
        this.description = desc;
        // this.icon = icon;
        this.id = id;
    }
}
