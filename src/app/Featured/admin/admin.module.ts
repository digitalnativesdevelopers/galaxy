import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ActionBarComponent } from './action-bar/action-bar.component';
import { AppDetailsContainerComponent } from './app-details-container/app-details-container.component';
// import { NgSemanticModule } from 'ng-semantic';
// import { NgSemanticModule } from 'ng-semantic/ng-semantic';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { DonutChartService } from './statistics-chart/chart4/donut-chart.service';
import { AppDetailsContainerModule } from './app-details-container/app-details-container.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MorrisChartDirective } from './admin-directives/morris-chart.directive';


@NgModule({
  declarations: [
    AdminComponent,
    FooterComponent,
    AppDetailsContainerComponent,
    MorrisChartDirective,
    MorrisChartDirective,
  ],
  imports: [
    CommonModule,
    MDBBootstrapModule,
    AppDetailsContainerModule,
    FormsModule, ReactiveFormsModule,
    AdminRoutingModule,
  ],
  exports : [
    AdminComponent,
    FooterComponent,
    AppDetailsContainerComponent,
  ],
  providers: [
    DonutChartService
  ]
})
export class AdminModule { }
