export class App {
    id?: number;
    name: string;
    url: string;
    icon: any;
    status: string;
    category: string;
    description: string;
    dateOfCreation: string;

    constructor(name: string, url: string, icon: any, status: string, desc: string, category: string, date: string, id?: number) {
        this.name = name;
        this.url = url;
        this.icon = icon;
        this.status = status;
        this.description = desc;
        this.id = id;
        this.category = category;
        this.dateOfCreation = date;
    }
}
