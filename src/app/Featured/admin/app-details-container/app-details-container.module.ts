import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppDetailsContainerRoutingModule } from './app-details-container-routing.module';
import { StatisticsChartComponent } from '../statistics-chart/statistics-chart.component';
import { Chart1Component } from '../statistics-chart/chart1/chart1.component';
import { Chart2Component } from '../statistics-chart/chart2/chart2.component';
import { Chart3Component } from '../statistics-chart/chart3/chart3.component';
import { Chart4Component } from '../statistics-chart/chart4/chart4.component';
import { AppsComponent } from './apps/apps.component';
import { CategoriesComponent } from './categories/categories.component';
import { HeaderComponent } from '../header/header.component';
import { ActionBarComponent } from '../action-bar/action-bar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { UsersComponent } from './users/users.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { LoaderComponent } from './../../../loader/loader.component';
// import { AlertsModule } from 'angular-alert-module';
import { ErrorNotificationComponent } from '../../../error-notification/error-notification.component';

@NgModule({
  declarations: [
    LoaderComponent,
    ErrorNotificationComponent,
    HeaderComponent,
    ActionBarComponent,
    StatisticsChartComponent,
    Chart1Component, Chart2Component, Chart3Component, Chart4Component,
    AppsComponent, CategoriesComponent, UsersComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    // AlertsModule,
    ReactiveFormsModule,
    FormsModule,
    MDBBootstrapModule,
    MatTableModule,
    AppDetailsContainerRoutingModule,
  ],
  exports: [
    LoaderComponent, ErrorNotificationComponent,
    StatisticsChartComponent, HeaderComponent,
    ActionBarComponent,
    Chart1Component, Chart2Component, Chart3Component, Chart4Component,
    AppsComponent, CategoriesComponent, UsersComponent
  ],
  providers: [
  ]
})
export class AppDetailsContainerModule { }
