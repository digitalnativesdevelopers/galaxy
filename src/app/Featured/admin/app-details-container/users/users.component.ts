import { App } from './../../appModel';
import { AppService } from './../../app-service.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserServiceService } from '../../user-service.service';
import { Users } from './usersModel';
import * as $ from 'jquery';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  editStaffRoleForm: FormGroup;
  users: Users[];
  loading: boolean;
  appRoles: any;
  applications: App[];
  roleId: number;
  roleToDelete;

  constructor(
    private fb: FormBuilder,
    private appServ: AppService,
    private userService: UserServiceService,
    private http: HttpClient
  ) { }

  getUsers() {
    this.userService.users.subscribe( (users: any) => {
      this.userService.loader$.subscribe( state => {
        this.loading = state;
      });
      // Create users table as a data table
      $(document).ready(function () {
        (<any>$('#dtBasicExample')).DataTable();
        $('.dataTables_length').addClass('bs-select');
      });
      this.users = users;
    });
  }

   // get and set the edit category modal form
   setFormData (id, email, name) {
    this.editStaffRoleForm.controls['StaffName'].setValue(name);
    this.editStaffRoleForm.controls['StaffId'].setValue(id);
    this.editStaffRoleForm.controls['StaffEmail'].setValue(email);
   }

  ngOnInit() {
    // create edit staff role form
    this.editStaffRoleForm = this.fb.group({
        StaffName: [null, Validators.required],
        StaffId: [null, Validators.required],
        StaffEmail: [null, Validators.compose([Validators.required, Validators.email])],
        application: [null, Validators.required],
        Role: [null, Validators.required]
    });
    this.userService.getAllUsers();
    this.getUsers();
    this.getApplications();
  }

  changeStaffRole(staffDetails) {
    const details = {
      'StaffName': staffDetails.StaffName,
      'StaffEmail': staffDetails.StaffEmail,
      'RoleId': this.roleId,
      'StaffId': staffDetails.StaffId
    };
    this.userService.editStaffRole(details);
    this.getUsers();
  }

    // get all applications
    getApplications() {
      this.appServ.applications.subscribe( res => {
        this.applications = res;
      });
    }

  getAppRoles (appName): Observable<any> {
    return this.http.post(environment.galaxy_getRolesByAppName, {'AppName': appName}, httpOptions);
  }

  change(value) {
    this.getAppRoles(value).subscribe(res => {
      this.appRoles = res.ResponseObject;
    });
    // console.log(value);
  }

  pickId(value) {
    const appRole: any = this.appRoles.filter( (app) => app.RoleName === value);
    this.roleId = appRole[0].RoleId;
  }

  // get details of staff to delete his role
  getDetails(name, staffid, email, roleId) {
    this.roleToDelete = {
      'StaffName': name,
      'StaffEmail': email,
      'RoleId': roleId,
      'StaffId': staffid
    };
  }

  deleteRole() {
   this.userService.deleteRole(this.roleToDelete);
  }

}
