export interface Users {
    StaffId: string;
    StaffName: string;
    StaffEmail: string;
    RoleName: string;
    RoleId: number;
}
