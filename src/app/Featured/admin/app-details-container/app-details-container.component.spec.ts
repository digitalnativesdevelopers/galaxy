import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppDetailsContainerComponent } from './app-details-container.component';

describe('AppDetailsContainerComponent', () => {
  let component: AppDetailsContainerComponent;
  let fixture: ComponentFixture<AppDetailsContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppDetailsContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppDetailsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
