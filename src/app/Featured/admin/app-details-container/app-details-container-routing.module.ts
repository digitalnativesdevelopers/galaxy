import { AuthGuard } from './../../../Auth/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppDetailsContainerComponent } from './app-details-container.component';
import { StatisticsChartComponent } from '../statistics-chart/statistics-chart.component';
import { AppsComponent } from './apps/apps.component';
import { CategoriesComponent } from './categories/categories.component';
import { UsersComponent } from './users/users.component';


const routes: Routes = [
  {path: '',  canActivate: [AuthGuard], component: AppDetailsContainerComponent,
  children: [
    {path: '', component: StatisticsChartComponent},
    {path: 'dashboard', component: StatisticsChartComponent},
    {path: 'manage', component: AppsComponent},
    {path: 'categories', component: CategoriesComponent},
    {path: 'users', component: UsersComponent}
  ]
},
{path: 'home', loadChildren: '../../home-page/home-page.module#HomePageModule'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppDetailsContainerRoutingModule { }
