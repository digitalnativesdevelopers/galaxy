import { Component, OnInit } from '@angular/core';
import { AppService } from '../../app-service.service';
import { App } from '../../appModel';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '../../category.service';
import { Category } from '../../AppCategoryModel';
import { AuthenticationService } from 'src/app/Auth/authentication.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-apps',
  templateUrl: './apps.component.html',
  styleUrls: ['./apps.component.scss']
})
export class AppsComponent implements OnInit {
  apps: App[];
  editAppForm: FormGroup;
  appIcon: any;
  icon: string;
  appToUpdate: any = {}; // app selected to be updated
  appIdToDelete: number;
  appIdToUpdate: number;
  loading: boolean;
  categories: Category[];
  staffId: string;

  constructor(
    private toastr: ToastrService,
    private auth: AuthenticationService,
    private data: AppService,
    private fb: FormBuilder,
    private categoryService: CategoryService
    ) { }

  ngOnInit() {
    this.getStaffDetail();
    this.editAppForm = this.fb.group({
      appName: ['', Validators.required],
      appCategory: ['', Validators.required],
      appUrl: ['', Validators.required],
      appStatus: [null, Validators.required],
      appIcon: ['', Validators.required],
      appDesc: ['', Validators.compose([Validators.required, Validators.minLength(10)])]
    });
    this.data.getAllApps();
    this.getApps();
    this.getCategories();
    // this.showSuccess();
  }
  // get staff details
  getStaffDetail() {
    this.auth.staffId.subscribe( res => {
      this.staffId = res;
    });
  }

  showSuccess() {
    this.toastr.success('Hello world!', 'Toastr fun!');
  }

  getCategories() {
    this.categoryService.appCategories.subscribe( res => {
      this.categories = res;
    });
  }
  getApps() {
    this.data.applications.subscribe( (apps: any) => {
      // create apps table as a data table
      $(document).ready(function () {
        (<any>$('#dtBasicExample')).DataTable();
        $('.dataTables_length').addClass('bs-select');
      });
      // set apps array to all applications gotten the server
      this.apps = apps;
    });
    this.data.loader$.subscribe( res => {
      this.loading = res;
    });
  }

  change(value) {
    const appid = this.categories.filter( (category) => category.name === value);
    this.appIdToUpdate = appid[0].id;
  }
    // convert Icon to bse64
    toBase64(event, icon) {
      const reader = new FileReader(); // an instance of FileReader
      if (event.target.files && event.target.files.length > 0) {
        const file = event.target.files[0];
        reader.readAsDataURL(file);
        reader.onload = () => {
          if (icon === 'app') {
            // console.log(file)
            this.editAppForm.controls['appIcon'].setValue(file.name);
            this.appIcon = reader.result;
            this.appToUpdate.AppIcon = this.appIcon;
          }
        };
      }
      // return reader.result;
    }

     // get and set the edit category modal form
  setFormData(name, category, description, url, icon, id) {
    this.editAppForm.controls['appName'].setValue(name);
   // this.editAppForm.controls['appCategory'].setValue(category);
    this.editAppForm.controls['appDesc'].setValue(description);
    this.editAppForm.controls['appUrl'].setValue(url);
    // this.editAppForm.controls['appIcon'].setValue(icon);
    this.appToUpdate.ApplicationId = id;
  }

  // Edit app details
  editApp() {
    this.appToUpdate.AppName = this.editAppForm.controls['appName'].value;
    this.appToUpdate.AppCategory = this.editAppForm.controls['appCategory'].value;
    this.appToUpdate.AppUrl = this.editAppForm.controls['appUrl'].value;
    this.appToUpdate.CategoryId = this.appIdToUpdate;
    if ( this.editAppForm.controls['appStatus'].value === 'Inactive') {
      this.appToUpdate.AppStatus = 0 ;
    } else {
      this.appToUpdate.AppStatus = 1 ;
    }
    this.appToUpdate.AppDescription = this.editAppForm.controls['appDesc'].value;
    // this.appToUpdate.AppIcon = this.editAppForm.controls['appIcon'].value;
    this.data.subscribeEditApp(this.staffId, this.appToUpdate.ApplicationId, this.appToUpdate);
  }
  // set the id of the application to be updated
  getId(id) {
    console.log(id);
    this.appIdToDelete = id;
  }
  // delete Application
  deleteApp() {
    this.data.subscribeDeleteApp(this.staffId, this.appIdToDelete);
  }
}
