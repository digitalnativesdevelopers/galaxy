import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { CategoryService } from '../../category.service';
import { Category } from '../../AppCategoryModel';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
// import { AlertsService } from 'angular-alert-module';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  editCategoryForm: FormGroup; // form to edit category
  // Variable initialisation
  categories: Category[]; // all categories
  categoryToUpdate: any = {}; // categories selected to be updated
  categoryIdToDelete: number; // id of category selected to be deleted
  loading: boolean;

  constructor(
    private data: CategoryService, private fb: FormBuilder, private chRef: ChangeDetectorRef,
    // private alerts: AlertsService
    ) { }

  ngOnInit() {
    // create the edit category form
    this.editCategoryForm = this.fb.group({
      catName: ['', Validators.required],
      catDesc: ['', Validators.required]
    });
    this.data.getAllCategories();
    // call the get all categories method
    this.getCategories();
    // this.alerts.setMessage('All the fields are required', 'error');
  }

  // get the app categories
  getCategories() {
    this.data.appCategories.subscribe((categories: any) => {
      this.data.loader$.subscribe( state => {
        this.loading = state;
      });
      // create categories table as a data table
      $(document).ready(function () {
        (<any>$('#dtBasicExample')).DataTable();
        $('.dataTables_length').addClass('bs-select');
      });
      // set categories array to all categories gotten the server
      this.categories = categories;
    });
  }
  // get and set the edit category modal form
  setFormData(name, description, id) {
    this.editCategoryForm.controls['catName'].setValue(name);
    this.editCategoryForm.controls['catDesc'].setValue(description);
    this.categoryToUpdate.CategoryId = id;
  }

  editCategory() {
    this.categoryToUpdate.CategoryName = this.editCategoryForm.controls['catName'].value;
    this.categoryToUpdate.CategoryDescription = this.editCategoryForm.controls['catDesc'].value;
    this.data.extractUpdatedCategory(this.categoryToUpdate.CategoryId, this.categoryToUpdate);
  }
  // set the id of the category to be deleted
  getId(id) {
    this.categoryIdToDelete = id;
  }
  // delete category
  deleteCategory() {
    this.data.subscribeDeleteCategory(this.categoryIdToDelete);
  }
}
