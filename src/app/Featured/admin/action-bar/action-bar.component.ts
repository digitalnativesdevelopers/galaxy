import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { App } from '../appModel';
import { Category } from '../AppCategoryModel';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CategoryService } from '../category.service';
import { AppService } from '../app-service.service';
import { AuthenticationService } from 'src/app/Auth/authentication.service';
import { StatisticsService } from '../statistics-chart/statistics.service';
import { UserServiceService } from '../user-service.service';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Response } from '../../home-page/favourite-apps/favorite-app';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Component({
  selector: 'app-action-bar',
  templateUrl: './action-bar.component.html',
  styleUrls: ['./action-bar.component.scss']
})
export class ActionBarComponent implements OnInit {
  addAppForm: FormGroup;
  addCategoryForm: FormGroup;
  addUserForm: FormGroup;
  addRoleForm: FormGroup;
  appIcon: any;
  catIcon: any;
  app: App;
  appRoles: any;
  categories: Category[];
  categoryIdToEdit: number;
  applications: App[];
  staffId: string;
  roleId: number;

  constructor(
    private auth: AuthenticationService,
    private fb: FormBuilder,
    private http: HttpClient,
    private data: CategoryService,
    private appservice: AppService,
    private stat: StatisticsService,
    private userService: UserServiceService
    ) {}

  ngOnInit() {
    // this.createNewAppForm;
    this.addAppForm = this.fb.group ({
      appName: [null, Validators.required],
      appCategory: [null, Validators.required],
      appStatus: [null, Validators.required],
      appUrl: [null, Validators.required],
      appDesc: [null, Validators.compose([Validators.required, Validators.minLength(10)])],
      appIcon: [null, Validators.required]
    });
    // create New category form
    this.addCategoryForm = this.fb.group({
      categoryName: ['', Validators.required],
      categoryDescription: ['',  Validators.compose([Validators.required, Validators.minLength(10)])]
    });
    // create new user form
    this.addUserForm = this.fb.group( {
      StaffName: [null, Validators.required],
      StaffId: [null, Validators.required],
      StaffEmail: [null, Validators.compose([Validators.required, Validators.email])],
      application: [null, Validators.required],
      Role: [null, Validators.required]
    });

    // create new role form
    this.addRoleForm = this.fb.group({
      roleName: [null, Validators.required]
    });
    this.data.getAllCategories();
    this.getCategories();
    this.getApplications();
  }

  // get staff details
  getStaffDetail() {
    this.auth.staffId.subscribe( res => {
      this.staffId = res;
    });
  }

    // Create a new application
  createNewApp (formValue) {
    let appStatus: number;
    if (formValue.appStatus === 'active') {
      appStatus = 1;
    } else {
      appStatus = 0;
    }
    this.addAppForm.reset();
    this.appservice.addAppSub(
      {'AppName': formValue.appName,
       'AppDescription': formValue.appDesc,
       'AppIcon': this.appIcon,
       'AppCategory' : formValue.appCategory,
       'CategoryId': this.categoryIdToEdit,
       'AppStatus': appStatus,
       'AppUrl' : formValue.appUrl
      }
      );
  }
  // Assign staff role
  assignStaffRole(staffDetails) {
    const details = {
      'StaffName': staffDetails.StaffName,
      'StaffEmail': staffDetails.StaffEmail,
      'RoleId': this.roleId,
      'StaffId': staffDetails.StaffId
    };
    this.userService.addStaffRole(details);
  }

  // Create new role
  createNewRole(formValue) {
  }

  // Create new Category
  createNewCategory(formValue) {
    this.addCategoryForm.reset();
    this.data.subscribeAddCategory(
      {
      'CategoryName' : formValue.categoryName,
      'CategoryDescription' : formValue.categoryDescription
     }
    );
    this.stat.getTotalNoOfCategories();
    this.stat.categoryCount.subscribe( res => {
    });
  }

  // get all categories
  getCategories() {
    this.data.appCategories.subscribe( res => {
      this.categories = res;
    });
  }

  // get all applications
  getApplications() {
    this.appservice.applications.subscribe( res => {
      this.applications = res;
    });
  }

  getAppRoles (appName): Observable<any> {
    return this.http.post(environment.galaxy_getRolesByAppName, {'AppName': appName}, httpOptions);
  }

  change(value) {
    this.getAppRoles(value).subscribe(res => {
      this.appRoles = res.ResponseObject;
    });
  }

  changeCategory(value) {
    const appCategory = this.categories.filter( (category) => category.name === value);
    this.categoryIdToEdit = appCategory[0].id;
  }

  pickId(value) {
    const appRole: any = this.appRoles.filter( (app) => app.RoleName === value);
    this.roleId = appRole[0].RoleId;
  }
  // convert Icon to bse64
  toBase64(event, icon) {
    const reader = new FileReader(); // an instance of FileReader
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        if (icon === 'app') {
          this.addAppForm.controls['appIcon'].setValue(file.name);
          this.appIcon = reader.result;
        } else if (icon === 'cat') {
          this.addCategoryForm.controls['categoryIcon'].setValue(file.name);
          this.catIcon = reader.result;
        }
      };
    }
    // return reader.result;
  }

}
