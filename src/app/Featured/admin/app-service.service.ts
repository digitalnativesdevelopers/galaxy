import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { App } from './appModel';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
// import { AlertsService } from 'angular-alert-module';
import { ToastrService } from 'ngx-toastr';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class AppService {
  applications: Subject<App[]> = new BehaviorSubject<any>(null);
  // loader source
  private loaderSource = new BehaviorSubject<boolean>(false);
  loader$ = this.loaderSource.asObservable();

  constructor( private http: HttpClient,
    private toastr: ToastrService
    // private alerts: AlertsService
    ) { }

  fetchApps(): Observable<any> {
    return this.http.get(environment.galaxy_admin_getAllApps, httpOptions);
  }

  // get all apps by Staff I.d
  getAllApps() {
    this.updateLoader(true);
    this.fetchApps().subscribe(res => {
      this.extractAppData(res.ResponseObject);
    },
    (err: HttpErrorResponse) => {
      console.log(err);
      this.updateLoader(false);
    }
    );
  }
    extractAppData(response) {
      if (response) {
        const apps: App[] = [];
      for (const app of response) {
        if (app.CreationDate) {
          const creationDate = app.CreationDate.slice(0, -12);
          apps.push(
            new App (app.AppName, app.AppUrl, app.AppIcon, app.AppStatus, app.AppDescription, app.AppCategory, creationDate, app.AppId, )
          );
        } else {apps.push(
          new App (app.AppName, app.AppUrl, app.AppIcon, app.AppDescription, app.AppCategory, app.CreationDate, app.AppId, )
        );
        }
      }
      this.applications.next(apps);
      this.updateLoader(false);
      }
    }

    // Add a new application
  addApplication(details): Observable<any> {
    return this.http.post( environment.galaxy_admin_addApplication, details);
  }

  // Subscription to add application observable
  addAppSub(details) {
    this.addApplication(details).subscribe( res => {
      if (res.ResponseCode !== '00') {
        this.toastr.error('Application not created,ensure you have all details right then try again');
      } else {
        this.toastr.success('Application created');
        this.getAllApps();
      }
    },
    (err: HttpErrorResponse) => {
      this.toastr.error('Error creating the application');
      console.log(err);
    }
    );
  }

  // Edit aplication details
  editApplication(id, details): Observable<any> {
    return this.http.post<any>(environment.galaxy_admin_editApplication + id, details);
  }
  subscribeEditApp(staffId, id, details) {
    this.editApplication(id, details).subscribe( res => {
      if (res.ResponseCode !== '00') {
        this.toastr.error('Application not edited,ensure you have all details right then try again');
      } else {
        this.toastr.success('App edited successfully');
        this.getAllApps();
      }
    },
     (err: HttpErrorResponse) => {
       this.toastr.error('Error editing the application');
      console.log(err);
    }
     );
    }

     // Method to delete category
  deleteApp(id): Observable<any> {
    return this.http.post<any>(environment.galaxy_admin_deleteApplication + id, id );
  }

  subscribeDeleteApp(staffId, id) {
    this.deleteApp(id).subscribe( res => {
      this.toastr.success('Application deleted');
     // this.alerts.setMessage('Application deleted succesfully', 'success');
      this.getAllApps();
    },
    (err: HttpErrorResponse) => {
      this.toastr.error('Application not deleted, pls try again');
    //  this.alerts.setMessage('Application not deleted, please try again', 'error');
      console.log(err);
    });
  }


  // update loader
  updateLoader(state) {
    this.loaderSource.next(state);
  }
}
