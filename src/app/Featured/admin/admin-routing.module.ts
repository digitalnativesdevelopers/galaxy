import { AuthGuard } from './../../Auth/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AppDetailsContainerComponent } from './app-details-container/app-details-container.component';

const routes: Routes = [
  {path: '', component: AppDetailsContainerComponent,
  // children: [
  //   {path: '', canActivate: [AuthGuard], component: AppDetailsContainerComponent}
  // ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
