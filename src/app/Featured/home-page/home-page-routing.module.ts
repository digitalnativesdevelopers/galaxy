import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page.component';
import { AppDetailsContainerComponent } from '../admin/app-details-container/app-details-container.component';

const routes: Routes = [
  { path: '', component: HomePageComponent},
  { path: 'allapps', loadChildren: '../all-apps/all-apps.module#AllAppsModule' },
  { path: 'admin', loadChildren: '../admin/admin.module#AdminModule'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule { }
