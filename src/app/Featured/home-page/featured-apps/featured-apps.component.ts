import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FeaturedAppService } from './featured-app.service';

@Component({
  selector: 'app-featured-apps',
  templateUrl: './featured-apps.component.html',
  styleUrls: ['./featured-apps.component.scss']
})
export class FeaturedAppsComponent implements OnInit {

  constructor(private http: HttpClient, private featuredService: FeaturedAppService) { }

  ngOnInit() {
    this.displayFeaturedApps();
  }

  displayFeaturedApps() {
    // this.featuredService.getFeaturedApps().subscribe(
    //   response => (console.log(response))
    // );
  }

}
