import { TestBed } from '@angular/core/testing';

import { FeaturedAppService } from './featured-app.service';

describe('FeaturedAppService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FeaturedAppService = TestBed.get(FeaturedAppService);
    expect(service).toBeTruthy();
  });
});
