import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedAppsComponent } from './featured-apps.component';

describe('FeaturedAppsComponent', () => {
  let component: FeaturedAppsComponent;
  let fixture: ComponentFixture<FeaturedAppsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedAppsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedAppsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
