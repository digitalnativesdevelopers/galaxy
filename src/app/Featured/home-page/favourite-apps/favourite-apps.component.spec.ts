import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavouriteAppsComponent } from './favourite-apps.component';

describe('FavouriteAppsComponent', () => {
  let component: FavouriteAppsComponent;
  let fixture: ComponentFixture<FavouriteAppsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavouriteAppsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavouriteAppsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
