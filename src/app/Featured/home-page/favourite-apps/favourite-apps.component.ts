import { Application } from './../../../Core/application.model';
import { Component, OnInit } from '@angular/core';
import { FavoriteAppService } from './favorite-app.service';
import { isArray } from 'util';
import { AuthenticationService } from 'src/app/Auth/authentication.service';

@Component({
  selector: 'app-favourite-apps',
  templateUrl: './favourite-apps.component.html',
  styleUrls: ['./favourite-apps.component.scss']
})
export class FavouriteAppsComponent implements OnInit {
  favoriteApps: Application[];
  favoriteAppsCount: number;
  staffId: string;

  constructor(private auth: AuthenticationService, private favoriteService: FavoriteAppService) { }

  ngOnInit() {
    this.getStaffDetail();
    this.getFavoriteApps();
    this.favoriteService.getFavoriteApps(this.staffId);
  }

  // get staff details
  getStaffDetail() {
    this.auth.staffId.subscribe( res => {
      this.staffId = res;
    });
  }
  getFavoriteApps() {
     this.favoriteService.updatedFavoriteApp.subscribe(
       response => {
          this.favoriteApps = response;
          // // this.favoriteAppsCount = response.length;
          // console.log('favoriteAppsCount ');
          // console.log(response);
          // if (isArray(response)) { // fix this well
          //   this.favoriteAppsCount = response.length;
          // }
       }
     );
  }
  removeFromFavorite(appId: number) {
    this.favoriteService.removeAppFromFavorite(appId, this.staffId);
    this.favoriteService.removedSuccessFully.subscribe(res => {
      if (res) {
        const application = this.favoriteApps.find(( _val, _index, _arr) => _val.AppId === appId);
        const update = this.favoriteService.updateFavoriteAppsObservable(application, 'remove') === true ?
        this.updateFavoriteStatusForReconmendedApp(appId, false) : '';
        // console.log(this.favoriteApps);
      }
    });
    
  }

  private updateFavoriteStatusForReconmendedApp(appId: number, truthy: boolean) {
    this.favoriteApps.map((app, _index, _number) => {
      if (app.AppId === appId) {
        app.IsFavorite = truthy;
      }
    });
  }
}
