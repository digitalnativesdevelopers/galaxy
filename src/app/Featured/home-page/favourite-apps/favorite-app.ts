export class Response {
  responseCode: string;
  responseDescription: string;
}

export class FavoriteApp extends Response {

  appIcon: string;
  appId: number;
  appName: string;
  appUrl: string;

  constructor(appIcon: string, appId: number, appName: string, appUrl: string) {
    super();
    this.appIcon = appIcon;
    this.appId = appId;
    this.appName = appName;
    this.appUrl = appUrl;

  }
}

