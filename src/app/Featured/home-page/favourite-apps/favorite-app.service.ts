import { Application } from 'src/app/Core/application.model';
// import { Application } from './../../../Core/application.model';
import { FavoriteApplication } from './favorite-application.model';
import { Injectable } from '@angular/core';
import { FavoriteApp, Response } from './favorite-app';
import { Subject, BehaviorSubject, Observable, from } from 'rxjs';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Staff } from '../../../Core/staff.model';
import { ErrorsHandler } from 'src/app/Core/Errors';
import { mergeMap, retry } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class FavoriteAppService {
  staffFavoriteApps: Application[];
  addedSuccessfully: Subject<Boolean> = new BehaviorSubject<Boolean>(false);
  removedSuccessFully: Subject<Boolean> = new BehaviorSubject<Boolean>(false);
  updatedFavoriteApp: Subject<Application[]> = new BehaviorSubject<Application[]>(null);

  constructor(private http: HttpClient, private error: ErrorsHandler) {}

  getStaffFavoriteAppsFromService(staffId: string): Observable<any> {
    const path = environment.galaxy_GetStaffFavoriteApps;
    return this.http
      .post<any>(path, { staffId: staffId })
      .pipe
      // retry(3),

      // catchError(this.handleError) // then handle the error
      ();
  }

  removeStaffFavoriteAppsFromService(
    staffId: string,
    appId: number
  ): Observable<any> {
    const path = environment.galaxy_RemoveFavoriteAppFromService;
    return this.http
      .post<any>(path, {
        AppId: appId,
        StaffId: staffId
      })
      .pipe
      // retry(3),

      // catchError(this.handleError) // then handle the error
      ();
  }

  addStaffFavoriteAppToService(
    staffId: string,
    appId: number
  ): Observable<any> {
    const path = environment.galaxy_addFavoriteAppToService;
    return this.http
      .post<any>(path, {
        AppId: appId,
        StaffId: staffId
      })
      .pipe
      // retry(3),

      // catchError(this.handleError) // then handle the error
      ();
  }

  getFavoriteApps(staffId: string) {
    this.getStaffFavoriteAppsFromService(staffId).subscribe(
      res => {
        const favoriteApps = this.extractFavoriteApps(res);
        this.staffFavoriteApps = favoriteApps;
        this.updatedFavoriteApp.next(favoriteApps);
      },
      (err: HttpErrorResponse) => {
        // console.log(err);
        this.error.handleError(err);
      }
    );
  }

  extractFavoriteApps(res): Application[] {
    const favoriteApps = [];
    if (res.ResponseCode === '00') {
      const returnedApps = JSON.parse(res.ResponseMessage);

      for (const app of returnedApps) {
        const favoriteApp = new Application(
          app.AppName,
          app.AppURL,
          app.AppIcon,
          app.AppId,
          app.AppDesc
        );

        const favoriteLength = favoriteApps.push(favoriteApp);
      }
    }
    return favoriteApps;
  }

  // remove from favorites


  removeAppFromFavorite(appId: number, staffId: string) {
    console.log(appId);
    this.removeStaffFavoriteAppsFromService(staffId, appId).subscribe(
      
      res => {

        if (res.ResponseCode === '00') {
          this.removedSuccessFully.next(true);
        }
      },
      (err: HttpErrorResponse) => {
        // console.log(err);
        this.error.handleError(err);
        console.log(err);
      }
    );
  }

  addAppToFavorite(staffId: string, appId: number) {
    this.addStaffFavoriteAppToService(staffId, appId).subscribe(
      res => {

        if (res.ResponseCode === '00') {
          this.addedSuccessfully.next(true);
        }
      },
      (err: HttpErrorResponse) => {
        // console.log(err);
        this.error.handleError(err);
        console.log(err);
      }
    );
  }

  updateFavoriteAppsObservable(application: Application, action: string = 'remove'): Boolean {
    if (this.staffFavoriteApps.length > 2 && action === 'add') {
      // notify the user he cannot add more than 3 apps in his favorite
      console.log('You cannot have more than three favorite apps');


    } else if (this.staffFavoriteApps.includes(application) && action === 'add') {
      // notify the user the application is already in his favorites
      console.log(application.AppName + 'is already one of your favorites.');

    } else {


      if (action === 'add') {
        application.IsFavorite = true;
        this.staffFavoriteApps.push(application);
      } else {

        const _index = this.staffFavoriteApps.map(x => {
          return x.AppId;
        }).indexOf(application.AppId);
         this.staffFavoriteApps.splice(_index, 1);
      }

      this.updatedFavoriteApp.next(this.staffFavoriteApps);

      return true;
    }

    return false;
  }
}
