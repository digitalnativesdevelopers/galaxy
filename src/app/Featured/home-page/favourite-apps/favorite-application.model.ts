import { Application } from './../../../Core/application.model';
export class FavoriteApplication extends Application {


  constructor(appName: string, appUrl: string, appIcon: string, appId: number, appDesc) {
    super(appName, appUrl, appIcon, appId, appDesc);
  }
}
