import { TestBed } from '@angular/core/testing';

import { FavoriteAppService } from './favorite-app.service';

describe('FavoriteAppService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FavoriteAppService = TestBed.get(FavoriteAppService);
    expect(service).toBeTruthy();
  });
});
