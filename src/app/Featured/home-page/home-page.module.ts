import { SharedModule } from './../../shared/shared.module';
import { SafeHtml } from './../../shared/pipes/safe-html-pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageRoutingModule } from './home-page-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RecommendedAppsComponent } from './recommended-apps/recommended-apps.component';
import { FeaturedAppsComponent } from './featured-apps/featured-apps.component';
import { FavouriteAppsComponent } from './favourite-apps/favourite-apps.component';
import { RecentlyUsedComponent } from './recently-used/recently-used.component';
import { HomePageComponent } from './home-page.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MatAutocompleteModule, MatInputModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    HomePageComponent,
    HeaderComponent,
    FooterComponent,
    RecommendedAppsComponent,
    FeaturedAppsComponent,
    FavouriteAppsComponent,
    RecentlyUsedComponent,
  ],
  imports: [
    CommonModule,
    MatInputModule,
    NgbModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    MDBBootstrapModule,
    // BrowserAnimationsModule,
    HomePageRoutingModule,
    SharedModule
  ],
  exports: [
    HomePageComponent,
    HeaderComponent,
    FooterComponent,
    RecommendedAppsComponent,
    FeaturedAppsComponent,
    FavouriteAppsComponent,
    RecentlyUsedComponent
  ]
})
export class HomePageModule { }
