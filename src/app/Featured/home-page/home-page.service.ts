import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HomePageService {
  constructor() {}

  getFavApps() {}

  getFeaturedApps() {}

  getRecentluUsedApps() {}

  getRecommendedApps() {}
}
