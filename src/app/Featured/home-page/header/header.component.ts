import { Response } from './../favourite-apps/favorite-app';
import { Application } from './../../../Core/application.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import {Observable, Subject, merge} from 'rxjs';
import { MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { AuthenticationService } from 'src/app/Auth/authentication.service';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import {debounceTime, distinctUntilChanged, filter, startWith, map} from 'rxjs/operators';
import { AppService } from '../../admin/app-service.service';
import { App } from '../../admin/appModel';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
  // apps: Application[];
  apps;
  staffName: string;
  staffEmail: string;
  staffAvatar: string;
  staffAcronym: string;
  greeting: string;
  loggedIn: boolean;
  staffRole: string;
  staffId: string;
  isSuperAdmin: boolean;
  myControl = new FormControl();
  // let apps: App[] = [];
  options: string[] = [];
  filteredOptions: Observable<string[]>;


  constructor( private adalSvc: MsAdalAngular6Service,
    private auth: AuthenticationService,
    private appService: AppService
    ) {
    this.loggedIn = false;
  }

  getUserDetails () {
    this.auth.staffDetails$.subscribe( res => {
      if (res) {
        this.staffName = res.StaffName;
        this.staffAcronym = `${this.staffName.split(' ')[0][0]}${this.staffName.split(' ')[1][0]}`;
        this.staffAvatar = res.staffAvatar;
        this.staffEmail = res.Email;
      }
    });
  }
    get AdminStatus () {
      if (this.staffRole.includes('Super Admin')) {
        return true;
      }
      return false;
    }

  getApps () {
    this.appService.applications.subscribe(res => {
      // console.log(res);
      if (res) {
        for (const app of res ) {
          this.options.push(app.name);
        }
      }
      // console.log(this.options);
    });
  }
  // get User role abd check if user is an admin
  getUserRole() {
    this.auth.staffRole.subscribe( res => {
      if (res) {
        this.staffRole = window.atob(res);
        this.isSuperAdmin = this.AdminStatus;
      }
    });
  }

  greetUser() {
    const today = new Date();
    const curHr = today.getHours();
    if (curHr < 12) {
      this.greeting = 'Good Morning';
    } else if (curHr < 18) {
      this.greeting = 'Good Afternoon';
    } else {
      this.greeting = 'Good Evening';
    }
  }
  private _filter(value: string): string[] {
    const filterValue = value;
    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    this.greetUser();
    this.appService.getAllApps();
    this.getApps();
    this.getUserDetails();
    this.getUserRole();
  }
}
