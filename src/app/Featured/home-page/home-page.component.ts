import { Component, OnInit } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { AppService } from '../admin/app-service.service';
import { App } from '../admin/appModel';
import { MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { AuthenticationService } from 'src/app/Auth/authentication.service';
import { FavoriteAppService } from './favourite-apps/favorite-app.service';
import { RecentlyUsedAppService } from './recently-used/recently-used-app.service';
import { RecommendedService } from './recommended-apps/recommended.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  animations: [
    trigger('fade', [
      state('void', style({ opacity: 0 })),

      transition('void => *', [animate(4000)])
    ])
  ]
})
export class HomePageComponent implements OnInit {
  apps: App[];
  num = 0;
  staffId: string;
  staffRole: string;
  public now: Date = new Date();
  constructor(
    private _appService: AppService,
    private adalSvc: MsAdalAngular6Service,
    private authService: AuthenticationService,
    private favoriteAppsService: FavoriteAppService,
    private recentlyUsedService: RecentlyUsedAppService,
    private recommendedAppsService: RecommendedService
    ) {
      setInterval(() => {
        this.now = new Date();
      }, 1);
    }

  ngOnInit() {
    // this.autoSwitchComponent();
    this.getAllApps();
  }


  getAllApps() {
    this._appService.fetchApps(
    //   {
    //   'staffId': this.staffId
    // }
    ).subscribe(res => {
      this.apps = res.ResponseObject;
    });
  }

  switchComponent(val) {
    // console.log(this.num);
    // 0 = Recently Used app
    // 1 = Recommended apps
    // 2 = Featured apps
    if (val === 'backward') {
      if (this.num === 0) {
        this.num = 1; //
      } else if (this.num === 1) {
        this.num--;
      }
      // else if (this.num === 2) {
      //   this.num--;
      // }
    } else {
      if (this.num === 0) {
        this.num++;
      }
      // else if (this.num === 1) {
      //   this.num++;
      // }
      else if (this.num === 1) {
        this.num = 0;
      }
    }
  }

  autoSwitchComponent() {
    setInterval(() => {
      if (this.num === 0) {
        this.num++;
      } else if (this.num === 1) {
        this.num --;
      }
      // else {
      //   this.num = 0;
      // }
    }, 5000);
  }
}
