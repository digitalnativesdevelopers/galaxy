import { FavoriteAppService } from './../favourite-apps/favorite-app.service';
import { RecentlyUsedApplication } from './recently-used-application.model';
import { RecentlyUsedAppService } from './recently-used-app.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Application } from 'src/app/Core/application.model';
import { AuthenticationService } from 'src/app/Auth/authentication.service';

@Component({
  selector: 'app-recently-used',
  templateUrl: './recently-used.component.html',
  styleUrls: ['./recently-used.component.scss']
})
export class RecentlyUsedComponent implements OnInit {
  recentlyUsedApplications:  Application[];
  staffId: string;


  constructor(
    private auth: AuthenticationService,
    private recentlyUsedService: RecentlyUsedAppService,
    private favoriteService: FavoriteAppService) { }

  ngOnInit() {
    this.getStaffDetail();
    this.recentlyUsedService.getRecentlyUsedApps(this.staffId);
    this.getRecentlyUsedApp();
  }

   // get staff details
   getStaffDetail() {
    this.auth.staffId.subscribe( res => {
      this.staffId = res;
    });
  }
  getRecentlyUsedApp() {
    this.recentlyUsedService.recentlyUsedApp.subscribe(res => {
      this.recentlyUsedApplications = res;
    });
  }

  addToFavorite(appId: number) {
    if (this.staffId) {
      this.favoriteService.addAppToFavorite(this.staffId, appId);
      this.favoriteService.addedSuccessfully.subscribe(res => {
        if (res) {
          const application = this.recentlyUsedApplications.find(( _val, _index, _arr) => _val.AppId === appId);
          const update = this.favoriteService.updateFavoriteAppsObservable(application) === true ?
          this.updateFavoriteStatusForRecentlyUsedApp(appId, true) : '';
        }
      });
    }
  }

  private updateFavoriteStatusForRecentlyUsedApp(appId: number, truthy: boolean) {
    this.recentlyUsedApplications.map((app, _index, _number) => {
      if (app.AppId === appId) {
        app.IsFavorite = truthy;
      }
    });
  }
}
