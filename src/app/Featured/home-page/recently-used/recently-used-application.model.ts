import { Application } from 'src/app/Core/application.model';

export class RecentlyUsedApplication extends Application {
  constructor(appName: string, appUrl: string, appIcon: string, appId:  number, appDesc) {
    super(appName, appUrl, appIcon, appId, appDesc);
  }
}
