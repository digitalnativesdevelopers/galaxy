import { TestBed } from '@angular/core/testing';

import { RecentlyUsedAppService } from './recently-used-app.service';

describe('RecentlyUsedAppService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecentlyUsedAppService = TestBed.get(RecentlyUsedAppService);
    expect(service).toBeTruthy();
  });
});
