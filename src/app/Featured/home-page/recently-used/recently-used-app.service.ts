import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FavoriteApplication } from '../favourite-apps/favorite-application.model';
import { ErrorsHandler } from 'src/app/Core/Errors';
import { RecentlyUsedApplication } from './recently-used-application.model';
import { Application } from 'src/app/Core/application.model';

@Injectable({
  providedIn: 'root'
})
export class RecentlyUsedAppService {
  recentlyUsedApp: Subject<Application[]> = new BehaviorSubject<Application[]>(null);

  constructor(private http: HttpClient, private error: ErrorsHandler) { }

  getRecentlyUsedAppFromService(staffId: string): Observable<any> {

    const path = environment.galaxy_GetRecentlyUsedApps;

    return this.http.post<any>(path, {'staffId': staffId})
                     .pipe(
                       // retry(3),

                        // catchError(this.handleError) // then handle the error
                    );

   }

  getRecentlyUsedApps(staffId: string) {

    this.getRecentlyUsedAppFromService(staffId).subscribe(
      res => {
         const recentlyUsedApps =  this.extractRecentlyUsedApps(res);
         this.recentlyUsedApp.next(recentlyUsedApps);
      },
      (err: HttpErrorResponse) => {

        this.error.handleError(err);
      }
    );
  }

  extractRecentlyUsedApps(res): Application[] {
    const  recentlyUsedApps = [];
    if (res.ResponseCode === '00') {
      // console.log('is 00 for recently used');
      for (const app of res.ResponseObject) {
        const recentlyUsedApp = new Application(
          app.AppName,
          app.AppUrl,
          app.AppIcon,
          app.AppId,
          app.AppDescription
          );
          recentlyUsedApps.push(recentlyUsedApp);
      }
      // console.log(recentlyUsedApps);
    }
    return recentlyUsedApps;
  }
}
