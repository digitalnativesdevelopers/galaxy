import { Application } from 'src/app/Core/application.model';
import { Component, OnInit } from '@angular/core';
import { RecommendedService } from './recommended.service';
import { FavoriteAppService } from '../favourite-apps/favorite-app.service';
import { AuthenticationService } from 'src/app/Auth/authentication.service';

@Component({
  selector: 'app-recommended-apps',
  templateUrl: './recommended-apps.component.html',
  styleUrls: ['./recommended-apps.component.scss']
})
export class RecommendedAppsComponent implements OnInit {
  reconmendedApps: Application[];
  staffId: string;

  constructor(
    private auth: AuthenticationService,
    private recommendedService: RecommendedService,
    private favoriteService: FavoriteAppService
    ) { }

  ngOnInit() {
    this.getStaffDetail();
    this.getRecommendedApps();
    this.recommendedService.getReconmendedApps(this.staffId);
  }

  // get staff details
  getStaffDetail() {
    this.auth.staffId.subscribe( res => {
      this.staffId = res;
    });
  }

  getRecommendedApps() {
     this.recommendedService.reconmendedApps.subscribe(
       response => {
         this.reconmendedApps = response;
       }
     );
  }

  addToFavorite(appId: number) {
      this.favoriteService.addAppToFavorite(this.staffId, appId);
    this.favoriteService.addedSuccessfully.subscribe(res => {
      if (res) {
        const application = this.reconmendedApps.find(( _val, _index, _arr) => _val.AppId === appId);
        const update = this.favoriteService.updateFavoriteAppsObservable(application, 'add') === true ?
        this.updateFavoriteStatusForReconmendedApp(appId, true) : '';
      }
    });
  }

  private updateFavoriteStatusForReconmendedApp(appId: number, truthy: boolean) {
    this.reconmendedApps.map((app, _index, _number) => {
      if (app.AppId === appId) {
        app.IsFavorite = truthy;
      }
    });
  }
}
