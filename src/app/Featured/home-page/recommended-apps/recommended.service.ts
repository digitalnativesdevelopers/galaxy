import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Application } from 'src/app/Core/application.model';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { ErrorsHandler } from 'src/app/Core/Errors';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class RecommendedService {

  reconmendedApps: Subject<Application[]> = new BehaviorSubject<Application[]>(null);

  constructor(private http: HttpClient, private error: ErrorsHandler) { }

  getReconmendedAppsFromService(staffId: string): Observable<any> {

    const path = environment.galaxy_GetReconmendedApps;

    return this.http.post<any>(path, {'staffId': staffId, 'platfrom': 'web'})
                     .pipe(
                       // retry(3),

                        // catchError(this.handleError) // then handle the error
                    );

   }

  getReconmendedApps(staffId: string) {
    this.getReconmendedAppsFromService(staffId).subscribe(
      res => {
         const recentlyUsedApps =  this.extractReconmendedApps(res);
         this.reconmendedApps.next(recentlyUsedApps);
      },
      (err: HttpErrorResponse) => {

        this.error.handleError(err);
      }
    );
  }

  extractReconmendedApps(res): Application[] {
    const  reconmendedApps = [];

    if (res.ResponseCode === '00') {
      for (const app of res.ResponseObject) {
        const recentlyUsedApp = new Application(
          app.AppName,
          app.AppUrl,
          app.AppIcon,
          app.AppId,
          app.AppDescription
          );
          reconmendedApps.push(recentlyUsedApp);
      }
    }
    return reconmendedApps;
  }
}
