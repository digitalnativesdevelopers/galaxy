import { TestBed } from '@angular/core/testing';

import { AllAppsService } from './all-apps.service';

describe('AllAppsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AllAppsService = TestBed.get(AllAppsService);
    expect(service).toBeTruthy();
  });
});
