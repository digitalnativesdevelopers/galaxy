import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllAppsComponent } from './all-apps.component';

const routes: Routes = [
  {path: '', component: AllAppsComponent},
  {path: 'home', loadChildren: '../home-page/home-page.module#HomePageModule'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllAppsRoutingModule { }
