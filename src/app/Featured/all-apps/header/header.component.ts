import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/Auth/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  userName: string;

  constructor( private auth: AuthenticationService ) { }

  getUserDetails () {
    this.auth.staffDetails$.subscribe( res => {
      if (res) {
        this.userName = res.StaffName;
      }
    });
  }

  ngOnInit() {
    this.getUserDetails();
  }

}
