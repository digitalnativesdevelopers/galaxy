import { Component, OnInit } from '@angular/core';
import { AppsService } from './apps-service.service';
import { AuthenticationService } from 'src/app/Auth/authentication.service';

@Component({
  selector: 'app-apps-container',
  templateUrl: './apps-container.component.html',
  styleUrls: ['./apps-container.component.scss']
})
export class AppsContainerComponent implements OnInit {

  loading: boolean;

  constructor(private _appService: AppsService, private auth: AuthenticationService) { }



  ngOnInit() {
    this._appService.getAllApps();
  }

}
