import { Component, OnInit } from '@angular/core';
import { AppsService } from '../apps-service.service';
import { Category } from '../category.model';
import { FavoriteAppService } from '../../../home-page/favourite-apps/favorite-app.service';

@Component({
  selector: 'app-social-apps',
  templateUrl: './social-apps.component.html',
  styleUrls: ['./social-apps.component.scss']
})
export class SocialAppsComponent implements OnInit {
  categories: Category[ ];
  hasApp: boolean;
  constructor(private _appService: AppsService,
    private favoriteService: FavoriteAppService) { }

  getAllApps() {
    this._appService.allCategories.subscribe(response => {
      this.categories = response;
    });
  }

  ngOnInit() {
    this.getAllApps();
  }

}
