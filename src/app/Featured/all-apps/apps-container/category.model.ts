export class Category {
    name: string;
    apps: App[];

}

export class App {
    name: string;
    icon: string;
    url: string;
    isFavorite: boolean;
    id: number;
}

