import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject, throwError } from 'rxjs';
import { Category, App } from './category.model';
import { environment } from 'src/environments/environment';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppsService {

  allCategories: Subject<any> = new BehaviorSubject<any>(null);

  platform = { 'Platform': 'web'};
  loader = new BehaviorSubject<any>(false);
  loader$ = this.loader.asObservable();

  constructor( private http: HttpClient ) { }

  fetchApps(): Observable<any> {
    return this.http.post(environment.galaxy_admin_allAppByCategory, this.platform)
    .pipe (
      retry(3), catchError (this.handleError)
    );
  }

  getAllApps() {
    this.updateLoader(true);
    this.fetchApps().subscribe( (res) => {
      this.extractTogetCategories(res.Categories);
    },
    (err: HttpErrorResponse ) => {
      this.updateLoader(true);
      console.log(err);
    });
  }

  extractTogetCategories(response) {
    const categories: Category[] = [];
    for (const category of response ) {
      const appCategory = new Category();
        if (category.CategoryApps.length !== 0) {
          appCategory.name = category.CategoryName;
        }
      const apps = [];
      for (const app of category.CategoryApps) {
        
        apps.push(app);
      }
      appCategory.apps = apps;
      categories.push(appCategory);
    }
    this.allCategories.next(categories);
    this.updateLoader(false);
  }

  updateLoader(state) {
    this.loader.next(state);
  }

  handleError( error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }

}
