import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrasactionalAppsComponent } from './trasactional-apps.component';

describe('TrasactionalAppsComponent', () => {
  let component: TrasactionalAppsComponent;
  let fixture: ComponentFixture<TrasactionalAppsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrasactionalAppsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrasactionalAppsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
