import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppsContainerComponent } from './apps-container.component';

describe('AppsContainerComponent', () => {
  let component: AppsContainerComponent;
  let fixture: ComponentFixture<AppsContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppsContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
