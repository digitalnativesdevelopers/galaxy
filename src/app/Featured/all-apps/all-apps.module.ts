import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllAppsRoutingModule } from './all-apps-routing.module';
import { AllAppsComponent } from './all-apps.component';
import { RightSideBarComponent } from './right-side-bar/right-side-bar.component';
import { LeftSideBarComponent } from './left-side-bar/left-side-bar.component';
import { HeaderComponent } from './header/header.component';
import { AppsContainerComponent } from './apps-container/apps-container.component';
import { TrasactionalAppsComponent } from './apps-container/trasactional-apps/trasactional-apps.component';
import { SocialAppsComponent } from './apps-container/social-apps/social-apps.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    AllAppsComponent,
    RightSideBarComponent,
    LeftSideBarComponent,
    HeaderComponent,
    AppsContainerComponent,
    TrasactionalAppsComponent,
    SocialAppsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AllAppsRoutingModule
  ]
})
export class AllAppsModule { }
