import { RecentlyUsedAppService } from './Featured/home-page/recently-used/recently-used-app.service';
import { RecommendedService } from './Featured/home-page/recommended-apps/recommended.service';
import { Component, OnInit } from '@angular/core';
import { FavoriteAppService } from './Featured/home-page/favourite-apps/favorite-app.service';
import { AuthenticationService } from './Auth/authentication.service';
import { MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { Router } from '@angular/router';
// import { AlertsService } from 'angular-alert-module';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'galaxy';
  staffId: string;

  constructor(
    // private alerts: AlertsService,
    private toastr: ToastrService,
    private route: Router,
    private adalSvc: MsAdalAngular6Service,
    private auth: AuthenticationService,
    private favoriteAppsService: FavoriteAppService,
    private recentlyUsedService: RecentlyUsedAppService,
    private recommendedAppsService: RecommendedService
    ) {}

  authenticateUser() {
    // get staff role
    this.auth.getStaffRole();
  }


  ngOnInit() {
    this.auth.getStaffDetails();
     this.authenticateUser();
  }
}
